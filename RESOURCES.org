* General Information
    Emu-Docs seems to be the most comprehensive.

    - https://github.com/Emu-Docs/Emu-Docs ::
    - http://fms.komkon.org/EMUL8/ ::

* Gameboy Information
** Cartridge
    - http://fms.komkon.org/GameBoy/Tech/Carts.html ::

** Hardware
    - http://marc.rawer.de/Gameboy/ ::
    - http://imrannazar.com/GameBoy-Emulation-in-JavaScript ::
    - http://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html ::
    - https://en.wikipedia.org/wiki/Bank_switching ::
    - http://www.classic-games.com/atari2600/bankswitch.html ::
    - https://realboyemulator.wordpress.com/2013/01/02/the-nintendo-game-boy-part-3/ ::
    - http://stackoverflow.com/questions/8034566/overflow-and-carry-flags-on-z80/8037485#8037485 ::
    - http://www.righto.com/2013/09/the-z-80-has-4-bit-alu-heres-how-it.html ::
