(in-package #:ops-tests)

(in-suite cpl-suite)

(create-op-tests ()
  (:test-list (cpl))
  (:body ;; Clear flags register.
         (setf *f* 0)
         (let ((result (logand (lognot *a*) +byte-mask+)))
           (op)
           (is (= *a* result)
               "Result check: expected 0x~x, got 0x~x" result *a*)
           ;; Check that the flags are set to the expected values:
           ;;   Half-carry: t
           ;;   Subtract:   t
           ;;   Carry:      unaffected
           ;;   Zero:       unaffected
           (flag-check "Flags check" :zero nil :subtract t :half-carry t :carry nil))))
