(in-package #:ops-tests)

(in-suite rst-suite)

(create-op-tests ()
  (:test-list (rst-0) (rst-1) (rst-2) (rst-3)
              (rst-4) (rst-5) (rst-6) (rst-7))

  (:body (let ((previous-pc (logand *pc* +two-byte-mask+))
               ;; Destination is a number for these ops.
               (expected-pc (* #x08 destination)))
           (op)

           ;; Check that PC is the expected value.
           (is (= *pc* expected-pc)
               "PC check: expected 0x~x, got 0x~x" expected-pc *pc*)

           ;; Check that the previous PC was saved to the stack.
           (let* ((least-significant-byte (read-memory *sp*))
                  (most-significant-byte  (read-memory (1+ *sp*)))
                  (stack-pc (dpb most-significant-byte (byte 8 8) (ldb (byte 8 0) least-significant-byte))))
             (is (= previous-pc stack-pc)
                 "Stack PC check: expected 0x~x, got 0x~x" previous-pc stack-pc)))))

