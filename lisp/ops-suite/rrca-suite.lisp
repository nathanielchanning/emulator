(in-package #:ops-tests)

(in-suite rrca-suite)

(create-op-tests ()
  (:test-list (rrca))

  (:body ;; Make sure A is odd so
         ;; that the carry flag can
         ;; be tested.
         (when (= (logand *a* 1) 0)
           (setf *a* (1+ *a*)))
         ;; We add on the previous 0th bit to the end for this
         ;; operation.
         (let ((result (+ (ash (logand *a* +byte-mask+) -1)
                          (if (= (logand *a* 1) 0)
                              0
                              #x80))))
           ;; Check for a correct result.
           (op)
           (is (= *a* result)
               "Result check: expected 0x~x, got 0x~x" result *a*)

           ;; Check that the expected flag bits are set.
           (flag-check "Flags check" :zero nil :subtract nil :half-carry nil :carry t)

           ;; Check that the zero flag is set when the result is zero.
           (setf *a* #x00)
           (op)
           (flag-check "Zero check" :zero t :subtract nil :half-carry nil :carry nil))))
