(in-package #:ops-tests)

(in-suite add16-suite)

;;; Use register pair instead of memory accessor.
;;; ADD16 doesn't affect the zero bit in the flag
;;; register, so set it to something to ensure its
;;; value is known.
(create-op-tests (:memory-accessor nil)
  (:test-list (add-hl-bc) (add-hl-de)
              (add-hl-hl) (add-hl-sp))
  (:body (let ((result (logand (+ source destination) +two-byte-mask+)))
           ;; Check that the result is correct.
           (op)
           (is (= result destination)
               "Result check: expected ~a, got ~a" result destination)
           ;; Check that the half carry flag is set
           ;; when there's an over flow in the lower nibble
           ;; of the upper byte, i.e., the 11th bit.
           (setf source      #x0F00
                 destination #x0F00
                 *f* (logandc2 *f* +zero-flag+))
           (op)
           (flag-check "Half-carry check" :zero nil :subtract nil :half-carry t :carry nil)
           ;; Check that the carry flag is set
           ;; when there's an over flow in the upper nibble
           ;; of the upper byte.
           (setf source      #xF000
                 destination #xF000)
           (op)
           (flag-check "Carry check" :zero nil :subtract nil :half-carry nil :carry t))))
