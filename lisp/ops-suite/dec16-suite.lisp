(in-package #:ops-tests)

(in-suite dec16-suite)

;;; Flags are not affected for this op.
(create-op-tests (:memory-accessor nil)
  (:test-list (dec16-bc) (dec16-de)
              (dec16-hl) (dec16-sp))

  (:body (let ((result (logand (1- destination) +two-byte-mask+)))
           ;; Check that the result is correct.
           (op)
           (is (= destination result)
               "Result check: expected 0x~x, got 0x~x" result destination))))
