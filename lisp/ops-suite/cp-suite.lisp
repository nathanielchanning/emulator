(in-package #:ops-tests)

(in-suite cp-suite)

(create-op-tests ()
  (:test-list (cp-a-b)  (cp-a-c) (cp-a-d)
              (cp-a-e)  (cp-a-h) (cp-a-l)
              (cp-a-hl) (cp-a-a))

  (:body ;; Check that the destination, *A*, does
         ;; not change after execution.
         (let ((result (setf destination (lognot source))))
           (op)
           (is (= destination result)
               "Result check: expected ~a, got ~a" result destination)

           ;; Check that the zero flag is set when the result is 0.
           (setf destination #x00
                 source      #x00)
           (op)
           (flag-check "Zero check" :zero t :subtract t :half-carry nil :carry nil)

           ;; Do not perform these tests on CP-A-A, since it will never
           ;; succeed.
           (unless (= op-code #xBF)
             ;; Check that the half carry flag is set when there is a borrow
             ;; at the 3rd bit.
             (setf destination #b11000111
                   source      #b01001111)
             (op)
             (flag-check "Half-carry check" :zero nil :subtract t :half-carry t :carry nil)

             ;; Check that the carry flag is set when there is a borrow
             ;; at the 7th bit.
             (setf destination #b01111111
                   source      #b11111111)
             (op)
             (flag-check "Carry check" :zero nil :subtract t :half-carry nil :carry t)))))


