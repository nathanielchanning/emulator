(in-package #:ops-tests)

(in-suite rlc-suite)

(create-op-tests ()
  (:test-list (rlc-b)  (rlc-c) (rlc-d)
              (rlc-e)  (rlc-h) (rlc-l)
              (rlc-hl) (rlc-a))

  (:body ;; Ensure last bit is set to
         ;; test the carry flag.
         (setf destination #xF0)

         ;; We add on the previous 7th bit for this
         ;; operation.
         (let ((result (+ (logand (ash destination 1) +byte-mask+)
                          (if (= (ash destination 7) 0)
                              0
                              1))))
           ;; Check for a correct result.
           (op)
           (is (= destination result)
               "Result check: expected 0x~x, got 0x~x" result destination)

           ;; Check that the expected flag bits are set.
           (flag-check "Flags check" :zero nil :subtract nil :half-carry nil :carry t)

           ;; Check that the zero flag is set when the result is zero.
           (setf destination #x00)
           (op)
           (flag-check "Zero check" :zero t :subtract nil :half-carry nil :carry nil))))
