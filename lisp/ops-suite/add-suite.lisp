(in-package #:ops-tests)

(in-suite add-suite)

(create-op-tests ()
  (:test-list (add-a-b)  (add-a-c) (add-a-d)
              (add-a-e)  (add-a-h) (add-a-l)
              (add-a-hl) (add-a-a))
  (:body (let ((result (logand (+ source destination) +byte-mask+)))
           ;; Check that the result is correct.
           (op)
           (is (= result destination)
               "Result check: expected ~a, got ~a" result destination)

           ;; Check that the half carry flag is set when the result exceeds
           ;; #x0F in the lower nibbles and the carry flag isn't set when there
           ;; isn't an overflow in the upper nibbles.
           (setf source      #x0F
                 destination #x0F)
           (op)
           (flag-check "Half-carry check" :zero nil :subtract nil :half-carry t :carry nil)

           ;; Check that the carry flag is set when the result exceeds #xFF
           ;; and the half-carry flag isn't set when there isn't an overflow
           ;; in the lower nibbles.
           (setf source      #xF0
                 destination #xF0)
           (op)
           (flag-check "Carry check" :zero nil :subtract nil :half-carry nil :carry t)

           ;; Check that the zero flag is set when the result is 0.
           (setf source      #x00
                 destination #x00)
           (op)
           (flag-check "Zero check" :zero t :subtract nil :half-carry nil :carry nil))))
