(in-package #:ops-tests)

(in-suite inc-suite)

(create-op-tests ()
  (:test-list (inc-b)  (inc-c) (inc-d)
              (inc-e)  (inc-h) (inc-l)
              (inc-hl) (inc-a))

  (:body (let ((result (logand (1+ destination) +byte-mask+)))
           ;; Check that the result is correct.
           (op)
           (is (= destination result)
               "Result check: expected 0x~x, got 0x~x" result destination)

           ;; Check that the half carry flag is set when the result exceeds
           ;; #x0F in the lower nibbles and the carry flag isn't set when there
           ;; isn't an overflow in the upper nibbles.
           (setf destination #x0F)
           (op)
           (flag-check "Half-carry check" :zero nil :subtract nil :half-carry t :carry nil)

           ;; Check that the carry flag is set when the result exceeds #xFF
           ;; and the half-carry flag isn't set when there isn't an overflow
           ;; in the lower nibbles.
           ;;
           ;; Check that the zero flag is set when the result is zero.
           (setf destination #xFF)
           (op)
           (flag-check "Carry and Zero check" :zero t :subtract nil :half-carry t :carry t))))
