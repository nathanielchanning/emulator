(in-package #:ops-tests)

(in-suite rl-suite)

(create-op-tests ()
  (:test-list (rl-b)  (rl-c) (rl-d)
              (rl-e)  (rl-h) (rl-l)
              (rl-hl) (rl-a))

  (:body ;; Ensure last bit is set to
         ;; test the carry flag.
         (setf destination #xF0)

         (let ((result (logand (ash destination 1) +byte-mask+)))
           ;; Check for a correct result.
           (op)
           (is (= destination result)
               "Result check: expected 0x~x, got 0x~x" result destination)

           ;; Check that the expected flag bits are set.
           (flag-check "Flags check" :zero nil :subtract nil :half-carry nil :carry t)

           ;; Check that the zero flag is set when the result is zero.
           (setf destination #x80
                 *f* #x00)
           (op)
           (flag-check "Zero check" :zero t :subtract nil :half-carry nil :carry t))))


