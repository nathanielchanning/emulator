(in-package #:ops-tests)

(in-suite inc16-suite)

;;; Flags are not affected for this op.
(create-op-tests (:memory-accessor nil)
  (:test-list (inc16-bc) (inc16-de)
              (inc16-hl) (inc16-sp))

  (:body (let ((result (logand (1+ destination) +two-byte-mask+)))
           ;; Check that the result is correct.
           (op)
           (is (= destination result)
               "Result check: expected 0x~x, got 0x~x" result destination))))

