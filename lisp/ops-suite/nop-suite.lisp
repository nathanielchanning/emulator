(in-package #:ops-tests)

(in-suite nop-suite)

(create-op-tests ()
  (:test-list (nop))

  (:body ;; Check that the flag register
         ;; isn't affected.
         (setf *f* #xF0)
         (op)
         (flag-check "Flags check" :zero t :subtract t :half-carry t :carry t)))
