(in-package #:ops-tests)

(in-suite rr-suite)

(create-op-tests ()
  (:test-list (rr-b)  (rr-c) (rr-d)
              (rr-e)  (rr-h) (rr-l)
              (rr-hl) (rr-a))

  (:body ;; Ensure there aren't any flags set and
         ;; destination is non-zero.
         (setf destination #xF0
               *f*         #x00)
       
         (let ((result (logand (ash destination -1) +byte-mask+)))
           ;; Check for a correct result.
           (op)
           (is (= destination result)
               "Result check: expected 0x~x, got 0x~x" result destination)
       
           ;; Check that the expected flag bits are set.
           (flag-check "Flags check" :zero nil :subtract nil :half-carry nil :carry nil)
       
           ;; Check that the zero flag is set when the result is zero.
           (setf destination #x01
                 *f* #x00)
           (op)
           (flag-check "Zero check" :zero t :subtract nil :half-carry nil :carry t))))
