(in-package #:ops-tests)

(in-suite xor-suite)

(create-op-tests ()
  (:test-list (xor-a-b)  (xor-a-c) (xor-a-d)
              (xor-a-e)  (xor-a-h) (xor-a-l)
              (xor-a-hl) (xor-a-a))

  (:body ;; Check that the result is correct.
         (setf source      #b10101010
               destination #b11111111)
         (let ((result (logxor source destination)))
           (op)
           (is (= destination result)
               "Result check: expected 0x~x, got 0x~x" result destination)


           ;; Check that the flags are set to the
           ;; expected values. Reset zero flag in case
           ;; the op is xor-a-a.
           (when (= op-code #xAF)
             (setf *f* (logandc2 *f* +zero-flag+)))
           (flag-check "Flags check" :zero nil :subtract nil :half-carry nil :carry nil)

           ;; Check that the zero flag is set when
           ;; the result is zero.
           (setf source destination)
           (op)
           (flag-check "Zero check" :zero t :subtract nil :half-carry nil :carry nil))))
 
