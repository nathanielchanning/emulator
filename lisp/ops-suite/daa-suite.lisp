(in-package #:ops-tests)

(in-suite daa-suite)

;; Half-carry should always be nil
;; after the operation. The subtraction
;; flag is unaffected.
(create-op-tests ()
  (:test-list (daa))
  (:body ;; Check that DAA to the gets the
         ;; expected value. While we're adding
         ;; #x99 and #x11 here, after using DAA,
         ;; it should appear that we added 99d and
         ;; 11d, to get 10d. This should be stored
         ;; as #x10.
         (setf *a* #x99
               *b* #x11)
         (add-a-b)
         (op)
         (is (= *a* #x10)
             "Result add check: expected 0x10, got 0x~x" *a*)

         ;; Check that the carry flag is set after
         ;; there's an overflow in the upper nibble.
         (flag-check "Carry check" :zero nil :subtract nil :half-carry nil :carry t)

         ;; Check that DAA to the expected value
         ;; when subtracting.
         (setf *a* #x99
               *b* #x11)
         (sub-a-b)
         (op)
         (is (= *a* #x88)
             "Result subtract check: expected 0x88, got 0x~x" *a*)

         ;; Check that the subtract flag isn't changed.
         (flag-check "Subtract check" :zero nil :subtract t :half-carry nil :carry nil)

         ;; Check that the zero flag is set when the result is zero.
         (setf *a* #x00)
         (op)
         (flag-check "Zero check" :zero t :subtract t :half-carry nil :carry nil)))
