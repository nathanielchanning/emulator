(in-package #:ops-tests)

(in-suite bit-suite)


(create-op-tests ()
  (:test-list (bit-b-0) (bit-c-0) (bit-d-0)  (bit-e-0)
              (bit-h-0) (bit-l-0) (bit-hl-0) (bit-a-0)

              (bit-b-1) (bit-c-1) (bit-d-1)  (bit-e-1)
              (bit-h-1) (bit-l-1) (bit-hl-1) (bit-a-1)

              (bit-b-2) (bit-c-2) (bit-d-2)  (bit-e-2)
              (bit-h-2) (bit-l-2) (bit-hl-2) (bit-a-2)

              (bit-b-3) (bit-c-3) (bit-d-3)  (bit-e-3)
              (bit-h-3) (bit-l-3) (bit-hl-3) (bit-a-3)

              (bit-b-4) (bit-c-4) (bit-d-4)  (bit-e-4)
              (bit-h-4) (bit-l-4) (bit-hl-4) (bit-a-4)

              (bit-b-5) (bit-c-5) (bit-d-5)  (bit-e-5)
              (bit-h-5) (bit-l-5) (bit-hl-5) (bit-a-5)

              (bit-b-6) (bit-c-6) (bit-d-6)  (bit-e-6)
              (bit-h-6) (bit-l-6) (bit-hl-6) (bit-a-6)

              (bit-b-7) (bit-c-7) (bit-d-7)  (bit-e-7)
              (bit-h-7) (bit-l-7) (bit-hl-7) (bit-a-7))

  (:body (let* ((set-value (ash #x01 source))
                (reset-value (logand (lognot set-value) +byte-mask+)))

           ;; Check for correct value when
           ;; the bit is set.
           (setf destination set-value)
           (op)
           (flag-check "Bit set check" :zero nil :subtract nil :half-carry t :carry nil)

           ;; Check for correct value when
           ;; the bit is reset.
           (setf destination reset-value)
           (op)
           (flag-check "Bit reset check" :zero t :subtract nil :half-carry t :carry nil))))
