(in-package #:ops-tests)

(in-suite set-suite)


(create-op-tests ()
  (:test-list (set-b-0) (set-c-0) (set-d-0 ) (set-e-0)
              (set-h-0) (set-l-0) (set-hl-0) (set-a-0)

              (set-b-1) (set-c-1) (set-d-1 ) (set-e-1)
              (set-h-1) (set-l-1) (set-hl-1) (set-a-1)

              (set-b-2) (set-c-2) (set-d-2 ) (set-e-2)
              (set-h-2) (set-l-2) (set-hl-2) (set-a-2)

              (set-b-3) (set-c-3) (set-d-3 ) (set-e-3)
              (set-h-3) (set-l-3) (set-hl-3) (set-a-3)

              (set-b-4) (set-c-4) (set-d-4 ) (set-e-4)
              (set-h-4) (set-l-4) (set-hl-4) (set-a-4)

              (set-b-5) (set-c-5) (set-d-5 ) (set-e-5)
              (set-h-5) (set-l-5) (set-hl-5) (set-a-5)

              (set-b-6) (set-c-6) (set-d-6 ) (set-e-6)
              (set-h-6) (set-l-6) (set-hl-6) (set-a-6)

              (set-b-7) (set-c-7) (set-d-7 ) (set-e-7)
              (set-h-7) (set-l-7) (set-hl-7) (set-a-7))
  (:body ;; Zero out destination to make
         ;; sure the bit is definitely being
         ;; set.
         (setf destination 0)
         (let ((expected-result (ash #x1 source)))

           ;; Check the result.
           (op)
           (is (= expected-result destination)
               "Result check: expected 0x~x, got 0x~x" expected-result destination))))
