(in-package #:ops-tests)

(in-suite rlca-suite)

(create-op-tests ()
  (:test-list (rlca))

  (:body ;; We add on the previous 7th bit for this
         ;; operation.
         (let ((result (+ (logand (ash *a* 1) +byte-mask+)
                          (if (= (ash *a* 7) 0)
                              0
                              1))))
           ;; Check for a correct result.
           (op)
           (is (= *a* result)
               "Result check: expected 0x~x, got 0x~x" result *a*)

           ;; Check that the expected flag bits are set.
           (flag-check "Flags check" :zero nil :subtract nil :half-carry nil :carry t)

           ;; Check that the zero flag is set when the result is zero.
           (setf *a* #x00)
           (op)
           (flag-check "Zero check" :zero t :subtract nil :half-carry nil :carry nil))))

