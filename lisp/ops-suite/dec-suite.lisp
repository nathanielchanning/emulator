(in-package #:ops-tests)

(in-suite dec-suite)

(create-op-tests ()
  (:test-list (dec-b)  (dec-c) (dec-d)
              (dec-e)  (dec-h) (dec-l)
              (dec-hl) (dec-a))

  (:body ;; The subtract flag should be set for all of these.
         (let ((result (logand (1- destination) +byte-mask+)))
           ;; Check that the result is correct.
           (op)
           (is (= destination result)
               "Result check: expected ~a, got ~a" result destination)

           ;; Check that the zero flag is set when the result is 0.
           (setf destination #x01)
           (op)
           (flag-check "Zero check" :zero t :subtract t :half-carry nil :carry nil)

           ;; Check that the half carry flag is set when there is a borrow
           ;; at the 3rd bit.
           (setf destination #b00110000)
           (op)
           (flag-check "Half-carry check" :zero nil :subtract t :half-carry t :carry nil)

           ;; Check that the result doesn't use more than 8 bits when it
           ;; is negative. This is checking C implementation rather than
           ;; hardware functionality and is probably a regression test.
           (setf destination #x00)
           (op)
           (is (= destination (logand destination #xFF))
               "Result exceeds 8 bits: 0x~x" destination))))

