(in-package #:ops-tests)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun convert-symbol (symbol-string &key memory-access)
    "Convert SYMBOL-STRING to its corresponding accessor form.
Signals an error if SYMBOL-STRING does not designate a proper
source or destination in any op."
      (case (ensure-symbol symbol-string '#:ops-tests)
        (A  '*a*)
        (B  '*b*)
        (D  '*d*)
        (H  '*h*)
        (F  '*f*)
        (C  '*c*)
        (E  '*e*)
        (L  '*l*)
        (SP '*sp*)
        (PC '*pc*)
        (BC (if memory-access
                '(memory (pair *b* *c*))
                '(pair *b* *c*)))
        (DE (if memory-access
                '(memory (pair *d* *e*))
                '(pair *d* *e*)))
        (HL (if memory-access
                '(memory (pair *h* *l*))
                '(pair *h* *l*)))
        ;; Note that we assume that PC is incremented before the function call.
        (N (if memory-access
               '(memory (dpb #xFF (byte 8 8) (dpb (memory *pc*) (byte 8 0) #x00)))
               '(memory *pc*)))
        ;; LSB is stored first in memory.
        (NN (if memory-access
                '(memory (dpb (memory (1+ *pc*)) (byte 8 8) (dpb (memory *pc*) (byte 8 0) #x00)))
                '(dpb (memory (1+ *pc*)) (byte 8 8) (dpb (memory *pc*) (byte 8 0) #x00))))
        ;; Check if SYMBOL-STRING is a number instead.
        (t (let ((integer? (parse-integer symbol-string :radix 16 :junk-allowed t)))
             (if integer?
                 integer?
                 (error "Invalid OP argument: ~a." symbol-string))))))

  (defun get-op-args (op-symbol &key (memory-access t))
    "Gets the op args from OP-SYMBOL. The naming convention in
the C source code follows an OP-DESTINATION-SOURCE/OP-DESTINATION naming
scheme in general. This function grabs the last two words separated
by -'s, converts them to symbols, and returns them as multiple values."
    (let* ((op-string (symbol-name op-symbol))
           (first-  (search "-" op-string))
           (second- (search "-" op-string :from-end t))
           ;; destination (typically)
           (first-arg (cond
                        ;; Return nil if neither first- or second-
                        ;; have values.
                        ((not first-) nil)
                        ((equal first- second-) (subseq op-string (1+ first-)))
                        (second- (subseq op-string (1+ first-) second-))))
           ;; source (typically)
           (second-arg (unless (equal first- second-)
                         (subseq op-string (1+ second-)))))
      (values (when first-arg
                `,(convert-symbol first-arg :memory-access memory-access))
              (when second-arg
                `,(convert-symbol second-arg :memory-access memory-access))))))

(defmacro create-op-tests  ((&key (pc #xFFCA00)
                                  (sp #xFFC600)
                                  (a  #xFFC0)
                                  (b  #xFFC0)
                                  (d  #xFFC0)
                                  (h  #xFFC0)
                                  (f  #xFF00)
                                  (c  #xFF00)
                                  (e  #xFF00)
                                  (l  #xFF00)
                                  (memory-accessor t))
                            &body rest)
  "Create op tests that execute a body for a set of ops.
Takes any register defined in 'test/registers.lisp' as a keyword
and a value to set it to as the default for the tests.
The keyword :MEMORY-ACCESSOR specifies whether the accessors for the op-args
should be accessing memory or a register pair.
REST takes two lists--one that starts with :BODY and another that
starts with :TEST-LIST. :BODY has access to the symbols DESTINATION, SOURCE,
OP, and OP-CODE. These respectively correspond to the accessors corresponding
to the destination and the source,the function to call the C function that
corresponds with the current test, and the op code of the current op being tested.
:TEST-LIST contains lists with one lisp symbol corresponding to a C function,
g.e., (:TEST-LIST (ld-b-a) (ld-c-b)). Each test can optionally specify whether
a memory accessor should be used, having the same functionality as previously mentioned."
  (let ((test-list  (rest (assoc :test-list rest)))
        (body (rest (assoc :body rest))))
    (unless (and test-list body)
      (error ":BODY and/or :TEST-LIST was not provided as a parameter in the REST argument."))
    `(progn
       ,@(loop for test in test-list collect
               (let* ((op      (first test))
                      (op-code (get-op-code op))
                      (memory-accessor (if (equal (getf (rest test) :memory-accessor 'not-provided)
                                                  'not-provided)
                                           memory-accessor
                                           (getf (rest test) :memory-accessor))))
                 (multiple-value-bind (destination source)
                     (get-op-args op :memory-access memory-accessor)
                   `(test ,op
                      ;; Set sane default values.
                      (setf *a*  ,a
                            *b*  ,b
                            *d*  ,d
                            *h*  ,h
                            *f*  ,f
                            *c*  ,c
                            *e*  ,e
                            *l*  ,l
                            *pc* ,pc
                            *sp* ,sp
                            (memory ,pc ) #x00
                            (memory ,(1+ pc)) #xC0)

                      ;; Check the dispatch table to make sure that the
                      ;; op code matches its corresponding function.
                      (op-code-check ',op ',op-code)

                      ;; Expose useful values to BODY.
                      (symbol-macrolet ((destination ,destination)
                                        (source ,source)
                                        (op-code ,op-code))
                        (flet ((op ()
                                 (funcall #',op)))
                          (progn
                            ,@body))))))))))
