(in-package #:ops-tests)

(in-suite rrc-suite)

(create-op-tests ()
  (:test-list (rrc-b)  (rrc-c) (rrc-d)
              (rrc-e)  (rrc-h) (rrc-l)
              (rrc-hl) (rrc-a))

  (:body ;; Make sure destination is odd so
         ;; that the carry flag can
         ;; be tested.
         (setf destination #x0F)

         ;; We add on the previous 0th bit to the end for this
         ;; operation.
         (let ((result (+ (logand (ash destination -1) +byte-mask+)
                          (if (= (logand destination 1) 0)
                              0
                              #x80))))
           ;; Check for a correct result.
           (op)
           (is (= destination result)
               "Result check: expected 0x~x, got 0x~x" result destination)

           ;; Check that the expected flag bits are set.
           (flag-check "Flags check" :zero nil :subtract nil :half-carry nil :carry t)

           ;; Check that the zero flag is set when the result is zero.
           (setf destination #x00)
           (op)
           (flag-check "Zero check" :zero t :subtract nil :half-carry nil :carry nil))))
