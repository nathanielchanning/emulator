(in-package #:ops-tests)

(in-suite sla-suite)

(create-op-tests ()
  (:test-list (sla-b)  (sla-c) (sla-d)
              (sla-e)  (sla-h) (sla-l)
              (sla-hl) (sla-a))

  (:body ;; Ensure last bit is set to
         ;; test the carry flag.
         (setf destination #xF0)
       
         ;; We add on the previous 7th bit for this
         ;; operation.
         (let ((result (logand +byte-mask+ (ash destination 1))))
           ;; Check for a correct result.
           (op)
           (is (= destination result)
               "Result check: expected 0x~x, got 0x~x" result destination)
       
           ;; Check that the expected flag bits are set.
           (flag-check "Flags check" :zero nil :subtract nil :half-carry nil :carry t)
       
           ;; Check that the zero flag is set when the result is zero.
           (setf destination #x00)
           (op)
           (flag-check "Zero check" :zero t :subtract nil :half-carry nil :carry nil))))

