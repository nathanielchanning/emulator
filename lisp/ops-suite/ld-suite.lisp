(in-package #:ops-tests)

(in-suite ld-suite)

(create-op-tests ()
  (:test-list
   (ld-b-b) (ld-b-c) (ld-b-d) (ld-b-e) (ld-b-h) (ld-b-l) (ld-b-hl) (ld-b-a)
   (ld-c-b) (ld-c-c) (ld-c-d) (ld-c-e) (ld-c-h) (ld-c-l) (ld-c-hl) (ld-c-a)
   (ld-d-b) (ld-d-c) (ld-d-d) (ld-d-e) (ld-d-h) (ld-d-l) (ld-d-hl) (ld-d-a)
   (ld-e-b) (ld-e-c) (ld-e-d) (ld-e-e) (ld-e-h) (ld-e-l) (ld-e-hl) (ld-e-a)
   (ld-h-b) (ld-h-c) (ld-h-d) (ld-h-e) (ld-h-h) (ld-h-l) (ld-h-hl) (ld-h-a)
   (ld-l-b) (ld-l-c) (ld-l-d) (ld-l-e) (ld-l-h) (ld-l-l) (ld-l-hl) (ld-l-a)
   (ld-a-b) (ld-a-c) (ld-a-d) (ld-a-e) (ld-a-h) (ld-a-l) (ld-a-hl) (ld-a-a)
   (ld-a-n :memory-accessor nil) (ld-a-nn))

  (:body (let ((~destination (logand (lognot destination) +byte-mask+)))
           (setf source ~destination)
           (op)
           (is (equal ~destination destination)
               "Expected, 0x~x, got 0x~x" ~destination destination))))
