(in-package #:ops-tests)

(defun flag-check (test-name &key (zero nil) (subtract nil) (half-carry nil) (carry nil))
  "Performs an IS check on the flag register for the current test. TEST-NAME
is the name of the test to print if the IS check fails. The key arguments
all correspond to a bit flag in the flag register and should be non-nil when
expected to be set."
  (let* ((flag-list (list (if zero
                              +zero-flag+
                              0)
                          (if subtract
                              +subtract-flag+
                              0)
                          (if half-carry
                              +half-carry-flag+
                              0)
                          (if carry
                              +carry-flag+
                              0)))
         ;; Grab the flag bits.
         (masked-flag (logand *f* #xF0))
         (expected-flag (reduce #'logior flag-list)))
    (is (= masked-flag expected-flag)
        "~a: expected 0x~x, found 0x~x" test-name expected-flag masked-flag)))

(defun op-code-check (op-symbol op-code)
  "Performs a test that succeeds when OP-SYMBOL's corresponding C function's
address is located at OP-CODE in the dispatch table. This includes the 0xCB
dispatch table."
  (is (pointer-eq (foreign-symbol-pointer (get-c-name op-symbol))
                  (get-dispatch-table-index op-code))
      "Index 0x~x in dispatch table does not match ~a's address" op-code op-symbol))
