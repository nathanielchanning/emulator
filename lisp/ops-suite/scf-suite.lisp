(in-package #:ops-tests)

(in-suite scf-suite)

(create-op-tests ()
  (:test-list (scf))

  (:body ;; Set flags register.
         (setf *f* #xF0)
         (op)
         ;; Check that the flags are set to the expected values:
         ;;   Half-carry: nil
         ;;   Subtract:   nil
         ;;   Carry:      t
         ;;   Zero:       unaffected
         (flag-check "Flags check" :zero t :subtract nil :half-carry nil :carry t)))
