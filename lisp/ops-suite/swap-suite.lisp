(in-package #:ops-tests)

(in-suite swap-suite)

(create-op-tests ()
  (:test-list (swap-b)  (swap-c) (swap-d)
              (swap-e)  (swap-h) (swap-l)
              (swap-hl) (swap-a))

  (:body ;; Set the destination for a flags check.
         (setf destination #xF0)

         (let ((result (logand (logior (ash destination -4)
                                       (ash destination 4))
                               +byte-mask+)))
           ;; Check that the result is correct.
           (op)
           (is (= destination result)
               "Result check: expected 0x~x, got 0x~x" result destination)

           ;; Check that the flags are the expected values:
           ;;   Half-Carry: 0
           ;;   Carry:      0
           ;;   Subtract:   0
           (flag-check "Flags check" :zero nil :subtract nil :half-carry nil :carry nil)

           ;; Check that the zero flag is set when
           ;; the result is zero.
           (setf destination #x00)
           (op)
           (flag-check "Zero check" :zero t :subtract nil :half-carry nil :carry nil))))

