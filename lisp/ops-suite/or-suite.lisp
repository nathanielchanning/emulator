(in-package #:ops-tests)

(in-suite or-suite)

(create-op-tests ()
  (:test-list (or-a-b)  (or-a-c) (or-a-d)
              (or-a-e)  (or-a-h) (or-a-l)
              (or-a-hl) (or-a-a))

  (:body (let ((result (logior destination source)))
           ;; Check that the result is correct.
           (op)
           (is (= destination result)
               "Result check: expected ~a, got ~a" result destination)

           ;; Check that the flags are set to
           ;; the expected values.
           (setf destination #xFF)
           (op)
           (flag-check "Flags check" :zero nil :subtract nil :half-carry nil :carry nil)

           ;; Check that the zero flag is set
           ;; when the result is zero.
           (setf destination #x00
                 source      #x00)
           (op)
           (flag-check "Zero check" :zero t :subtract nil :half-carry nil :carry nil))))
