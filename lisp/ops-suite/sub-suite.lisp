(in-package #:ops-tests)

(in-suite sub-suite)

(create-op-tests ()
  (:test-list (sub-a-b)  (sub-a-c) (sub-a-d)
              (sub-a-e)  (sub-a-h) (sub-a-l)
              (sub-a-hl) (sub-a-a))

  (:body ;; The subtract flag should be set for all of these.
         (let ((result (logand (- destination source) +byte-mask+)))
           ;; Check that the result is correct.
           (op)
           (is (= destination result)
               "Result check: expected ~a, got ~a" result destination)

           ;; Check that the zero flag is set when the result is 0.
           (setf destination #x00
                 source      #x00)
           (op)
           (flag-check "Zero check" :zero t :subtract t :half-carry nil :carry nil)

           ;; Do not perform these tests on SUB-A-A, since it will never
           ;; succeed.
           (unless (= op-code #x97)
             ;; Check that the half carry flag is set when there is a borrow
             ;; at the 3rd bit.
             (setf destination #b11000111
                   source      #b01001111)
             (op)
             (flag-check "Half-carry check" :zero nil :subtract t :half-carry t :carry nil)

             ;; Check that the carry flag is set when there is a borrow
             ;; at the 7th bit.
             (setf destination #b01111111
                   source      #b11111111)
             (op)
             (flag-check "Carry check" :zero nil :subtract t :half-carry nil :carry t)

             ;; Check that the result doesn't use more than 8 bits when it
             ;; is negative. This is checking C implementation rather than
             ;; hardware functionality and is probably a regression test.
             (setf destination #x00
                   source      #xFF)
             (op)
             (is (= destination (logand destination #xFF))
                 "Result exceeds 8 bits: 0x~x" destination)))))

