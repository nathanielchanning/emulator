(in-package #:ops-tests)

(in-suite rla-suite)

(create-op-tests ()
  (:test-list (rla))

  (:body (let ((result (logand (ash *a* 1) +byte-mask+)))
           ;; Check for a correct result.
           (op)
           (is (= *a* result)
               "Result check: expected 0x~x, got 0x~x" result *a*)

           ;; Check that the expected flag bits are set.
           (flag-check "Flags check" :zero nil :subtract nil :half-carry nil :carry t)

           ;; Check that the zero flag is set when the result is zero.
           (setf *a* #x80
                 *f* #x00)
           (op)
           (flag-check "Zero check" :zero t :subtract nil :half-carry nil :carry t))))
