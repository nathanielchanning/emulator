(in-package #:ops-tests)

(in-suite and-suite)

(create-op-tests ()
  (:test-list (and-a-b)  (and-a-c) (and-a-d)
              (and-a-e)  (and-a-h) (and-a-l)
              (and-a-hl) (and-a-a))
  (:body (let ((result (logand destination source)))
           ;; Check that the result is correct.
           (op)
           (is (= destination result)
               "Result check: expected 0x~x, got 0x~x" result destination)

           ;; Check that the expected flags are set.
           ;;   Carry Flag      - 0
           ;;   Half-Carry Flag - 1
           ;;   Subtract Flag   - 0
           (setf destination #x01
                 source      #x01
                 *f* (logandc2 *f* +zero-flag+))
           (op)
           (flag-check "Flags check" :zero nil :subtract nil :half-carry t :carry nil)

           ;; Check that the zero flag is set when the
           ;; result is zero.
           (setf destination #x00
                 source      #x00)
           (op)
           (flag-check "Zero check" :zero t :subtract nil :half-carry t :carry nil))))
