(in-package #:ops-tests)

(in-suite ccf-suite)

(create-op-tests ()
  (:test-list (ccf))
  (:body ;; Set flags register.
         (setf *f* #xF0)
         (op)
         ;; Check that the flags are set to the expected values:
         ;;   Half-carry: t
         ;;   Subtract:   t
         ;;   Carry:      unaffected
         ;;   Zero:       unaffected
         (flag-check "Flags check" :zero t :subtract nil :half-carry nil :carry nil)))
