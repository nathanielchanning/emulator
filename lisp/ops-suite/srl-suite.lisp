(in-package #:ops-tests)

(in-suite srl-suite)

(create-op-tests ()
  (:test-list (srl-b)  (srl-c) (srl-d)
              (srl-e)  (srl-h) (srl-l)
              (srl-hl) (srl-a))

  (:body ;; Make sure destination is odd so
         ;; that the carry flag can
         ;; be tested.
         (setf destination #x0F)

         ;; We add on the previous 0th bit to the end for this
         ;; operation.
         (let ((result (ash destination -1)))
           ;; Check for a correct result.
           (op)
           (is (= destination result)
               "Result check: expected 0x~x, got 0x~x" result destination)

           ;; Check that the expected flag bits are set.
           (flag-check "Flags check" :zero nil :subtract nil :half-carry nil :carry t)

           ;; Check that the zero flag is set when the result is zero.
           (setf destination #x00)
           (op)
           (flag-check "Zero check" :zero t :subtract nil :half-carry nil :carry nil))))

