(in-package #:ops-tests)

(in-suite res-suite)

(create-op-tests ()
  (:test-list (res-b-0) (res-c-0) (res-d-0 ) (res-e-0)
              (res-h-0) (res-l-0) (res-hl-0) (res-a-0)

              (res-b-1) (res-c-1) (res-d-1 ) (res-e-1)
              (res-h-1) (res-l-1) (res-hl-1) (res-a-1)

              (res-b-2) (res-c-2) (res-d-2 ) (res-e-2)
              (res-h-2) (res-l-2) (res-hl-2) (res-a-2)

              (res-b-3) (res-c-3) (res-d-3 ) (res-e-3)
              (res-h-3) (res-l-3) (res-hl-3) (res-a-3)

              (res-b-4) (res-c-4) (res-d-4 ) (res-e-4)
              (res-h-4) (res-l-4) (res-hl-4) (res-a-4)

              (res-b-5) (res-c-5) (res-d-5 ) (res-e-5)
              (res-h-5) (res-l-5) (res-hl-5) (res-a-5)

              (res-b-6) (res-c-6) (res-d-6 ) (res-e-6)
              (res-h-6) (res-l-6) (res-hl-6) (res-a-6)

              (res-b-7) (res-c-7) (res-d-7 ) (res-e-7)
              (res-h-7) (res-l-7) (res-hl-7) (res-a-7))

  (:body ;; Fill out destination to make
         ;; sure the bit is definitely being
         ;; reset.
         (setf destination #xFF)
         (let ((expected-result (dpb #x0 (byte 1 source) #xFF)))

           ;; Check the result.
           (op)
           (is (= expected-result destination)
               "Result check: expected 0x~x, got 0x~x" expected-result destination))))
