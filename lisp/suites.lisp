(in-package #:suites)

(def-suite emulator-suite
    :description "Test all emulator functionality.")

(def-suite ops-suite :in emulator-suite
    :description "Tests behavior of operations.")

(def-suite ld-suite :in ops-suite
    :description "Test 'LD' ops.")

(def-suite add-suite :in ops-suite
    :description "Test 'ADD' ops.")

(def-suite adc-suite :in ops-suite
    :description "Test 'ADC' ops.")

(def-suite add16-suite :in ops-suite
    :description "Test 'ADD16' ops.")

(def-suite sub-suite :in ops-suite
    :description "Test 'SUB' ops.")

(def-suite sbc-suite :in ops-suite
    :description "Test 'SBC' ops.")

(def-suite and-suite :in ops-suite
    :description "Test 'AND' ops.")

(def-suite xor-suite :in ops-suite
    :description "Test 'XOR' ops.")

(def-suite or-suite :in ops-suite
    :description "Test 'OR' ops.")

(def-suite cp-suite :in ops-suite
    :description "Test 'CP' ops.")

(def-suite inc-suite :in ops-suite
    :description "Test 'INC' ops.")

(def-suite inc16-suite :in ops-suite
    :description "Test 'INC16' ops.")

(def-suite dec-suite :in ops-suite
    :description "Test 'DEC' ops.")

(def-suite dec16-suite :in ops-suite
    :description "Test 'DEC16' ops.")

(def-suite swap-suite :in ops-suite
    :description "Test 'SWAP' ops.")

(def-suite daa-suite :in ops-suite
    :description "Test 'DAA' ops.")

(def-suite cpl-suite :in ops-suite
    :description "Test 'CPL' ops.")

(def-suite ccf-suite :in ops-suite
    :description "Test 'CCF' ops.")

(def-suite scf-suite :in ops-suite
    :description "Test 'CCF' ops.")

(def-suite nop-suite :in ops-suite
    :description "Test 'NOP' ops.")

(def-suite rlca-suite :in ops-suite
    :description "Test 'RLCA' ops.")

(def-suite rla-suite :in ops-suite
    :description "Test 'RLA' ops.")

(def-suite rrca-suite :in ops-suite
    :description "Test 'RRCA' ops.")

(def-suite rra-suite :in ops-suite
    :description "Test 'RRA' ops.")

(def-suite rlc-suite :in ops-suite
    :description "Test 'RLC' ops.")

(def-suite rl-suite :in ops-suite
    :description "Test 'RL' ops.")

(def-suite rrc-suite :in ops-suite
    :description "Test 'RRC' ops.")

(def-suite rr-suite :in ops-suite
    :description "Test 'RR' ops.")

(def-suite sla-suite :in ops-suite
    :description "Test 'SLA' ops.")

(def-suite sra-suite :in ops-suite
    :description "Test 'SRA' ops.")

(def-suite srl-suite :in ops-suite
    :description "Test 'SRA' ops.")

(def-suite rst-suite :in ops-suite
    :description "Test 'RST' ops.")

(def-suite set-suite :in ops-suite
    :description "Test 'SET' ops.")

(def-suite res-suite :in ops-suite
    :description "Test 'RES' ops.")

(def-suite bit-suite :in ops-suite
    :description "Test 'BIT' ops.")

(def-suite memory-suite :in emulator-suite
    :description "Test the memory interface.")

(def-suite no-mbc-suite :in memory-suite
    :description "Test memory with no MBC.")
