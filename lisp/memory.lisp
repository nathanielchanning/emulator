(in-package #:memory)

;;; RAM arrays
(defcvar "graphics_ram" (:pointer :uint8))
(defcvar "external_ram" (:pointer :uint8))
(defcvar "internal_ram" (:pointer :uint8))
(defcvar "graphics_attribute_ram" (:pointer :uint8))
(defcvar "zero_page_ram" (:pointer :uint8))
(defcvar "rom_mmap" (:pointer :uint8))

;;; Convenience pointers
(define-symbol-macro +graphics-ram+           (get-var-pointer '*graphics-ram*))
(define-symbol-macro +external-ram+           (get-var-pointer '*external-ram*))
(define-symbol-macro +internal-ram+           (get-var-pointer '*internal-ram*))
(define-symbol-macro +graphics-attribute-ram+ (get-var-pointer '*graphics-attribute-ram*))
(define-symbol-macro +zero-page-ram+          (get-var-pointer '*zero-page-ram*))

;;; Sizes of each section of RAM
(defconstant +graphics-ram-size+           #x2000)
(defconstant +external-ram-size+           #x2000)
(defconstant +internal-ram-size+           #x2000)
(defconstant +graphics-attribute-ram-size+ #x9F)
(defconstant +zero-page-ram-size+          #x7F)

;;; C functions
(defcfun "read_no_mbc" :int
  (address :int))

(defcfun "write_no_mbc" :int
  (address :int)
  (value :int))

(defcfun "read_memory" :int
  (address :int))

(defcfun "write_memory" :int
  (address :int)
  (value :int))

;;; Memory accessor
(defun memory (address)
  "Access ADDRESS in memory."
  (read-memory (ldb (byte 16 0) address)))

(defun (setf memory) (new-value address)
  (write-memory (ldb (byte 16 0) address) new-value))
