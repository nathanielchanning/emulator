(in-package #:cl-user)

(cffi:define-foreign-library libemulator
  (:unix "libemulator.so"))

(push "." cffi:*foreign-library-directories*)

(cffi:use-foreign-library libemulator)

(defpackage suites
  (:use cl fiveam)
  (:export ;; Ops
           ld-suite
           add-suite
           add16-suite
           adc-suite
           sub-suite
           sbc-suite
           and-suite
           xor-suite
           or-suite
           cp-suite
           inc-suite
           inc16-suite
           dec-suite
           dec16-suite
           swap-suite
           daa-suite
           cpl-suite
           ccf-suite
           scf-suite
           nop-suite
           rlca-suite
           rla-suite
           rrca-suite
           rra-suite
           rlc-suite
           rl-suite
           rrc-suite
           rr-suite
           sla-suite
           sra-suite
           srl-suite
           rst-suite
           set-suite
           res-suite
           bit-suite

           ;; Memory
           no-mbc-suite))

(defpackage registers
  (:use cffi cl)
  (:export ;; Constants
           +lower-nibble-mask+
           +byte-mask+
           +twelve-bit-mask+
           +two-byte-mask+
           +zero-flag+
           +subtract-flag+
           +half-carry-flag+
           +carry-flag+

           ;; Helper functions
           pair

           ;; Registers
           *a*
           *b*
           *d*
           *h*
           *f*
           *c*
           *e*
           *l*
           *sp*
           *pc*))

(defpackage ops
  (:use cffi cl)
  (:export ;; Foreign vars
           *op-dispatch*

           ;; helper functions
           get-c-name
           requires-immediate-p
           get-op-code
           get-dispatch-table-index
           get-cb-dispatch-table-index))

(defpackage memory
  (:use cffi cl)
  (:export ;;memory access
           memory
           read-no-mbc
           write-no-mbc
           read-memory
           write-memory

           ;; RAM
           *rom-mmap*

           ;; RAM array pointers
           +graphics-ram+
           +external-ram+
           +internal-ram+
           +graphics-attribute-ram+
           +zero-page-ram+

           ;; RAM section sizes
           +graphics-ram-size+
           +external-ram-size+
           +internal-ram-size+
           +graphics-attribute-ram-size+
           +zero-page-ram-size+))

(defpackage ops-tests
  (:use cffi cl fiveam suites registers ops memory alexandria))

(defpackage memory-tests
  (:use cffi cl fiveam suites registers memory))
