(in-package #:assembler)

(defclass assembler ()
  ((assembled-binary :reader binary-vector
                     :initform (make-array 0
                                           :adjustable t
                                           :fill-pointer 0)
                     :documentation "A vector that stores the assembled binary.")
   (base-address :reader base-address
                 :initform 0
                 :type 'fixnum
                 :documentation "The address where the first instruction
of ASSEMBLED-BINARY is loaded into.")
   (label->address :reader label->address
                   :initform (make-hash-table)
                   :documentation "Maps a label to its corresponding address.")
   (label->replacement-addresses :reader label->replacement-addresses
                                 :initform (make-hash-table)
                                 :documentation "Maps a label to addresses
that need to be replaced with the label's corresponding address.")
   (immediate-required? :accessor immediate-required?
                        :initform nil
                        :documentation "Specifies whether the last op
assembled requires an immediate argument. If it is non-nil, it specifies
the expected size of the immediate in bytes, which should only ever be
1 or 2.")
   (current-offset :accessor current-offset
                   :initform 0
                   :documentation "The current offset in ASSEMBLED-BINARY.")))

(defgeneric add-to-binary (assembler-object op-code)
  (:documentation "Adds OP-CODE to ASSEMBLED-BINARY in ASSEMBLER-OBJECT and
increments the CURRENT-OFFSET of the ASSEMBLED-BINARY. Always resets
immediate-required? to nil."))

(defgeneric insert-label-addresses (assembler-object)
  (:documentation "First does a check to make sure there aren't any
label replacements without a corresponding label address. Then replaces
all label immediate arguments with their respective address."))

(defgeneric add-to-assembler (assembler-object op)
  (:documentation "Adds OP to the assembler object.
The exact behavior of this method varies on the type
of op, but in most cases, it will add a binary representation
of op onto (BINARY-VECTOR ASSEMBLER-OBJECT) via ADD-TO-BINARY."))

;;; Clean this up a bit at some point.
(defmethod add-to-binary ((assembler-object assembler) (op-code fixnum))
  (let ((binary-vector (binary-vector assembler-object))
        (least-significant-byte (ldb (byte 8 0) op-code))
        (most-significant-byte  (ldb (byte 8 8) op-code)))
    (vector-push-extend least-significant-byte binary-vector)
    (incf (current-offset assembler-object))
    (cond
      ;; Add on the MSB for #xCB ops.
      ((and (not (immediate-required? assembler-object))
            (< #xFF op-code))
       (vector-push-extend most-significant-byte binary-vector)
       (incf (current-offset assembler-object)))
      ((and (equal 1 (immediate-required? assembler-object))
            (< #xFF op-code))
       (warn "1 byte immediate expected, truncating 0x~x at position ~a."
            op-code
            (current-offset assembler-object)))
      ;; Add on another byte if the immediate is supposed to
      ;; be two bytes and reset immediate-required?.
      ((equal 2 (immediate-required? assembler-object))
       (vector-push-extend most-significant-byte binary-vector)
       (incf (current-offset assembler-object))))
    (setf (immediate-required? assembler-object) nil)))

(defmethod add-to-assembler ((assembler-object assembler) (op fixnum))
  (cond
    ((immediate-required? assembler-object)
     (add-to-binary assembler-object op))
    (t
     (error "Immediate, ~a, is not required for previous op." op))))

(defmethod add-to-assembler ((assembler-object assembler) (op symbol))
  (with-slots (label->address
               label->replacement-addresses
               immediate-required?
               current-offset)
      assembler-object
    (let ((op-code (get-op-code op)))
      (cond
        ;; Since keyword isn't a class,
        ;; do the conditional here.
        ((typep op 'keyword)
         (cond
           (immediate-required?
            (push current-offset (gethash op label->replacement-addresses))
            ;; Add 0 to the binary as a filler until it is replaced
            ;; by its respective address.
            (add-to-binary assembler-object 0))
           ((gethash op label->address)
            (error "Label, ~a, is already defined." op))
           (t
            (setf (gethash op label->address) current-offset))))
        ((not op-code)
         (error "~a is not a valid op." op))
        ((requires-immediate-p op)
         (add-to-binary assembler-object op-code)
         (setf immediate-required? t))
        (t
         (add-to-binary assembler-object op-code))))))

(defmethod insert-label-addresses ((assembler-object assembler))
  (with-slots (label->replacement-addresses
               label->address
               assembled-binary
               offset-address
               base-address)
      assembler-object
    (flet ((label-defined (label _)
             (declare (ignore _))
             (unless (gethash label label->address nil)
               (error "Undefined label, ~a" label)))
           (replace-label-with-address (label offset-address)
             (let ((address (+ offset-address base-address))
                   (replacement-indices (gethash label label->replacement-addresses)))
               (dolist (index replacement-indices)
                 (setf (aref assembled-binary index) address)))))
      (maphash #'label-defined label->replacement-addresses)
      (maphash #'replace-label-with-address label->address)))
  assembler-object)

(defmacro assemble (&body ops)
  "Transforms OPS into an ASSEMBLER class. The following types
are accepted so far:
  KEYWORD--used stand-alone to define a label or succeeding an
           op that requires an immediate value.
  SYMBOL--corresponds to an op located in the ops package.
  FIXNUM--used for ops that require an immediate value.
Example:
  (assemble
   :test
    ld-b-a
    ld-a-n #x8
    jmp-nn :test)"
  (let ((assembler (make-instance 'assembler)))
    (dolist (op ops assembler)
      (add-to-assembler assembler op))))
