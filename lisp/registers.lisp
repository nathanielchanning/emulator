(in-package #:registers)

;;; Useful constants.
(defconstant +lower-nibble-mask+ #xF)
(defconstant +byte-mask+ #xFF)
(defconstant +twelve-bit-mask+ #xFFF)
(defconstant +two-byte-mask+ #xFFFF)
(defconstant +zero-flag+ #x80)
(defconstant +subtract-flag+ #x40)
(defconstant +half-carry-flag+ #x20)
(defconstant +carry-flag+ #x10)

;;; Pair function
(defun pair (reg2 reg1)
  "Takes the value of the register pair REG2, REG1, where
REG2 is the most significant byte and REG1 is the least
significant byte."
  (dpb reg2 (byte 8 8) (dpb reg1 (byte 8 0) 0)))

(define-setf-expander pair (reg2 reg1 &environment env)
  "Takes a new value, and assigns the first byte to REG1 and
the second byte to REG2."
  (multiple-value-bind (temps1 vals1 stores1
                               store-form-1 access-form-1)
      (get-setf-expansion reg1 env)
    (multiple-value-bind (temps2 vals2 stores2
                                 store-form-2 access-form-2)
        (get-setf-expansion reg2 env)
      (let ((store (gensym))
            (temp-store-1 (first stores1))
            (temp-store-2 (first stores2)))
        (values (append temps1 temps2)
                (append vals1  vals2)
                (list store)
                `(let ((,temp-store-1 (logand ,store +byte-mask+))
                       (,temp-store-2 (logand (ash ,store -8) +byte-mask+)))
                   ,store-form-1
                   ,store-form-2
                   ,store)
                `(pair ,access-form-2 ,access-form-1))))))


;;; 8 bit registers
;;; Higher-order when paired.
(defcvar "A" :int)
(defcvar "B" :int)
(defcvar "D" :int)
(defcvar "H" :int)
;;; Lower-order when paired
(defcvar "F" :int)
(defcvar "C" :int)
(defcvar "E" :int)
(defcvar "L" :int)

;;; 16 bit registers.
(defcvar "SP" :int)
(defcvar "PC" :int)
