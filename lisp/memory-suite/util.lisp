(in-package #:memory-tests)

(defun memset (pointer value size)
  "Sets SIZE bytes to VALUE starting at POINTER."
  (dotimes (index size)
    (setf (mem-aref pointer :uint8 index) value)))

(defun zero-out-ram ()
  "Set all RAM sections to 0."
  (memset +graphics-ram+ 0 +graphics-ram-size+)
  (memset +external-ram+ 0 +external-ram-size+)
  (memset +internal-ram+ 0 +internal-ram-size+)
  (memset +zero-page-ram+ 0 +zero-page-ram-size+)
  (memset +graphics-attribute-ram+ 0 +graphics-attribute-ram-size+))
