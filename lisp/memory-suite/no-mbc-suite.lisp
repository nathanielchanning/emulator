(in-package #:memory-tests)

(in-suite no-mbc-suite)


(test read-no-mbc-bounds-check
  (flet ((bounds-check (ram-section-name array lower-bound upper-bound)
           "Performs a bounds check on RAM-SECTION-NAME. ARRAY is the pointer
            to the array being tested. LOWER-BOUND and UPPER-BOUND are the
            bounds of RAM-SECTION-NAME in the Gameboy memory map, where both
            bounds are inclusive."
           (let ((array-upper-bound (- upper-bound lower-bound)))

             ;; Set the bounds to #x01.
             (setf (mem-aref array :uint8 0) #x01
                   (mem-aref array :uint8 array-upper-bound) #x01)

             ;; Check for correct values.
             (let ((lower-bound-value (read-no-mbc lower-bound))
                   (upper-bound-value (read-no-mbc upper-bound)))

               ;; Check the lower bound value.
               (is (= lower-bound-value #x01)
                   (format nil "~a, lower bound: expected 0x~x, got 0x~x" ram-section-name #x01 lower-bound-value))

               ;; Check the upper bound value.
               (is (= upper-bound-value #x01)
                   (format nil "~a, upper bound: expected 0x~x, got 0x~x" ram-section-name #x01 upper-bound-value)))

             ;; Set the bounds back to #x00, the assumed value.
             (setf (mem-aref array :uint8 0) #x00
                   (mem-aref array :uint8 array-upper-bound) #x00))))

    (zero-out-ram)
    (bounds-check "Graphics RAM"           +graphics-ram+           #x8000 #x9FFF)
    (bounds-check "External RAM"           +external-ram+           #xA000 #xBFFF)
    (bounds-check "Internal RAM"           +internal-ram+           #xC000 #xDFFF)
    (bounds-check "Internal RAM Mirror"    +internal-ram+           #xE000 #xFDFF)
    (bounds-check "Graphics Attribute RAM" +graphics-attribute-ram+ #xFE00 #xFE9F)
    (bounds-check "Zero Page RAM"          +zero-page-ram+          #xFF80 #xFFFF)))

(test write-no-mbc-bounds-check
  (flet ((bounds-check (ram-section-name array lower-bound upper-bound)
           "Performs a bounds check on RAM-SECTION-NAME. ARRAY is the pointer
            to the array being tested. LOWER-BOUND and UPPER-BOUND are the
            bounds of RAM-SECTION-NAME in the Gameboy memory map, where both
            bounds are inclusive."
           (let ((array-upper-bound (- upper-bound lower-bound)))

             ;; Set the bounds to #x01.
             (write-no-mbc lower-bound #x01)
             (write-no-mbc upper-bound #x01)

             ;; Check for correct values.
             (let ((lower-bound-value (mem-aref array :uint8 0))
                   (upper-bound-value (mem-aref array :uint8 array-upper-bound)))

               ;; Check the lower bound value.
               (is (= lower-bound-value #x01)
                   (format nil "~a, lower bound: expected 0x~x, got 0x~x" ram-section-name #x01 lower-bound-value))

               ;; Check the upper bound value.
               (is (= upper-bound-value #x01)
                   (format nil "~a, upper bound: expected 0x~x, got 0x~x" ram-section-name #x01 upper-bound-value)))

             ;; Set the bounds back to #x00, the assumed value.
             (setf (mem-aref array :uint8 0) #x00
                   (mem-aref array :uint8 array-upper-bound) #x00))))

    (zero-out-ram)
    (bounds-check "Graphics RAM"           +graphics-ram+           #x8000 #x9FFF)
    (bounds-check "External RAM"           +external-ram+           #xA000 #xBFFF)
    (bounds-check "Internal RAM"           +internal-ram+           #xC000 #xDFFF)
    (bounds-check "Internal RAM Mirror"    +internal-ram+           #xE000 #xFDFF)
    (bounds-check "Graphics Attribute RAM" +graphics-attribute-ram+ #xFE00 #xFE9F)
    (bounds-check "Zero Page RAM"          +zero-page-ram+          #xFF80 #xFFFF)))
