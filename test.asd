(in-package #:cl-user)

(ql:quickload '(cffi cffi-grovel fiveam))

(defpackage test-asd
  (:use cl asdf))

(in-package #:test-asd)

(defsystem test
    :depends-on ("fiveam" "cffi")
    :components
    ((:module "lisp"
              :components
              ((:file "package")
               (:file "registers")
               (:file "ops")
               (:file "memory")
               (:file "suites")

               (:module "assembler"
                        :components
                        ((:file "package")
                         (:file "assembler-class")))

               (:module "ops-suite"
                        :components
                        ((:file "util")
                         (:file "macro")
                         (:file "ld-suite")
                         (:file "add-suite")
                         (:file "add16-suite")
                         (:file "adc-suite")
                         (:file "sub-suite")
                         (:file "sbc-suite")
                         (:file "and-suite")
                         (:file "xor-suite")
                         (:file "or-suite")
                         (:file "cp-suite")
                         (:file "inc-suite")
                         (:file "inc16-suite")
                         (:file "dec-suite")
                         (:file "dec16-suite")
                         (:file "swap-suite")
                         (:file "daa-suite")
                         (:file "cpl-suite")
                         (:file "ccf-suite")
                         (:file "scf-suite")
                         (:file "nop-suite")
                         (:file "rlca-suite")
                         (:file "rla-suite")
                         (:file "rrca-suite")
                         (:file "rra-suite")
                         (:file "rlc-suite")
                         (:file "rl-suite")
                         (:file "rrc-suite")
                         (:file "rr-suite")
                         (:file "sla-suite")
                         (:file "sra-suite")
                         (:file "srl-suite")
                         (:file "rst-suite")
                         (:file "set-suite")
                         (:file "res-suite")
                         (:file "bit-suite")))

               (:module "memory-suite"
                        :components
                        ((:file "util")
                         (:file "no-mbc-suite"))))))
    :perform (test-op (o c) (uiop:symbol-call :fiveam '#:run!
                                              (uiop:find-symbol* 'emulator-suite :suites))))

(load-system 'test :force t)
;;;(test-system 'test :force t)
