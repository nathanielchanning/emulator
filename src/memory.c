#include <stdint.h>
#include <sys/mman.h>
#include <errno.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "memory.h"

/*-------------------------------------------------------------------*/
/*----------------------------MEMORY-MAP-----------------------------*/
/*-------------------------------------------------------------------*/
/* The Gameboy has 64 KB of addressable memory, of which most is     */
/* memory-mapped for a specific purpose. The address space is split  */
/* into two general regions: ROM and RAM.                            */
/*                                                                   */
/* ROM begins at 0x0000 and extends to 0x7FFF. The section from      */
/* 0x0000 to 0x3FFF is ROM Bank 0. This bank is accessable at all    */
/* times while a program is running. The section from 0x4000 to      */
/* 0x7FFF is indexable by an MBC to a number of different external   */
/* banks located in the Gameboy cartridge.                           */
/*                                                                   */
/* RAM begins at 0x8000 and extends to 0xFFFF, though a small part   */
/* is used for memory-mapped I/O. The section from 0x8000 to 0x9FFF  */
/* is Graphics RAM and is used for the display. The section from     */
/* 0xC000 to 0xDFFF is an Internal RAM Bank. It is accessible at all */
/* times when the program is running. The section from 0xE000 to     */
/* 0xFDFF is an Internal RAM Mirror. It mirrors the contents of the  */
/* Internal RAM Bank. The section from 0xFE00 to 0xFE9F is Graphics  */
/* Attribute RAM. It holds data about sprites used in the Graphics   */
/* RAM. The section from 0xFF80 to 0xFFFF is Zero-page RAM. It is    */
/* high-speed and generally used to interact with the Gameboy        */
/* hardware.                                                         */
/*                                                                   */
/* Memory-mapped I/O is from 0xFF00 to 0xFF7F. It is used for        */
/* interacting with the Gameboy hardware.                            */
/*                                                                   */
/*--------------------MEMORY-BANK-CONTROLLER-(MBC)-------------------*/
/* Cartridges can optionally have a chip built-in to access larger   */
/* amounts of RAM and ROM. ~~~                                       */
/*                                                                   */
/*       ADD INFORMATION ABOUT MBC'S WHEN YOU GET HERE.              */
/*                                                                   */
/*-------------------------------------------------------------------*/
/* ROM begins at 0x0000 and extends to 0x7FFF.                       */
/* RAM begins at 0x8000 and extends to 0xFFFF. (some is mmap'd I/O)  */
/*-------------------------------------------------------------------*/
/* Section Name                                      Lower    Upper  */
/*                                                   Bound    Bound  */
/*                                                                   */
/* ROM Bank 0 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 0x0000 - 0x3FFF */
/*                                                                   */
/* ROM Bank 1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 0x4000 - 0x7FFF */
/*                                                                   */
/* Graphics RAM ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 0x8000 - 0x9FFF */
/*                                                                   */
/* External Cartridge RAM ~~~~~~~~~~~~~~~~~~~~~~~~~~ 0xA000 - 0xBFFF */
/*                                                                   */
/* Internal RAM ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 0xC000 - 0xDFFF */
/*                                                                   */
/* Internal RAM mirror ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 0xE000 - 0xFDFF */
/*                                                                   */
/* Graphics Attribute RAM ~~~~~~~~~~~~~~~~~~~~~~~~~~ 0xFE00 - 0xFE9F */
/*                                                                   */
/* Memory-mapped I/O ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 0xFF00 - 0xFF7F */
/*                                                                   */
/* Zero-page RAM ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 0xFF80 - 0xFFFF */
/*-------------------------------------------------------------------*/

static int rom_mmap_size;

/* Allocate the maximum amount of RAM that
 * can be used--32 kilobytes or 0x8000 bytes.*/
uint8_t external_ram [0x8000];
uint8_t internal_ram [0x2000];
uint8_t graphics_ram [0x2000];
uint8_t graphics_attribute_ram [0xA0];
uint8_t zero_page_ram [0x80];
uint8_t * rom_mmap;

/* Used to keep track of the
   current banks being used. */
static int current_ram_bank = 1;
static int current_rom_bank = 1;


/*-------------------------------------------------------------------*/
/*--------------------------INITIALIZE_ROM---------------------------*/
/*-------------------------------------------------------------------*/
/* DESCRIPTION:                                                      */
/*   Memory map the ROM file to be used.                             */
/*                                                                   */
/* SUCCESS CONDITION:                                                */
/*   The file was mapped successfully.                               */
/*                                                                   */
/* FAILURE CONDITION:                                                */
/*   The file wasn't mapped successfully.                            */
/*                                                                   */
/* RETURN VALUE:                                                     */
/*   non-zero if it fails.                                           */
/*   The value will be the error number.                             */
/*-------------------------------------------------------------------*/
int initialize_rom (char * filename) {
  FILE * file;
  struct stat file_information;

  /* Grab the size of the file.  */
  if (stat (filename, & file_information)) {
    return errno;
  }

  rom_mmap_size = file_information. st_size;

  file = fopen (filename, "r");

  /* If opening the file failed,
     return the error number. */
  if (file == NULL) {
    return errno;
  }

  rom_mmap = mmap (NULL, rom_mmap_size, PROT_READ, 0, fileno (file), 0);

  /* Safe to close the file at this point. */
  fclose (file);

  /* If the mmap failed,
     return the errno. */
  if (rom_mmap == NULL) {
    return errno;
  }

  return 0;
}

/*-------------------------------------------------------------------*/
/*--------------------------TERMINATE_ROM----------------------------*/
/*-------------------------------------------------------------------*/
/* DESCRIPTION:                                                      */
/*   Unmap the memory the ROM file is using.                         */
/*                                                                   */
/* SUCCESS CONDITION:                                                */
/*   The file was unmapped successfully.                             */
/*                                                                   */
/* FAILURE CONDITION:                                                */
/*   The file wasn't unmapped successfully.                          */
/*                                                                   */
/* RETURN VALUE:                                                     */
/*   non-zero if it fails.                                           */
/*   The value will be the error number.                             */
/*-------------------------------------------------------------------*/
int terminate_rom () {
  int result = munmap (rom_mmap, rom_mmap_size);

  return result?
           errno:
           0;
}

/*-------------------------------------------------------------------*/
/*--------------------------READ_NO_MBC------------------------------*/
/*-------------------------------------------------------------------*/
/* DESCRIPTION:                                                      */
/*   Read data from the address in RAM.                              */
/*                                                                   */
/* SUCCESS CONDITION:                                                */
/*   The data was successfully read.                                 */
/*                                                                   */
/* FAILURE CONDITION:                                                */
/*   The data wasn't successfully read.                              */
/*                                                                   */
/* RETURN VALUE:                                                     */
/*   The data from the address or a negative error number.           */
/*-------------------------------------------------------------------*/
int read_no_mbc (int address) {
  switch (address & 0xF000) {
    /* ROM Banks. */
    case 0x0000: case 0x1000:
    case 0x2000: case 0x3000:
    case 0x4000: case 0x5000:
    case 0x6000: case 0x7000:
      return rom_mmap [address];

    /* Graphics RAM. */
    case 0x8000:
    case 0x9000:
      address -= 0x8000;
      return (int) graphics_ram [address];

    /* External RAM. */
    case 0xA000:
    case 0xB000:
      address -= 0xA000;
      return (int) external_ram [address];

    /* Internal RAM and partial Internal Mirror. */
    case 0xE000:
      /* adjust internal mirror. */
      address -= 0x2000;
    case 0xC000:
    case 0xD000:
      address -= 0xC000;
      return (int) internal_ram [address];
  }

  /* All values below 0xF000 are covered before
   * this point. Thus, we can use single if statements
   * past here with an implicit bound if we keep
   * them in the correct order. */

  /* Rest of Internal RAM Mirror. */
  if (address < 0xFE00) {
      address -= (0x2000 + 0xC000);
      return (int) internal_ram [address];
  }

  /* Graphics Attribute RAM. */
  if (address <  0xFEA0) {
    address -= 0xFE00;
    return (int) graphics_attribute_ram [address];
  }

  /* Zero page RAM. */
  if (address >= 0xFF80) {
    address -= 0xFF80;
    return (int) zero_page_ram [address];
  }

  /* TODO: Memory mapped I/O goes here. */
  return 0;
}

/*-------------------------------------------------------------------*/
/*---------------------------READ_MEMORY-----------------------------*/
/*-------------------------------------------------------------------*/
/* DESCRIPTION:                                                      */
/*   Read data from the address.                                     */
/*                                                                   */
/* SUCCESS CONDITION:                                                */
/*   The data was successfully read.                                 */
/*                                                                   */
/* FAILURE CONDITION:                                                */
/*   The data wasn't successfully read or an invalid address         */
/*   was passed to the function.                                     */
/*                                                                   */
/* RETURN VALUE:                                                     */
/*   The data from the address or a negative error number.           */
/*-------------------------------------------------------------------*/
int read_memory (int address) {

  if (address < 0 || address > 0xFFFF) {
    /* This should only occur if there's a bug.
     * Can probably be removed after the load and store
     * instructions are implemented. */
    fprintf (stderr, "Invalid address passed to read_memory.");

    /* Correct address. */
    address &= TWO_BYTE_MASK;
  }

  return read_no_mbc (address);
}

/*-------------------------------------------------------------------*/
/*--------------------------WRITE_NO_MBC-----------------------------*/
/*-------------------------------------------------------------------*/
/* DESCRIPTION:                                                      */
/*   Write data to the address.                                      */
/*                                                                   */
/* SUCCESS CONDITION:                                                */
/*   The data was successfully written.                              */
/*                                                                   */
/* FAILURE CONDITION:                                                */
/*   The data wasn't successfully written.                           */
/*                                                                   */
/* RETURN VALUE:                                                     */
/*   Zero or a negative number to indicate failure.                  */
/*-------------------------------------------------------------------*/
int write_no_mbc (int address, int value) {
  switch (address & 0xF000) {
    /* ROM Banks. */
    case 0x0000: case 0x1000:
    case 0x2000: case 0x3000:
    case 0x4000: case 0x5000:
    case 0x6000: case 0x7000:
      return 0;

    /* Graphics RAM. */
    case 0x8000:
    case 0x9000:
      address -= 0x8000;
      return (int) (graphics_ram [address] = (uint8_t) value);

    /* External RAM. */
    case 0xA000:
    case 0xB000:
      address -= 0xA000;
      return (int) (external_ram [address] = (uint8_t) value);

    /* Internal RAM and partial Internal Mirror. */
    case 0xE000:
      /* adjust internal mirror. */
      address -= 0x2000;
    case 0xC000:
    case 0xD000:
      address -= 0xC000;
      return (int) (internal_ram [address] = (uint8_t) value);
  }

  /* All values below 0xF000 are covered before
   * this point. Thus, we can use single if statements
   * past here with an implicit lower bound if we keep
   * them in the correct order. */

  /* Rest of Internal RAM Mirror. */
  if (address < 0xFE00) {
      address -= (0x2000 + 0xC000);
      return (int) (internal_ram [address] = (uint8_t) value);
  }

  /* Graphics Attribute RAM. */
  if (address <  0xFEA0) {
    address -= 0xFE00;
    return (int) (graphics_attribute_ram [address] = (uint8_t) value);
  }

  /* Zero page RAM. */
  if (address >= 0xFF80) {
    address -= 0xFF80;
    return (int) (zero_page_ram [address] = (uint8_t) value);
  }

  /* TODO: Memory mapped I/O goes here. */
  return 0;
}


/*-------------------------------------------------------------------*/
/*---------------------------WRITE_MEMORY----------------------------*/
/*-------------------------------------------------------------------*/
/* DESCRIPTION:                                                      */
/*   Write data to the address.                                      */
/*                                                                   */
/* SUCCESS CONDITION:                                                */
/*   The data was successfully written.                              */
/*                                                                   */
/* FAILURE CONDITION:                                                */
/*   The data wasn't successfully written.                           */
/*                                                                   */
/* RETURN VALUE:                                                     */
/*   Zero or a negative number to indicate failure.                  */
/*-------------------------------------------------------------------*/
int write_memory (int address, int value) {

  if (address < 0 || address > 0xFFFF) {
    /* This should only occur if there's a bug.
     * Can probably be removed after the load and store
     * instructions are implemented. */
    fprintf (stderr, "Invalid address passed to read_memory.");

    /* Correct address. */
    address &= TWO_BYTE_MASK;
  }

  return write_no_mbc (address, value);
}
