/* emulator.h */

/* Anything that is needed for testing
 * should be defined in here. These could
 * be defined in the tests; however, we don't
 * know how many files the tests will span, so
 * this may be simpler. */
#ifdef TEST_BUILD

/* 8  bit registers. */
/* Higher-order when paired. */
extern int A;
extern int B;
extern int D;
extern int H;
/* Lower-order when paired. */
extern int F;
extern int C;
extern int E;
extern int L;

/* 16 bit registers. */
extern int SP;
extern int PC;

/* Op dispatch table. */
/* This line needs tested at some point. */
extern void (* op_dispatch [0xFF + 0x01]) ();
extern void (* CB_op_dispatch [0xFF + 0x01]) ();

/* Operation Functions. */
void LD_B_B ();
void LD_B_C ();
void LD_B_D ();
void LD_B_E ();
void LD_B_H ();
void LD_B_L ();
void LD_B_HL ();
void LD_B_A ();
void LD_C_B ();
void LD_C_C ();
void LD_C_D ();
void LD_C_E ();
void LD_C_H ();
void LD_C_L ();
void LD_C_HL ();
void LD_C_A ();
void LD_D_B ();
void LD_D_C ();
void LD_D_D ();
void LD_D_E ();
void LD_D_H ();
void LD_D_L ();
void LD_D_HL ();
void LD_D_A ();
void LD_E_B ();
void LD_E_C ();
void LD_E_D ();
void LD_E_E ();
void LD_E_H ();
void LD_E_L ();
void LD_E_HL ();
void LD_E_A ();
void LD_H_B ();
void LD_H_C ();
void LD_H_D ();
void LD_H_E ();
void LD_H_H ();
void LD_H_L ();
void LD_H_HL ();
void LD_H_A ();
void LD_L_B ();
void LD_L_C ();
void LD_L_D ();
void LD_L_E ();
void LD_L_H ();
void LD_L_L ();
void LD_L_HL ();
void LD_L_A ();
void LD_HL_B ();
void LD_HL_C ();
void LD_HL_D ();
void LD_HL_E ();
void LD_HL_H ();
void LD_HL_L ();
void LD_HL_A ();
void LD_HL_n ();
void LD_A_B ();
void LD_A_C ();
void LD_A_D ();
void LD_A_E ();
void LD_A_H ();
void LD_A_L ();
void LD_A_A ();
void LD_A_BC ();
void LD_A_DE ();
void LD_A_HL ();
void LD_A_nn ();
void LD_A_n ();
void LD_BC_A ();
void LD_DE_A ();
void LD_nn_A ();

/* 8 bit arithmetic */
void ADD_A_B ();
void ADD_A_C ();
void ADD_A_D ();
void ADD_A_E ();
void ADD_A_H ();
void ADD_A_L ();
void ADD_A_HL ();
void ADD_A_A ();
void ADC_A_B ();
void ADC_A_C ();
void ADC_A_D ();
void ADC_A_E ();
void ADC_A_H ();
void ADC_A_L ();
void ADC_A_HL ();
void ADC_A_A ();
void SUB_A_B ();
void SUB_A_C ();
void SUB_A_D ();
void SUB_A_E ();
void SUB_A_H ();
void SUB_A_L ();
void SUB_A_HL ();
void SUB_A_A ();
void SBC_A_B ();
void SBC_A_C ();
void SBC_A_D ();
void SBC_A_E ();
void SBC_A_H ();
void SBC_A_L ();
void SBC_A_HL ();
void SBC_A_A ();
void AND_A_B ();
void AND_A_C ();
void AND_A_D ();
void AND_A_E ();
void AND_A_H ();
void AND_A_L ();
void AND_A_HL ();
void AND_A_A ();
void OR_A_B ();
void OR_A_C ();
void OR_A_D ();
void OR_A_E ();
void OR_A_H ();
void OR_A_L ();
void OR_A_HL ();
void OR_A_A ();
void XOR_A_B ();
void XOR_A_C ();
void XOR_A_D ();
void XOR_A_E ();
void XOR_A_H ();
void XOR_A_L ();
void XOR_A_HL ();
void XOR_A_A ();
void CP_A_B ();
void CP_A_C ();
void CP_A_D ();
void CP_A_E ();
void CP_A_H ();
void CP_A_L ();
void CP_A_HL ();
void CP_A_A ();
void INC_B ();
void INC_C ();
void INC_D ();
void INC_E ();
void INC_H ();
void INC_L ();
void INC_HL ();
void INC_A ();
void DEC_B ();
void DEC_C ();
void DEC_D ();
void DEC_E ();
void DEC_H ();
void DEC_L ();
void DEC_HL ();
void DEC_A ();

/* 16 bit arithmetic. */
void ADD_HL_BC ();
void ADD_HL_DE ();
void ADD_HL_HL ();
void ADD_HL_SP ();
void INC16_BC ();
void INC16_DE ();
void INC16_HL ();
void INC16_SP ();
void DEC16_BC ();
void DEC16_DE ();
void DEC16_HL ();
void DEC16_SP ();

/* Miscellaneous. */
void SWAP_B ();
void SWAP_C ();
void SWAP_D ();
void SWAP_E ();
void SWAP_H ();
void SWAP_L ();
void SWAP_HL ();
void SWAP_A ();
void DAA ();
void CPL ();
void CCF ();
void SCF ();
void NOP ();
void HALT ();
void STOP ();
void DI ();
void EI ();
void RST_0 ();
void RST_1 ();
void RST_2 ();
void RST_3 ();
void RST_4 ();
void RST_5 ();
void RST_6 ();
void RST_7 ();

/* Rotates and Shifts. */
void RLCA ();
void RLA ();
void RRCA ();
void RRA ();
void RLC_B ();
void RLC_C ();
void RLC_D ();
void RLC_E ();
void RLC_H ();
void RLC_L ();
void RLC_HL ();
void RLC_A ();
void RL_B ();
void RL_C ();
void RL_D ();
void RL_E ();
void RL_H ();
void RL_L ();
void RL_HL ();
void RL_A ();
void RRC_B ();
void RRC_C ();
void RRC_D ();
void RRC_E ();
void RRC_H ();
void RRC_L ();
void RRC_HL ();
void RRC_A ();
void RR_B ();
void RR_C ();
void RR_D ();
void RR_E ();
void RR_H ();
void RR_L ();
void RR_HL ();
void RR_A ();
void SLA_B ();
void SLA_C ();
void SLA_D ();
void SLA_E ();
void SLA_H ();
void SLA_L ();
void SLA_HL ();
void SLA_A ();
void SRA_B ();
void SRA_C ();
void SRA_D ();
void SRA_E ();
void SRA_H ();
void SRA_L ();
void SRA_HL ();
void SRA_A ();
void SRL_B ();
void SRL_C ();
void SRL_D ();
void SRL_E ();
void SRL_H ();
void SRL_L ();
void SRL_HL ();
void SRL_A ();

/* Bit ops */

void SET_B_0 ();
void SET_C_0 ();
void SET_D_0 ();
void SET_E_0 ();
void SET_H_0 ();
void SET_L_0 ();
void SET_HL_0 ();
void SET_A_0 ();

void SET_B_1 ();
void SET_C_1 ();
void SET_D_1 ();
void SET_E_1 ();
void SET_H_1 ();
void SET_L_1 ();
void SET_HL_1 ();
void SET_A_1 ();

void SET_B_2 ();
void SET_C_2 ();
void SET_D_2 ();
void SET_E_2 ();
void SET_H_2 ();
void SET_L_2 ();
void SET_HL_2 ();
void SET_A_2 ();

void SET_B_3 ();
void SET_C_3 ();
void SET_D_3 ();
void SET_E_3 ();
void SET_H_3 ();
void SET_L_3 ();
void SET_HL_3 ();
void SET_A_3 ();

void SET_B_4 ();
void SET_C_4 ();
void SET_D_4 ();
void SET_E_4 ();
void SET_H_4 ();
void SET_L_4 ();
void SET_HL_4 ();
void SET_A_4 ();

void SET_B_5 ();
void SET_C_5 ();
void SET_D_5 ();
void SET_E_5 ();
void SET_H_5 ();
void SET_L_5 ();
void SET_HL_5 ();
void SET_A_5 ();

void SET_B_6 ();
void SET_C_6 ();
void SET_D_6 ();
void SET_E_6 ();
void SET_H_6 ();
void SET_L_6 ();
void SET_HL_6 ();
void SET_A_6 ();

void SET_B_7 ();
void SET_C_7 ();
void SET_D_7 ();
void SET_E_7 ();
void SET_H_7 ();
void SET_L_7 ();
void SET_HL_7 ();
void SET_A_7 ();

void RES_B_0 ();
void RES_C_0 ();
void RES_D_0 ();
void RES_E_0 ();
void RES_H_0 ();
void RES_L_0 ();
void RES_HL_0 ();
void RES_A_0 ();

void RES_B_1 ();
void RES_C_1 ();
void RES_D_1 ();
void RES_E_1 ();
void RES_H_1 ();
void RES_L_1 ();
void RES_HL_1 ();
void RES_A_1 ();

void RES_B_2 ();
void RES_C_2 ();
void RES_D_2 ();
void RES_E_2 ();
void RES_H_2 ();
void RES_L_2 ();
void RES_HL_2 ();
void RES_A_2 ();

void RES_B_3 ();
void RES_C_3 ();
void RES_D_3 ();
void RES_E_3 ();
void RES_H_3 ();
void RES_L_3 ();
void RES_HL_3 ();
void RES_A_3 ();

void RES_B_4 ();
void RES_C_4 ();
void RES_D_4 ();
void RES_E_4 ();
void RES_H_4 ();
void RES_L_4 ();
void RES_HL_4 ();
void RES_A_4 ();

void RES_B_5 ();
void RES_C_5 ();
void RES_D_5 ();
void RES_E_5 ();
void RES_H_5 ();
void RES_L_5 ();
void RES_HL_5 ();
void RES_A_5 ();

void RES_B_6 ();
void RES_C_6 ();
void RES_D_6 ();
void RES_E_6 ();
void RES_H_6 ();
void RES_L_6 ();
void RES_HL_6 ();
void RES_A_6 ();

void RES_B_7 ();
void RES_C_7 ();
void RES_D_7 ();
void RES_E_7 ();
void RES_H_7 ();
void RES_L_7 ();
void RES_HL_7 ();
void RES_A_7 ();

void BIT_B_0 ();
void BIT_C_0 ();
void BIT_D_0 ();
void BIT_E_0 ();
void BIT_H_0 ();
void BIT_L_0 ();
void BIT_HL_0 ();
void BIT_A_0 ();

void BIT_B_1 ();
void BIT_C_1 ();
void BIT_D_1 ();
void BIT_E_1 ();
void BIT_H_1 ();
void BIT_L_1 ();
void BIT_HL_1 ();
void BIT_A_1 ();

void BIT_B_2 ();
void BIT_C_2 ();
void BIT_D_2 ();
void BIT_E_2 ();
void BIT_H_2 ();
void BIT_L_2 ();
void BIT_HL_2 ();
void BIT_A_2 ();

void BIT_B_3 ();
void BIT_C_3 ();
void BIT_D_3 ();
void BIT_E_3 ();
void BIT_H_3 ();
void BIT_L_3 ();
void BIT_HL_3 ();
void BIT_A_3 ();

void BIT_B_4 ();
void BIT_C_4 ();
void BIT_D_4 ();
void BIT_E_4 ();
void BIT_H_4 ();
void BIT_L_4 ();
void BIT_HL_4 ();
void BIT_A_4 ();

void BIT_B_5 ();
void BIT_C_5 ();
void BIT_D_5 ();
void BIT_E_5 ();
void BIT_H_5 ();
void BIT_L_5 ();
void BIT_HL_5 ();
void BIT_A_5 ();

void BIT_B_6 ();
void BIT_C_6 ();
void BIT_D_6 ();
void BIT_E_6 ();
void BIT_H_6 ();
void BIT_L_6 ();
void BIT_HL_6 ();
void BIT_A_6 ();

void BIT_B_7 ();
void BIT_C_7 ();
void BIT_D_7 ();
void BIT_E_7 ();
void BIT_H_7 ();
void BIT_L_7 ();
void BIT_HL_7 ();
void BIT_A_7 ();

#endif

/* Used to isolate the lower 4 bits. */
#define LOWER_NIBBLE_MASK 0xF

/* Used to isolate the lower 8 bits. */
#define BYTE_MASK 0xFF

/* Used to isolate the lower 12 bits. */
#define TWELVE_BIT_MASK 0xFFF

/* Used to isolate the lower 16 bits. */
#define TWO_BYTE_MASK 0xFFFF

/* Used to isolate the zero flag in F. */
#define ZERO_FLAG 0x80

/* Used to isolate the subtract flag in F. */
#define SUBTRACT_FLAG 0x40

/* Used to isolate the half-carry flag in F. */
#define HALF_CARRY_FLAG 0x20

/* Used to isolate the carry flag in F. */
#define CARRY_FLAG 0x10

/* Used to pair two registers together.
 * The following pairs are valid:
 *   AF, BC, DE, and HL. */
#define pair(reg1, reg2) ((reg1 & BYTE_MASK) << 8) | (reg2 & BYTE_MASK)
