#ifndef EMULATOR_MEMORY_H
#define EMULATOR_MEMORY_H
#include <stdint.h>

/* Used to isolate the lower 8 bits. */
#define BYTE_MASK 0xFF

/* Used to isolate the lower 16 bits. */
#define TWO_BYTE_MASK 0xFFFF

/* Used to indicate the ROM bank offsets in
   the ROM file. */
#define ROM_BANK_OFFSET 0x4000

/* Used to indicate the RAM bank offsets in
   the RAM file. */
#define RAM_BANK_OFFSET 0x2000

/* Used to indicate maximum size of external
 * RAM. */

#ifdef TEST_BUILD
extern uint8_t external_ram [0x8000];
extern uint8_t internal_ram [0x2000];
extern uint8_t graphics_ram [0x2000];
extern uint8_t graphics_attribute_ram [0xA0];
extern uint8_t zero_page_ram [0x80];
extern uint8_t * rom_mmap;

int read_no_mbc (int address);
int write_no_mbc (int address, int value);
#endif

int read_memory (int address);
int write_memory (int address, int value);


#endif
