/* emulator.c */
#include <stdio.h>
#include "emulator.h"
#include "memory.h"

/*-------------------------------------------------------------------*/
/*----------------------------REGISTERS------------------------------*/
/*-------------------------------------------------------------------*/
/* There are 6 GENERAL-PURPOSE 8-bit registers: B, D, H, C, E and L. */
/* They can be combined as register pairs for 16-bit operations.     */
/* These pairs are BC, DE and HL.                                    */
/*-------------------------------------------------------------------*/
/* The ACCUMULATOR flag, A, is part of the ALU. It is used for       */
/* ALU operations and the result of those operations is stored       */
/* in it. It can be combined with the flag register to form AF.      */
/*-------------------------------------------------------------------*/
/* The FLAG register, F, uses bits as flags to indicate certain      */
/* things. It can be combined with the flag register to form AF.     */
/*                                                                   */
/*                        -----------------                          */
/*                        -Z-N-H-C-0-0-0-0-                          */
/*                        -----------------                          */
/*                                                                   */
/* Zero Flag (Z)                                                     */
/*   Set when result of an ALU operation is zero or when two         */
/*   compared values match when using the CP instruction.            */
/*                                                                   */
/* Subtract Flag (N)                                                 */
/*   Set when subtraction was performed in the last arithmetic       */
/*   operation.                                                      */
/*                                                                   */
/* Half Carry Flag (H)                                               */
/*   Set when a carry occurred from the lower nibble (half-byte)     */
/*   in the last arithmetic operation.                               */
/*   --See DAA for usage example.                                    */
/*                                                                   */
/* Carry Flag (C)                                                    */
/*   Set when a carry occurred in the last arithmetic operation or   */
/*   if A is smaller when executing a CP instruction.                */
/*-------------------------------------------------------------------*/
/* The PROGRAM COUNTER is initialized to $100 on power-up.           */
/*-------------------------------------------------------------------*/
/* The STACK POINTER is initialized to $FFFE on power-up.            */
/*-------------------------------------------------------------------*/

/* 8  bit registers. */
/* Higher-order when paired. */
int A;
int B;
int D;
int H;
/* Lower-order when paired. */
int F;
int C;
int E;
int L;
/* Consider using a bit field for F. */

/* 16 bit registers. */
int SP;
int PC;

/*-------------------------------------------------------------------*/
/*---------------------GENERAL-PURPOSE-MACROS------------------------*/
/*-------------------------------------------------------------------*/

/* Increment the PC by 1 and & the result to prevent overflow.       */
#define increment_PC() PC = (PC + 1) & TWO_BYTE_MASK

/*-------------------------------------------------------------------*/
/*------------------------------MEMORY-------------------------------*/
/*-------------------------------------------------------------------*/

/*-------------------------------------------------------------------*/
/*-----------------------OPERATION-FUNCTIONS-------------------------*/
/*-------------------------------------------------------------------*/
/* Each op code will have a relevant function that can be accessed   */
/* at index 'op code' of the op_dispatch array.                      */
/*-------------------------------------------------------------------*/
#define LD_FROM_HL_MACRO(arg_register) void LD_##arg_register##_HL() {\
    arg_register = read_memory (pair (H, L));                         \
}

#define LD_B_MACRO(arg_register) void LD_B_##arg_register () {        \
    B = arg_register;                                                 \
}

/* Op     Parameters   Code   Cycles                                 */
/* LD        B, B      0x40      4                                   */
/*                                                                   */
/* Load value in B into B, i.e., do nothing.                         */
LD_B_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* LD        B, C      0x41      4                                   */
/*                                                                   */
/* Load value in C into B.                                           */
LD_B_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* LD        B, D      0x42      4                                   */
/*                                                                   */
/* Load value in D into B.                                           */
LD_B_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* LD        B, E      0x43      4                                   */
/*                                                                   */
/* Load value in E into B.                                           */
LD_B_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* LD        B, H      0x44      4                                   */
/*                                                                   */
/* Load value in H into B.                                           */
LD_B_MACRO (H)

/* Op     Parameters   Code   Cycles                                 */
/* LD        B, L      0x45      4                                   */
/*                                                                   */
/* Load value in L into B.                                           */
LD_B_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* LD        B, (HL)   0x46      8                                   */
/*                                                                   */
/* Load value at (HL) into B.                                        */
LD_FROM_HL_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* LD        B, A      0x47      4                                   */
/*                                                                   */
/* Load value in A into B.                                           */
LD_B_MACRO (A);

#undef LD_B_MACRO


#define LD_C_MACRO(arg_register) void LD_C_##arg_register () {        \
    C = arg_register;                                                 \
}

/* Op     Parameters   Code   Cycles                                 */
/* LD        C, B      0x48      4                                   */
/*                                                                   */
/* Load value in B into C.                                           */
LD_C_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* LD        C, C      0x49      4                                   */
/*                                                                   */
/* Load value in C into C, i.e., do nothing.                         */
LD_C_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* LD        C, D      0x4A      4                                   */
/*                                                                   */
/* Load value in D into C.                                           */
LD_C_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* LD        C, E      0x4B      4                                   */
/*                                                                   */
/* Load value in E into C.                                           */
LD_C_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* LD        C, H      0x4C      4                                   */
/*                                                                   */
/* Load value in H into C.                                           */
LD_C_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* LD        C, L      0x4D      4                                   */
/*                                                                   */
/* Load value in L into C.                                           */
LD_C_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* LD        C, (HL)   0x4E      8                                   */
/*                                                                   */
/* Load value at (HL) into C.                                        */
LD_FROM_HL_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* LD        C, A      0x4F      4                                   */
/*                                                                   */
/* Load value in A into C.                                           */
LD_C_MACRO (A);

#undef LD_C_MACRO


#define LD_D_MACRO(arg_register) void LD_D_##arg_register () {        \
    D = arg_register;                                                 \
}

/* Op     Parameters   Code   Cycles                                 */
/* LD        D, B      0x50      4                                   */
/*                                                                   */
/* Load value in B into D.                                           */
LD_D_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* LD        D, C      0x51      4                                   */
/*                                                                   */
/* Load value in C into D.                                           */
LD_D_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* LD        D, D      0x52      4                                   */
/*                                                                   */
/* Load value in D into D, i.e., do nothing.                         */
LD_D_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* LD        D, E      0x53      4                                   */
/*                                                                   */
/* Load value in E into D.                                           */
LD_D_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* LD        D, H      0x54      4                                   */
/*                                                                   */
/* Load value in H into D.                                           */
LD_D_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* LD        D, L      0x55      4                                   */
/*                                                                   */
/* Load value in L into D.                                           */
LD_D_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* LD        D, (HL)   0x56      8                                   */
/*                                                                   */
/* Load value at (HL) into D.                                        */
LD_FROM_HL_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* LD        D, A      0x57      4                                   */
/*                                                                   */
/* Load value in A into D.                                           */
LD_D_MACRO (A);

#undef LD_D_MACRO


#define LD_E_MACRO(arg_register) void LD_E_##arg_register () {        \
    E = arg_register;                                                 \
}

/* Op     Parameters   Code   Cycles                                 */
/* LD        E, B      0x58      4                                   */
/*                                                                   */
/* Load value in B into E.                                           */
LD_E_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* LD        E, C      0x59      4                                   */
/*                                                                   */
/* Load value in C into E.                                           */
LD_E_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* LD        E, D      0x5A      4                                   */
/*                                                                   */
/* Load value in D into E.                                           */
LD_E_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* LD        E, E      0x5B      4                                   */
/*                                                                   */
/* Load value in E into E, i.e., do nothing.                         */
LD_E_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* LD        E, H      0x5C      4                                   */
/*                                                                   */
/* Load value in D into E.                                           */
LD_E_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* LD        E, L      0x5D      4                                   */
/*                                                                   */
/* Load value in L into E.                                           */
LD_E_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* LD        E, (HL)   0x5E      8                                   */
/*                                                                   */
/* Load value at (HL) into E.                                        */
LD_FROM_HL_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* LD        E, A      0x5F      4                                   */
/*                                                                   */
/* Load value in A into E.                                           */
LD_E_MACRO (A);

#undef LD_E_MACRO


#define LD_H_MACRO(arg_register) void LD_H_##arg_register () {        \
    H = arg_register;                                                 \
}

/* Op     Parameters   Code   Cycles                                 */
/* LD        H, B      0x60      4                                   */
/*                                                                   */
/* Load value in B into H.                                           */
LD_H_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* LD        H, C      0x61      4                                   */
/*                                                                   */
/* Load value in C into H.                                           */
LD_H_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* LD        H, D      0x62      4                                   */
/*                                                                   */
/* Load value in D into H.                                           */
LD_H_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* LD        H, E      0x63      4                                   */
/*                                                                   */
/* Load value in E into H.                                           */
LD_H_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* LD        H, H      0x64      4                                   */
/*                                                                   */
/* Load value in H into H, i.e., do nothing.                         */
LD_H_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* LD        H, L      0x65      4                                   */
/*                                                                   */
/* Load value in L into H.                                           */
LD_H_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* LD        H, (HL)   0x66      8                                   */
/*                                                                   */
/* Load value at (HL) into H.                                        */
LD_FROM_HL_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* LD        H, A      0x67      4                                   */
/*                                                                   */
/* Load value in A into H.                                           */
LD_H_MACRO (A);

#undef LD_H_MACRO


#define LD_L_MACRO(arg_register) void LD_L_##arg_register () {        \
    L = arg_register;                                                 \
}

/* Op     Parameters   Code   Cycles                                 */
/* LD        L, B      0x68      4                                   */
/*                                                                   */
/* Load value in B into L.                                           */
LD_L_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* LD        L, C      0x69      4                                   */
/*                                                                   */
/* Load value in C into L.                                           */
LD_L_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* LD        L, D      0x6A      4                                   */
/*                                                                   */
/* Load value in D into L.                                           */
LD_L_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* LD        L, E      0x6B      4                                   */
/*                                                                   */
/* Load value in E into L.                                           */
LD_L_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* LD        L, H      0x6C      4                                   */
/*                                                                   */
/* Load value in H into L.                                           */
LD_L_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* LD        L, L      0x6D      4                                   */
/*                                                                   */
/* Load value in L into L, i.e., do nothing.                         */
LD_L_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* LD        L, (HL)   0x6E      8                                   */
/*                                                                   */
/* Load value at (HL) into L.                                        */
LD_FROM_HL_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* LD        L, A      0x6F      4                                   */
/*                                                                   */
/* Load value in A into L.                                           */
LD_L_MACRO (A);

#undef LD_L_MACRO

#define LD_INTO_HL_MACRO(arg_register) void LD_HL_##arg_register () { \
    write_memory (pair (H, L), arg_register);                         \
}

/* Op     Parameters   Code   Cycles                                 */
/* LD     (HL), B      0x70      8                                   */
/*                                                                   */
/* Load value in B into (HL).                                        */
LD_INTO_HL_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* LD     (HL), C      0x71      8                                   */
/*                                                                   */
/* Load value in C into (HL).                                        */
LD_INTO_HL_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* LD     (HL), D      0x72      8                                   */
/*                                                                   */
/* Load value in D into (HL).                                        */
LD_INTO_HL_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* LD     (HL), E      0x73      8                                   */
/*                                                                   */
/* Load value in E into (HL).                                        */
LD_INTO_HL_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* LD     (HL), H      0x74      8                                   */
/*                                                                   */
/* Load value in H into (HL).                                        */
LD_INTO_HL_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* LD     (HL), L      0x75      8                                   */
/*                                                                   */
/* Load value in L into (HL).                                        */
LD_INTO_HL_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* LD     (HL), A      0x77      8                                   */
/*                                                                   */
/* Load value in A into (HL).                                        */
LD_INTO_HL_MACRO (A);

/* Op     Parameters   Code   Cycles                                 */
/* LD     (HL), n      0x36      12                                  */
/*                                                                   */
/* Load value at PC into (HL).                                      */
void LD_HL_n () {
    /* Immediate is stored at the PC. */
    int immediate_value = read_memory (PC);

    write_memory (pair (H, L), PC);

    /* Increment PC to get past the immediate. */
    increment_PC ();
}

#undef LD_INTO_HL_MACRO

#define LD_A_MACRO(arg_register) void LD_A_##arg_register () {        \
    A = arg_register;                                                 \
}

/* Op     Parameters   Code   Cycles                                 */
/* LD        A, B      0x78      4                                   */
/*                                                                   */
/* Load value in B into A.                                           */
LD_A_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* LD        A, C      0x79      4                                   */
/*                                                                   */
/* Load value in C into A.                                           */
LD_A_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* LD        A, D      0x7A      4                                   */
/*                                                                   */
/* Load value in D into A.                                           */
LD_A_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* LD        A, E      0x7B      4                                   */
/*                                                                   */
/* Load value in E into A.                                           */
LD_A_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* LD        A, H      0x7C      4                                   */
/*                                                                   */
/* Load value in H into A.                                           */
LD_A_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* LD        A, L      0x7D      4                                   */
/*                                                                   */
/* Load value in L into A.                                           */
LD_A_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* LD        A, (HL)   0x7E      8                                   */
/*                                                                   */
/* Load value at (HL) into A.                                        */
LD_FROM_HL_MACRO (A);

/* Op     Parameters   Code   Cycles                                 */
/* LD        A, A      0x7F      4                                   */
/*                                                                   */
/* Load value in A into A, i.e., do nothing.                         */
LD_A_MACRO (A);

/* Op     Parameters   Code   Cycles                                 */
/* LD        A, (BC)   0x0A      8                                   */
/*                                                                   */
/* Load value at (BC) into A.                                        */
void LD_A_BC () {
    A = read_memory (pair (B, C));
}

/* Op     Parameters   Code   Cycles                                 */
/* LD        A, (DE)   0x1A      8                                   */
/*                                                                   */
/* Load value at (DE) into A.                                        */
void LD_A_DE () {
    A = read_memory (pair (D, E));
}

/* Op     Parameters   Code   Cycles                                 */
/* LD        A, (nn)   0xFA     16                                   */
/*                                                                   */
/* Load value at (nn) into A.                                        */
void LD_A_nn () {
    int least_significant_byte, most_significant_byte;

    /* LSB comes first at PC. */
    least_significant_byte = read_memory (PC);

    /* Increment past LSB. */
    increment_PC ();

    /* MSB comes second at PC+1.*/
    most_significant_byte = read_memory (PC);

    /* Increment past MSB. */
    increment_PC ();

    A = read_memory (pair (most_significant_byte,
                           least_significant_byte));
}

/* Op     Parameters   Code   Cycles                                 */
/* LD        A, n      0x3E     8                                    */
/*                                                                   */
/* Load n into A.                                                    */
void LD_A_n () {
    /* Immediate is located at PC. */
    A = read_memory (PC);

    /* Increment past immediate value. */
    increment_PC ();
}

/* Op     Parameters   Code   Cycles                                 */
/* LD     (BC), A      0x02      8                                   */
/*                                                                   */
/* Load value in A into (BC).                                        */
void LD_BC_A () {
    write_memory (pair (B, C), A);
}

/* Op     Parameters   Code   Cycles                                 */
/* LD     (DE), A      0x12      8                                   */
/*                                                                   */
/* Load value in A into (DE).                                        */
void LD_DE_A () {
    write_memory (pair (D, E), A);
}

/* Op     Parameters   Code   Cycles                                 */
/* LD     (nn), A      0xEA     16                                   */
/*                                                                   */
/* Load value in A into (nn).                                        */
void LD_nn_A () {
    int least_significant_byte, most_significant_byte;

    /* LSB comes first at PC. */
    least_significant_byte = read_memory (PC);

    /* Increment past LSB. */
    increment_PC ();

    /* MSB comes second at PC+1.*/
    most_significant_byte = read_memory (PC);

    /* Increment past MSB. */
    increment_PC ();

    write_memory (pair (most_significant_byte,
                        least_significant_byte),
                  A);
}


#undef LD_A_MACRO

#undef LD_FROM_HL_MACRO

/*-------------------------------------------------------------------*/
/*--------------------------ADD-Operations---------------------------*/
/*-------------------------------------------------------------------*/
/* This is an ALU operation. It operates on the A register and       */
/* another register, placing the result in A.                        */
/*                                                                   */
/* If the sum is greater than 0xFF, set the carry flag to 1.         */
/* Otherwise, set the carry flag to 0.                               */
/*                                                                   */
/* If the sum of the lower nibbles is greater than 0xF, set          */
/* the half-carry flag to 1. Otherwise, set the half-carry           */
/* flag to 0.                                                        */
/*                                                                   */
/* If the result is zero, set the zero flag to 1. Otherwise,         */
/* set the zero flag to 0.                                           */
/*                                                                   */
/* Set the subtract flag to 0.                                       */
/*-------------------------------------------------------------------*/

#define ADD_MACRO(arg_register) void ADD_A_##arg_register () {        \
    int sum;                                                          \
                                                                      \
    sum = A + arg_register;                                           \
                                                                      \
    /* Set the carry flag. */                                         \
    F = (sum > BYTE_MASK)?                                            \
            (F |  CARRY_FLAG):                                        \
            (F & ~CARRY_FLAG);                                        \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (0 == (sum & BYTE_MASK))?                                     \
            (F |  ZERO_FLAG):                                         \
            (F & ~ZERO_FLAG);                                         \
                                                                      \
    /* Set the subtract flag. */                                      \
    F &= ~SUBTRACT_FLAG;                                              \
                                                                      \
    /* Set the half-carry flag. */                                    \
    F = ((A & LOWER_NIBBLE_MASK) + (arg_register & LOWER_NIBBLE_MASK) \
            > LOWER_NIBBLE_MASK)?                                     \
            (F |  HALF_CARRY_FLAG):                                   \
            (F & ~HALF_CARRY_FLAG);                                   \
                                                                      \
    /* Set the result. */                                             \
    A = sum & BYTE_MASK;                                              \
}

/* Op     Parameters   Code   Cycles                                 */
/* ADD        B        0x80      4                                   */
/*                                                                   */
/* Add the value in B to the value in A.                             */
ADD_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* ADD        C        0x81      4                                   */
/*                                                                   */
/* Add the value in C to the value in A.                             */
ADD_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* ADD        D        0x82      4                                   */
/*                                                                   */
/* Add the value in D to the value in A.                             */
ADD_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* ADD        E        0x83      4                                   */
/*                                                                   */
/* Add the value in E to the value in A.                             */
ADD_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* ADD        H        0x84      4                                   */
/*                                                                   */
/* Add the value in H to the value in A.                             */
ADD_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* ADD        L        0x85      4                                   */
/*                                                                   */
/* Add the value in L to the value in A.                             */
ADD_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* ADD       (HL)      0x86      8                                   */
/*                                                                   */
/* Add the value at (HL) to A.                                       */
void ADD_A_HL () {
    int sum, HL;
    HL = read_memory (pair (H, L));

    sum = A + HL;

    /* Set the carry flag. */
    F = (sum > BYTE_MASK)?
            (F |  CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (0 == (sum & BYTE_MASK))?
            (F |  ZERO_FLAG):
            (F & ~ZERO_FLAG);

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the half-carry flag. */
    F = ((A & LOWER_NIBBLE_MASK) + (HL & LOWER_NIBBLE_MASK)
            > LOWER_NIBBLE_MASK)?
            (F |  HALF_CARRY_FLAG):
            (F & ~HALF_CARRY_FLAG);

    /* Set the result. */
    A = sum & BYTE_MASK;
}

/* Op     Parameters   Code   Cycles                                 */
/* ADD        A        0x87      4                                   */
/*                                                                   */
/* Add the value in A to the value in A.                             */
ADD_MACRO (A);

#undef ADD_MACRO

/*-------------------------------------------------------------------*/
/*--------------------------ADC-Operations---------------------------*/
/*-------------------------------------------------------------------*/
/* This is an ALU operation. It operates on the A register and       */
/* another register, placing the result in A.                        */
/*                                                                   */
/* If the sum is greater than 0xFF, set the carry flag to 1.         */
/* Otherwise, set the carry flag to 0.                               */
/*                                                                   */
/* If the sum of the lower nibbles is greater than 0xF, set          */
/* the half-carry flag to 1. Otherwise, set the half-carry           */
/* flag to 0.                                                        */
/*                                                                   */
/* If the result is zero, set the zero flag to 1. Otherwise,         */
/* set the zero flag to 0.                                           */
/*                                                                   */
/* Set the subtract flag to 0.                                       */
/*-------------------------------------------------------------------*/

#define ADC_MACRO(arg_register) void ADC_A_##arg_register () {        \
    int sum;                                                          \
    int carry;                                                        \
                                                                      \
    carry = (F & CARRY_FLAG)? 1 : 0;                                  \
    sum = A + arg_register + carry;                                   \
                                                                      \
    /* Set the carry flag. */                                         \
    F = (sum > BYTE_MASK)?                                            \
            (F |  CARRY_FLAG):                                        \
            (F & ~CARRY_FLAG);                                        \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (0 == (sum & BYTE_MASK))?                                     \
            (F |  ZERO_FLAG):                                         \
            (F & ~ZERO_FLAG);                                         \
                                                                      \
    /* Set the subtract flag. */                                      \
    F &= ~SUBTRACT_FLAG;                                              \
                                                                      \
    /* Set the half-carry flag. */                                    \
    F = ((A & LOWER_NIBBLE_MASK) + (arg_register & LOWER_NIBBLE_MASK) \
            > LOWER_NIBBLE_MASK)?                                     \
            (F |  HALF_CARRY_FLAG):                                   \
            (F & ~HALF_CARRY_FLAG);                                   \
                                                                      \
    /* Set the result. */                                             \
    A = sum & BYTE_MASK;                                              \
}

/* Op     Parameters   Code   Cycles                                 */
/* ADC        B        0x88      4                                   */
/*                                                                   */
/* Add the value in B and the carry flag to the value in A.          */
ADC_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* ADC        C        0x89      4                                   */
/*                                                                   */
/* Add the value in C and the carry flag to the value in A.          */
ADC_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* ADC        D        0x8A      4                                   */
/*                                                                   */
/* Add the value in D and the carry flag to the value in A.          */
ADC_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* ADC        E        0x8B      4                                   */
/*                                                                   */
/* Add the value in E and the carry flag to the value in A.          */
ADC_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* ADC        H        0x8C      4                                   */
/*                                                                   */
/* Add the value in H and the carry flag to the value in A.          */
ADC_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* ADC        L        0x8D      4                                   */
/*                                                                   */
/* Add the value in L and the carry flag to the value in A.          */
ADC_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* ADC       (HL)      0x8E      8                                   */
/*                                                                   */
/* Add the value at (HL) and the carry flag to the value in A.       */
void ADC_A_HL () {
    int sum, carry, HL;
    HL = read_memory (pair (H, L));

    carry = (F & CARRY_FLAG)? 1 : 0;
    sum = A + HL + carry;

    /* Set the carry flag. */
    F = (sum > BYTE_MASK)?
            (F |  CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (0 == (sum & BYTE_MASK))?
            (F |  ZERO_FLAG):
            (F & ~ZERO_FLAG);

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the half-carry flag. */
    F = ((A & LOWER_NIBBLE_MASK) + (HL & LOWER_NIBBLE_MASK)
            > LOWER_NIBBLE_MASK)?
            (F |  HALF_CARRY_FLAG):
            (F & ~HALF_CARRY_FLAG);

    /* Set the result. */
    A = sum & BYTE_MASK;
}

/* Op     Parameters   Code   Cycles                                 */
/* ADC        A        0x8F      4                                   */
/*                                                                   */
/* Add the value in A and the carry flag to the value in A.          */
ADC_MACRO (A);

#undef ADC_MACRO

/*-------------------------------------------------------------------*/
/*--------------------------SUB-Operations---------------------------*/
/*-------------------------------------------------------------------*/
/* This is an ALU operation. It operates on the A register and       */
/* another register, placing the result in A.                        */
/*                                                                   */
/* For the sake of any borrows, keep in mind that the A register     */
/* holds the minuend. Thus, any borrows will occur in the A          */
/* register. Also note that, bits are numbered starting with 0.      */
/*                                                                   */
/* The carry flag is set if there is a borrow from a hypothetical    */
/* 8th bit to the 7th bit.                                           */
/*                                                                   */
/* The half-carry flag is set if there is a borrow from the 4th bit  */
/* to the 3rd bit.                                                   */
/*                                                                   */
/* If the result is zero, set the zero flag to 1. Otherwise,         */
/* set the zero flag to 0.                                           */
/*                                                                   */
/* Set the subtract flag to 1.                                       */
/*-------------------------------------------------------------------*/

#define SUB_MACRO(arg_register) void SUB_A_##arg_register () {        \
    int result;                                                       \
                                                                      \
    result = A - arg_register;                                        \
                                                                      \
    /* Set the carry flag. */                                         \
    F = (A < arg_register)?                                           \
            (F |  CARRY_FLAG):                                        \
            (F & ~CARRY_FLAG);                                        \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (0 == (result & BYTE_MASK))?                                  \
            (F |  ZERO_FLAG):                                         \
            (F & ~ZERO_FLAG);                                         \
                                                                      \
    /* Set the subtract flag. */                                      \
    F |= SUBTRACT_FLAG;                                               \
                                                                      \
    /* Set the half-carry flag. */                                    \
    F = ((A & LOWER_NIBBLE_MASK)                                      \
            < (arg_register & LOWER_NIBBLE_MASK))?                    \
            (F |  HALF_CARRY_FLAG):                                   \
            (F & ~HALF_CARRY_FLAG);                                   \
                                                                      \
    /* Set the result. */                                             \
    A = result & BYTE_MASK;                                           \
}

/* Op     Parameters   Code   Cycles                                 */
/* SUB       B         0x90      4                                   */
/*                                                                   */
/* Subtract the value in B from the value in A.                      */
SUB_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* SUB       C         0x91      4                                   */
/*                                                                   */
/* Subtract the value in C from the value in A.                      */
SUB_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* SUB       D         0x92      4                                   */
/*                                                                   */
/* Subtract the value in D from the value in A.                      */
SUB_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* SUB       E         0x93      4                                   */
/*                                                                   */
/* Subtract the value in E from the value in A.                      */
SUB_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* SUB       H         0x94      4                                   */
/*                                                                   */
/* Subtract the value in H from the value in A.                      */
SUB_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* SUB       L         0x95      4                                   */
/*                                                                   */
/* Subtract the value in L from the value in A.                      */
SUB_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* SUB      (HL)       0x96      8                                   */
/*                                                                   */
/* Load value at (HL) into A.                                        */
void SUB_A_HL () {
    int result, HL;
    HL = read_memory (pair (H, L));

    result = A - HL;

    /* Set the carry flag. */
    F = (A < HL)?
            (F |  CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (0 == (result & BYTE_MASK))?
            (F |  ZERO_FLAG):
            (F & ~ZERO_FLAG);

    /* Set the subtract flag. */
    F |= SUBTRACT_FLAG;

    /* Set the half-carry flag. */
    F = ((A & LOWER_NIBBLE_MASK)
            < (HL & LOWER_NIBBLE_MASK))?
            (F |  HALF_CARRY_FLAG):
            (F & ~HALF_CARRY_FLAG);

    /* Set the result. */
    A = result & BYTE_MASK;
}

/* Op     Parameters   Code   Cycles                                 */
/* SUB       A         0x97      4                                   */
/*                                                                   */
/* Subtract the value in A from the value in A.                      */
SUB_MACRO (A);

#undef SUB_MACRO

/*-------------------------------------------------------------------*/
/*--------------------------SBC-Operations---------------------------*/
/*-------------------------------------------------------------------*/
/* This is an ALU operation. It operates on the A register and       */
/* another register, placing the result in A.                        */
/*                                                                   */
/* For the sake of any borrows, keep in mind that the A register     */
/* holds the minuend. Thus, any borrows will occur in the A          */
/* register. Also note that, bits are numbered starting with 0.      */
/*                                                                   */
/* The carry flag is set if there is a borrow from a hypothetical    */
/* 8th bit to the 7th bit.                                           */
/*                                                                   */
/* The half-carry flag is set if there is a borrow from the 4th bit  */
/* to the 3rd bit.                                                   */
/*                                                                   */
/* If the result is zero, set the zero flag to 1. Otherwise,         */
/* set the zero flag to 0.                                           */
/*                                                                   */
/* Set the subtract flag to 1.                                       */
/*-------------------------------------------------------------------*/
#define SBC_MACRO(arg_register) void SBC_A_##arg_register () {        \
    int result;                                                       \
    int carry;                                                        \
                                                                      \
    carry = (F & CARRY_FLAG)? 1 : 0;                                  \
    result = A - arg_register - carry;                                \
                                                                      \
    /* Set the carry flag. */                                         \
    F = (A < arg_register)?                                           \
            (F |  CARRY_FLAG):                                        \
            (F & ~CARRY_FLAG);                                        \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (0 == (result & BYTE_MASK))?                                  \
            (F |  ZERO_FLAG):                                         \
            (F & ~ZERO_FLAG);                                         \
                                                                      \
    /* Set the subtract flag. */                                      \
    F |= SUBTRACT_FLAG;                                               \
                                                                      \
    /* Set the half-carry flag. */                                    \
    F = ((A & LOWER_NIBBLE_MASK)                                      \
            < (arg_register & LOWER_NIBBLE_MASK))?                    \
            (F |  HALF_CARRY_FLAG):                                   \
            (F & ~HALF_CARRY_FLAG);                                   \
                                                                      \
    /* Set the result. */                                             \
    A = result & BYTE_MASK;                                           \
}

/* Op     Parameters   Code   Cycles                                 */
/* SBC       B         0x98      4                                   */
/*                                                                   */
/* Subtract the value in B and the carry flag from the value in A.   */
SBC_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* SBC       C         0x99      4                                   */
/*                                                                   */
/* Subtract the value in C and the carry flag from the value in A.   */
SBC_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* SBC       D         0x9A      4                                   */
/*                                                                   */
/* Subtract the value in D and the carry flag from the value in A.   */
SBC_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* SBC       E         0x9B      4                                   */
/*                                                                   */
/* Subtract the value in E from the value in A.                      */
SBC_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* SBC       H         0x9C      4                                   */
/*                                                                   */
/* Subtract the value in H from the value in A.                      */
SBC_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* SBC       L         0x9D      4                                   */
/*                                                                   */
/* Subtract the value in L from the value in A.                      */
SBC_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* SBC      (HL)       0x9E      8                                   */
/*                                                                   */
/* Load value at (HL) and the carry flag into A.                     */
void SBC_A_HL () {
    int result, carry, HL;
    HL = read_memory (pair (H, L));

    carry = (F & CARRY_FLAG)? 1 : 0;
    result = A - HL - carry;

    /* Set the carry flag. */
    F = (A < HL)?
            (F |  CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (0 == (result & BYTE_MASK))?
            (F |  ZERO_FLAG):
            (F & ~ZERO_FLAG);

    /* Set the subtract flag. */
    F |= SUBTRACT_FLAG;

    /* Set the half-carry flag. */
    F = ((A & LOWER_NIBBLE_MASK)
            < (HL & LOWER_NIBBLE_MASK))?
            (F |  HALF_CARRY_FLAG):
            (F & ~HALF_CARRY_FLAG);

    /* Set the result. */
    A = result & BYTE_MASK;
}

/* Op     Parameters   Code   Cycles                                 */
/* SBC       A         0x9F      4                                   */
/*                                                                   */
/* Subtract the value in A and the carry flag from the value in A.   */
SBC_MACRO (A);

#undef SBC_MACRO

/*-------------------------------------------------------------------*/
/*--------------------------AND-Operations---------------------------*/
/*-------------------------------------------------------------------*/
/* This is an ALU operation. It operates on the A register and       */
/* another register, placing the result in A.                        */
/*                                                                   */
/* The carry flag is set to 0.                                       */
/*                                                                   */
/* The half-carry flag is set to 1.                                  */
/*                                                                   */
/* If the result is zero, set the zero flag to 1. Otherwise,         */
/* set the zero flag to 0.                                           */
/*                                                                   */
/* Set the subtract flag to 0.                                       */
/*-------------------------------------------------------------------*/
#define AND_MACRO(arg_register) void AND_A_##arg_register () {        \
    int result = A & arg_register;                                    \
                                                                      \
    /* Set the carry flag. */                                         \
    F &= ~(CARRY_FLAG);                                               \
                                                                      \
    /* Set the half-carry flag. */                                    \
    F |= HALF_CARRY_FLAG;                                             \
                                                                      \
    /* Set the subtract flag. */                                      \
    F &= ~(SUBTRACT_FLAG);                                            \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (result)? (F & ~(ZERO_FLAG)) : (F | ZERO_FLAG);               \
                                                                      \
    A = result;                                                       \
}

/* Op     Parameters   Code   Cycles                                 */
/* AND       B         0xA0      4                                   */
/*                                                                   */
/* And the contents of B and A together.                             */
AND_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* AND       C         0xA1      4                                   */
/*                                                                   */
/* And the contents of C and A together.                             */
AND_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* AND       D         0xA2      4                                   */
/*                                                                   */
/* And the contents of D and A together.                             */
AND_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* AND       E         0xA3      4                                   */
/*                                                                   */
/* And the contents of E and A together.                             */
AND_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* AND       H         0xA4      4                                   */
/*                                                                   */
/* And the contents of H and A together.                             */
AND_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* AND       L         0xA5      4                                   */
/*                                                                   */
/* And the contents of L and A together.                             */
AND_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* AND      (HL)       0xA6      8                                   */
/*                                                                   */
/* And the contents of (HL) and A together.                          */
void AND_A_HL () {
    int result = A & read_memory (pair (H, L));

    /* Set the carry flag. */
    F &= ~(CARRY_FLAG);

    /* Set the half-carry flag. */
    F |= HALF_CARRY_FLAG;

    /* Set the subtract flag. */
    F &= ~(SUBTRACT_FLAG);

    /* Set the zero flag. */
    F = (result)? (F & ~(ZERO_FLAG)) : (F | ZERO_FLAG);

    A = result;
}

/* Op     Parameters   Code   Cycles                                 */
/* AND       A         0xA7      4                                   */
/*                                                                   */
/* And the contents of A and A together.                             */
AND_MACRO (A);

#undef AND_MACRO

/*-------------------------------------------------------------------*/
/*--------------------------XOR-Operations---------------------------*/
/*-------------------------------------------------------------------*/
/* This is an ALU operation. It operates on the A register and       */
/* another register, placing the result in A.                        */
/*                                                                   */
/* The carry flag is set to 0.                                       */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* If the result is zero, set the zero flag to 1. Otherwise,         */
/* set the zero flag to 0.                                           */
/*                                                                   */
/* Set the subtract flag to 0.                                       */
/*-------------------------------------------------------------------*/
#define XOR_MACRO(arg_register) void XOR_A_##arg_register () {        \
    int result = A ^ arg_register;                                    \
                                                                      \
    /* Set the carry flag. */                                         \
    F &= ~(CARRY_FLAG);                                               \
                                                                      \
    /* Set the half-carry flag. */                                    \
    F &= ~(HALF_CARRY_FLAG);                                          \
                                                                      \
    /* Set the subtract flag. */                                      \
    F &= ~(SUBTRACT_FLAG);                                            \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (result)? (F & ~(ZERO_FLAG)) : (F | ZERO_FLAG);               \
                                                                      \
    A = result;                                                       \
}

/* Op     Parameters   Code   Cycles                                 */
/* XOR       B         0xA8      4                                   */
/*                                                                   */
/* Xor the contents of B and A together.                             */
XOR_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* XOR       C         0xA9      4                                   */
/*                                                                   */
/* Xor the contents of C and A together.                             */
XOR_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* XOR       D         0xAA      4                                   */
/*                                                                   */
/* Xor the contents of D and A together.                             */
XOR_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* XOR       E         0xAB      4                                   */
/*                                                                   */
/* Xor the contents of E and A together.                             */
XOR_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* XOR       H         0xAC      4                                   */
/*                                                                   */
/* Xor the contents of H and A together.                             */
XOR_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* XOR       L         0xAD      4                                   */
/*                                                                   */
/* Xor the contents of L and A together.                             */
XOR_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* XOR      (HL)       0xAE      8                                   */
/*                                                                   */
/* Xor the contents of (HL) and A together.                          */
void XOR_A_HL () {
    int result = A ^ read_memory (pair (H, L));

    /* Set the carry flag. */
    F &= ~(CARRY_FLAG);

    /* Set the half-carry flag. */
    F &= ~(HALF_CARRY_FLAG);

    /* Set the subtract flag. */
    F &= ~(SUBTRACT_FLAG);

    /* Set the zero flag. */
    F = (result)? (F & ~(ZERO_FLAG)) : (F | ZERO_FLAG);

    A = result;
}

/* Op     Parameters   Code   Cycles                                 */
/* XOR       A         0xAF      4                                   */
/*                                                                   */
/* Xor the contents of A and A together.                             */
XOR_MACRO (A);

#undef XOR_MACRO

/*-------------------------------------------------------------------*/
/*---------------------------OR-Operations---------------------------*/
/*-------------------------------------------------------------------*/
/* This is an ALU operation. It operates on the A register and       */
/* another register, placing the result in A.                        */
/*                                                                   */
/* The carry flag is set to 0.                                       */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* If the result is zero, set the zero flag to 1. Otherwise,         */
/* set the zero flag to 0.                                           */
/*                                                                   */
/* Set the subtract flag to 0.                                       */
/*-------------------------------------------------------------------*/
#define OR_MACRO(arg_register) void OR_A_##arg_register () {          \
    int result = A | arg_register;                                    \
                                                                      \
    /* Set the carry flag. */                                         \
    F &= ~(CARRY_FLAG);                                               \
                                                                      \
    /* Set the half-carry flag. */                                    \
    F &= ~(HALF_CARRY_FLAG);                                          \
                                                                      \
    /* Set the subtract flag. */                                      \
    F &= ~(SUBTRACT_FLAG);                                            \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (result)? (F & ~(ZERO_FLAG)) : (F | ZERO_FLAG);               \
                                                                      \
    A = result;                                                       \
}

/* Op     Parameters   Code   Cycles                                 */
/* OR       B         0xB0      4                                    */
/*                                                                   */
/* Or the contents of B and A together.                              */
OR_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* OR       C         0xB1      4                                    */
/*                                                                   */
/* Or the contents of C and A together.                              */
OR_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* OR       D         0xB2      4                                    */
/*                                                                   */
/* Or the contents of D and A together.                              */
OR_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* OR       E         0xB3      4                                    */
/*                                                                   */
/* Or the contents of E and A together.                              */
OR_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* OR       H         0xB4      4                                    */
/*                                                                   */
/* Or the contents of H and A together.                              */
OR_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* OR       L         0xB5      4                                    */
/*                                                                   */
/* Or the contents of L and A together.                              */
OR_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* OR      (HL)       0xB6      8                                    */
/*                                                                   */
/* Or the contents of (HL) and A together.                           */
void OR_A_HL () {
    int result = A | read_memory (pair (H, L));

    /* Set the carry flag. */
    F &= ~(CARRY_FLAG);

    /* Set the half-carry flag. */
    F &= ~(HALF_CARRY_FLAG);

    /* Set the subtract flag. */
    F &= ~(SUBTRACT_FLAG);

    /* Set the zero flag. */
    F = (result)? (F & ~(ZERO_FLAG)) : (F | ZERO_FLAG);

    A = result;
}

/* Op     Parameters   Code   Cycles                                 */
/* OR       A         0xB7      4                                    */
/*                                                                   */
/* Or the contents of A and A together.                              */
OR_MACRO (A);

#undef OR_MACRO

/*-------------------------------------------------------------------*/
/*---------------------------CP-Operations---------------------------*/
/*-------------------------------------------------------------------*/
/* CP is the same as SUB except it doesn't store the result in A.    */
/*                                                                   */
/* For the sake of any borrows, keep in mind that the A register     */
/* holds the minuend. Thus, any borrows will occur in the A          */
/* register. Also note that, bits are numbered starting with 0.      */
/*                                                                   */
/* The carry flag is set if there is a borrow from a hypothetical    */
/* 8th bit to the 7th bit.                                           */
/*                                                                   */
/* The half-carry flag is set if there is a borrow from the 4th bit  */
/* to the 3rd bit.                                                   */
/*                                                                   */
/* If the result is zero, set the zero flag to 1. Otherwise,         */
/* set the zero flag to 0.                                           */
/*                                                                   */
/* Set the subtract flag to 1.                                       */
/*-------------------------------------------------------------------*/

#define CP_MACRO(arg_register) void CP_A_##arg_register () {          \
    int result;                                                       \
                                                                      \
    result = A - arg_register;                                        \
                                                                      \
    /* Set the carry flag. */                                         \
    F = (A < arg_register)?                                           \
            (F |  CARRY_FLAG):                                        \
            (F & ~CARRY_FLAG);                                        \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (0 == (result & BYTE_MASK))?                                  \
            (F |  ZERO_FLAG):                                         \
            (F & ~ZERO_FLAG);                                         \
                                                                      \
    /* Set the subtract flag. */                                      \
    F |= SUBTRACT_FLAG;                                               \
                                                                      \
    /* Set the half-carry flag. */                                    \
    F = ((A & LOWER_NIBBLE_MASK)                                      \
            < (arg_register & LOWER_NIBBLE_MASK))?                    \
            (F |  HALF_CARRY_FLAG):                                   \
            (F & ~HALF_CARRY_FLAG);                                   \
}

/* Op     Parameters   Code   Cycles                                 */
/* CP        B         0xB8      4                                   */
/*                                                                   */
/* Compare the contents of B and A with each other.                  */
CP_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* CP        C         0xB9      4                                   */
/*                                                                   */
/* Compare the contents of C and A with each other.                  */
CP_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* CP        D         0xBA      4                                   */
/*                                                                   */
/* Compare the contents of D and A with each other.                  */
CP_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* CP        E         0xBB      4                                   */
/*                                                                   */
/* Compare the contents of E and A with each other.                  */
CP_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* CP        H         0xBC      4                                   */
/*                                                                   */
/* Compare the contents of H and A with each other.                  */
CP_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* CP        L         0xBD      4                                   */
/*                                                                   */
/* Compare the contents of L and A with each other.                  */
CP_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* CP       (HL)       0xBE      8                                   */
/*                                                                   */
/* Compare the contents of (HL) and A with each other.               */
void CP_A_HL () {
    int result, HL;
    HL = read_memory (pair (H, L));

    result = A - HL;

    /* Set the carry flag. */
    F = (A < HL)?
            (F |  CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (0 == (result & BYTE_MASK))?
            (F |  ZERO_FLAG):
            (F & ~ZERO_FLAG);

    /* Set the subtract flag. */
    F |= SUBTRACT_FLAG;

    /* Set the half-carry flag. */
    F = ((A & LOWER_NIBBLE_MASK)
            < (HL & LOWER_NIBBLE_MASK))?
            (F |  HALF_CARRY_FLAG):
            (F & ~HALF_CARRY_FLAG);
}

/* Op     Parameters   Code   Cycles                                 */
/* CP        A         0xBF      4                                   */
/*                                                                   */
/* Compare the contents of A and A with each other.                  */
CP_MACRO (A);

#undef CP_MACRO

/*-------------------------------------------------------------------*/
/*--------------------------INC-Operations---------------------------*/
/*-------------------------------------------------------------------*/
/* If the sum of the lower nibbles is greater than 0xF, set          */
/* the half-carry flag to 1. Otherwise, set the half-carry           */
/* flag to 0.                                                        */
/*                                                                   */
/* If the result is zero, set the zero flag to 1. Otherwise,         */
/* set the zero flag to 0.                                           */
/*                                                                   */
/* Set the subtract flag to 0.                                       */
/*-------------------------------------------------------------------*/

#define INC_MACRO(arg_register) void INC_##arg_register () {        \
    int sum;                                                          \
                                                                      \
    sum = arg_register + 1;                                           \
                                                                      \
    /* Set the carry flag. */                                         \
    F = (sum > BYTE_MASK)?                                            \
            (F |  CARRY_FLAG):                                        \
            (F & ~CARRY_FLAG);                                        \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (0 == (sum & BYTE_MASK))?                                     \
            (F |  ZERO_FLAG):                                         \
            (F & ~ZERO_FLAG);                                         \
                                                                      \
    /* Set the subtract flag. */                                      \
    F &= ~SUBTRACT_FLAG;                                              \
                                                                      \
    /* Set the half-carry flag. */                                    \
    F = ((arg_register & LOWER_NIBBLE_MASK) + 1                       \
            > LOWER_NIBBLE_MASK)?                                     \
            (F |  HALF_CARRY_FLAG):                                   \
            (F & ~HALF_CARRY_FLAG);                                   \
                                                                      \
    /* Set the result. */                                             \
    arg_register = sum & BYTE_MASK;                                   \
}

/* Op     Parameters   Code   Cycles                                 */
/* INC        B        0x04      4                                   */
/*                                                                   */
/* Increment the value in B by one.                                  */
INC_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* INC        C        0x0C      4                                   */
/*                                                                   */
/* Increment the value in C by one.                                  */
INC_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* INC        D        0x14      4                                   */
/*                                                                   */
/* Increment the value in D by one.                                  */
INC_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* INC        E        0x1C      4                                   */
/*                                                                   */
/* Increment the value in E by one.                                  */
INC_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* INC        H        0x24      4                                   */
/*                                                                   */
/* Increment the value in H by one.                                  */
INC_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* INC        L        0x2C      4                                   */
/*                                                                   */
/* Increment the value in L by one.                                  */
INC_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* INC       (HL)      0x34      8                                   */
/*                                                                   */
/* Increment the value at (HL) by 1.                                 */
void INC_HL () {
    int sum, HL;
    HL = read_memory (pair (H, L));

    sum = HL + 1;

    /* Set the carry flag. */
    F = (sum > BYTE_MASK)?
            (F |  CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (0 == (sum & BYTE_MASK))?
            (F |  ZERO_FLAG):
            (F & ~ZERO_FLAG);

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the half-carry flag. */
    F = ((HL & LOWER_NIBBLE_MASK) + 1
            > LOWER_NIBBLE_MASK)?
            (F |  HALF_CARRY_FLAG):
            (F & ~HALF_CARRY_FLAG);

    /* Set the result. */
    write_memory (pair (H,L), sum);
}

/* Op     Parameters   Code   Cycles                                 */
/* INC        A        0x3C      4                                   */
/*                                                                   */
/* Increment the value in A by one.                                  */
INC_MACRO (A);

#undef INC_MACRO

/*-------------------------------------------------------------------*/
/*--------------------------DEC-Operations---------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is set to 0.                                       */
/*                                                                   */
/* The half-carry flag is set if there is a borrow from the 4th bit  */
/* to the 3rd bit. This should only happen when the lower nibble     */
/* is 0.                                                             */
/*                                                                   */
/* If the result is zero, set the zero flag to 1. Otherwise,         */
/* set the zero flag to 0.                                           */
/*                                                                   */
/* Set the subtract flag to 1.                                       */
/*-------------------------------------------------------------------*/

#define DEC_MACRO(arg_register) void DEC_##arg_register () {          \
    int result;                                                       \
                                                                      \
    result = arg_register - 1;                                        \
                                                                      \
    /* Set the carry flag. */                                         \
    F &= ~(CARRY_FLAG);                                               \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (0 == (result & BYTE_MASK))?                                  \
            (F |  ZERO_FLAG):                                         \
            (F & ~ZERO_FLAG);                                         \
                                                                      \
    /* Set the subtract flag. */                                      \
    F |= SUBTRACT_FLAG;                                               \
                                                                      \
    /* Set the half-carry flag. */                                    \
    F = (arg_register & LOWER_NIBBLE_MASK)?                           \
            (F & ~HALF_CARRY_FLAG):                                   \
            (F |  HALF_CARRY_FLAG);                                   \
                                                                      \
    /* Set the result. */                                             \
    arg_register = result & BYTE_MASK;                                \
}

/* Op     Parameters   Code   Cycles                                 */
/* DEC        B        0x05      4                                   */
/*                                                                   */
/* Decrement the value in B by one.                                  */
DEC_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* DEC        C        0x0D      4                                   */
/*                                                                   */
/* Decrement the value in C by one.                                  */
DEC_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* DEC        D        0x15      4                                   */
/*                                                                   */
/* Decrement the value in D by one.                                  */
DEC_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* DEC        E        0x1D      4                                   */
/*                                                                   */
/* Decrement the value in E by one.                                  */
DEC_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* DEC        H        0x25      4                                   */
/*                                                                   */
/* Decrement the value in H by one.                                  */
DEC_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* DEC        L        0x2D      4                                   */
/*                                                                   */
/* Decrement the value in L by one.                                  */
DEC_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* DEC       (HL)      0x35      8                                   */
/*                                                                   */
/* Decrement the value at (HL) by 1.                                 */
void DEC_HL () {
    int result, HL;

    HL = read_memory (pair (H, L));
    result = HL - 1;

    /* Set the carry flag. */
    F &= ~(CARRY_FLAG);

    /* Set the zero flag. */
    F = (0 == (result & BYTE_MASK))?
            (F |  ZERO_FLAG):
            (F & ~ZERO_FLAG);

    /* Set the subtract flag. */
    F |= SUBTRACT_FLAG;

    /* Set the half-carry flag. */
    F = (HL & LOWER_NIBBLE_MASK)?
            (F & ~HALF_CARRY_FLAG):
            (F |  HALF_CARRY_FLAG);

    /* Set the result. */
    write_memory (pair (H, L), result);
}

/* Op     Parameters   Code   Cycles                                 */
/* DEC        A        0x3D      4                                   */
/*                                                                   */
/* Decrement the value in A by one.                                  */
DEC_MACRO (A);

#undef DEC_MACRO

/*-------------------------------------------------------------------*/
/*-------------------------ADD-16-Operations-------------------------*/
/*-------------------------------------------------------------------*/
/* If the sum is greater than 0xFFFF, set the carry flag to 1.       */
/* Otherwise, set the carry flag to 0.                               */
/*                                                                   */
/* If the sum of the lower 12 bits is greater than 0xFFF, set        */
/* the half-carry flag to 1. Otherwise, set the half-carry           */
/* flag to 0.                                                        */
/*                                                                   */
/* The zero flag is not affected.                                    */
/*                                                                   */
/* Set the subtract flag to 0.                                       */
/*-------------------------------------------------------------------*/

#define ADD_16_MACRO(arg_register1, arg_register2)                    \
void ADD_HL_##arg_register1##arg_register2 () {                       \
    int HL;                                                           \
    int arg_addend;                                                   \
    int sum;                                                          \
                                                                      \
    /* Grab the paired registers. */                                  \
    HL = pair (H, L);                                                 \
    arg_addend = pair (arg_register1, arg_register2);                 \
                                                                      \
    sum = HL + arg_addend;                                            \
                                                                      \
    /* Set the carry flag. */                                         \
    F = (sum > TWO_BYTE_MASK)?                                        \
            (F |  CARRY_FLAG):                                        \
            (F & ~CARRY_FLAG);                                        \
                                                                      \
    /* Set the subtract flag. */                                      \
    F &= ~SUBTRACT_FLAG;                                              \
                                                                      \
    /* Set the half-carry flag. Assume the registers */               \
    /* are the correct length, i.e., just 8 bits.    */               \
    F = ((HL & TWELVE_BIT_MASK) + (arg_addend & TWELVE_BIT_MASK)      \
            > TWELVE_BIT_MASK)?                                       \
            (F |  HALF_CARRY_FLAG):                                   \
            (F & ~HALF_CARRY_FLAG);                                   \
                                                                      \
    /* Set the result. */                                             \
    L = sum & BYTE_MASK;                                              \
    H = (sum >> 8) & BYTE_MASK;                                       \
}

/* Op     Parameters   Code   Cycles                                 */
/* ADD        BC       0x09      8                                   */
/*                                                                   */
/* Add the value in BC to the value in HL.                           */
ADD_16_MACRO (B, C);

/* Op     Parameters   Code   Cycles                                 */
/* ADD        DE       0x19      8                                   */
/*                                                                   */
/* Add the value in DE to the value in HL.                           */
ADD_16_MACRO (D, E);

/* Op     Parameters   Code   Cycles                                 */
/* ADD        HL       0x29      8                                   */
/*                                                                   */
/* Add the value in HL to the value in HL.                           */
ADD_16_MACRO (H, L);

/* Op     Parameters   Code   Cycles                                 */
/* ADD        SP       0x39      8                                   */
/*                                                                   */
/* Add the value in SP to the value in HL.                           */
void ADD_HL_SP () {
    int HL;
    int sum;

    /* Grab the paired registers. */
    HL = pair (H, L);

    sum = HL + SP;

    /* Set the carry flag. */
    F = (sum > TWO_BYTE_MASK)?
            (F |  CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the half-carry flag. Assume the registers */
    /* are the correct length, i.e., just 8 bits.    */
    F = ((HL & TWELVE_BIT_MASK) + (SP & TWELVE_BIT_MASK)
            > TWELVE_BIT_MASK)?
            (F |  HALF_CARRY_FLAG):
            (F & ~HALF_CARRY_FLAG);

    /* Set the result. */
    L = sum & BYTE_MASK;
    H = (sum >> 8) & BYTE_MASK;
}

#undef ADD_16_MACRO

/*-------------------------------------------------------------------*/
/*-------------------------INC-16-Operations-------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is unaffected.                                     */
/*                                                                   */
/* The half-carry flag is unaffected.                                */
/*                                                                   */
/* The zero flag is not affected.                                    */
/*                                                                   */
/* The subtract flag is unaffected.                                  */
/*-------------------------------------------------------------------*/

#define INC_16_MACRO(arg_register1, arg_register2)                    \
void INC16_##arg_register1##arg_register2 () {                        \
    int arg_addend;                                                   \
    int sum;                                                          \
                                                                      \
    /* Grab the paired registers. */                                  \
    arg_addend = pair (arg_register1, arg_register2);                 \
                                                                      \
    sum = arg_addend + 1;                                             \
                                                                      \
    /* Set the result. */                                             \
    arg_register2 = sum & BYTE_MASK;                                  \
    arg_register1 = (sum >> 8) & BYTE_MASK;                           \
}

/* Op     Parameters   Code   Cycles                                 */
/* INC        BC       0x03      8                                   */
/*                                                                   */
/* Increase the value in BC by 1.                                    */
INC_16_MACRO (B, C);

/* Op     Parameters   Code   Cycles                                 */
/* INC        DE       0x13      8                                   */
/*                                                                   */
/* Increase the value in DE by 1.                                    */
INC_16_MACRO (D, E);

/* Op     Parameters   Code   Cycles                                 */
/* INC        HL       0x23      8                                   */
/*                                                                   */
/* Increase the value in HL by 1.                                    */
INC_16_MACRO (H, L);

/* Op     Parameters   Code   Cycles                                 */
/* INC        SP       0x33      8                                   */
/*                                                                   */
/* Increase the value in SP by 1.                                    */
void INC16_SP () {
    /* Set the result. */
    SP = (SP + 1) & TWO_BYTE_MASK;
}

#undef INC_16_MACRO

/*-------------------------------------------------------------------*/
/*-------------------------DEC-16-Operations-------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is unaffected.                                     */
/*                                                                   */
/* The half-carry flag is unaffected.                                */
/*                                                                   */
/* The zero flag is not affected.                                    */
/*                                                                   */
/* The subtract flag is unaffected.                                  */
/*-------------------------------------------------------------------*/

#define DEC_16_MACRO(arg_register1, arg_register2)                    \
void DEC16_##arg_register1##arg_register2 () {                        \
    int arg_minuend;                                                  \
    int result;                                                       \
                                                                      \
    /* Grab the paired registers. */                                  \
    arg_minuend = pair (arg_register1, arg_register2);                \
                                                                      \
    result = arg_minuend - 1;                                         \
                                                                      \
    /* Set the result. */                                             \
    arg_register2 = result & BYTE_MASK;                               \
    arg_register1 = (result >> 8) & BYTE_MASK;                        \
}

/* Op     Parameters   Code   Cycles                                 */
/* DEC        BC       0x0B      8                                   */
/*                                                                   */
/* Decrement the value in BC by 1.                                   */
DEC_16_MACRO (B, C);

/* Op     Parameters   Code   Cycles                                 */
/* DEC        DE       0x1B      8                                   */
/*                                                                   */
/* Decrement the value in DE by 1.                                   */
DEC_16_MACRO (D, E);

/* Op     Parameters   Code   Cycles                                 */
/* DEC        HL       0x2B      8                                   */
/*                                                                   */
/* Decrement the value in HL by 1.                                   */
DEC_16_MACRO (H, L);

/* Op     Parameters   Code   Cycles                                 */
/* DEC        SP       0x3B      8                                   */
/*                                                                   */
/* Decrement the value in SP by 1.                                   */
void DEC16_SP () {
    /* Set the result. */
    SP = (SP - 1) & TWO_BYTE_MASK;
}

#undef DEC_16_MACRO

/*-------------------------------------------------------------------*/
/*---------------------------SWAP-Operations-------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is set to 0.                                       */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* The zero flag is set to 1 if the result is 0. Otherwise,          */
/* it is set to 0.                                                   */
/*                                                                   */
/* The subtract flag is set to 0.                                    */
/*-------------------------------------------------------------------*/
#define SWAP_MACRO(arg_register) void SWAP_##arg_register () {        \
    int upper_nibble, lower_nibble;                                   \
                                                                      \
    /* Shift the upper nibble down. */                                \
    upper_nibble = (arg_register & BYTE_MASK) >> 4;                   \
    /* Shift the lower nibble up. */                                  \
    lower_nibble = arg_register << 4;                                 \
                                                                      \
    arg_register = (lower_nibble | upper_nibble) & BYTE_MASK;         \
                                                                      \
    /* Set the carry flag. */                                         \
    F &= ~CARRY_FLAG;                                                 \
                                                                      \
    /* Set the half-carry flag. */                                    \
    F &= ~HALF_CARRY_FLAG;                                            \
                                                                      \
    /* Set the subtract flag. */                                      \
    F &= ~ZERO_FLAG;                                                  \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (arg_register == 0)? (F | ZERO_FLAG) : (F & ~ZERO_FLAG);      \
                                                                      \
}

/* Op     Parameters   Code   Cycles                                 */
/* SWAP       B      0xCB 0x30   8                                   */
/*                                                                   */
/* Swap the upper and lower nibbles of the contents of B.            */
SWAP_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* SWAP       C      0xCB 0x31   8                                   */
/*                                                                   */
/* Swap the upper and lower nibbles of the contents of C.            */
SWAP_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* SWAP       D      0xCB 0x32   8                                   */
/*                                                                   */
/* Swap the upper and lower nibbles of the contents of D.            */
SWAP_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* SWAP       E      0xCB 0x33   8                                   */
/*                                                                   */
/* Swap the upper and lower nibbles of the contents of E.            */
SWAP_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* SWAP       H      0xCB 0x34   8                                   */
/*                                                                   */
/* Swap the upper and lower nibbles of the contents of H.            */
SWAP_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* SWAP       L      0xCB 0x35   8                                   */
/*                                                                   */
/* Swap the upper and lower nibbles of the contents of L.            */
SWAP_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* SWAP     (HL)     0xCB 0x36   16                                  */
/*                                                                   */
/* Swap the upper and lower nibbles of the contents at HL.           */
void SWAP_HL () {
    int upper_nibble, lower_nibble;
    int HL = read_memory (pair (H, L));

    /* Shift the upper nibble down. */
    upper_nibble = (HL & BYTE_MASK) >> 4;
    /* Shift the lower nibble up. */
    lower_nibble = HL << 4;

    HL = (lower_nibble | upper_nibble) & BYTE_MASK;

    /* Set the carry flag. */
    F &= ~CARRY_FLAG;

    /* Set the half-carry flag. */
    F &= ~HALF_CARRY_FLAG;

    /* Set the subtract flag. */
    F &= ~ZERO_FLAG;

    /* Set the zero flag. */
    F = (HL == 0)? (F | ZERO_FLAG) : (F & ~ZERO_FLAG);

    write_memory (pair (H, L), HL);
}

/* Op     Parameters   Code   Cycles                                 */
/* SWAP       A      0xCB 0x37   8                                   */
/*                                                                   */
/* Swap the upper and lower nibbles of the contents of A.            */
SWAP_MACRO (A);

#undef SWAP_MACRO

/*-------------------------------------------------------------------*/
/*----------------------------DAA-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is set based on the operation as described below.  */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* The zero flag is set to 1 if the result is 0. Otherwise,          */
/* it is set to 0.                                                   */
/*                                                                   */
/* The subtract flag is unaffected.                                  */
/*-------------------------------------------------------------------*/
/*    -OP INFORMATION-                                               */
/* DAA adjusts the A register such that the resulting contents are   */
/* valid binary-coded decimal (BCD). It does this by checking each   */
/* nibble to see if it exceeds 0x9 or by checking the half-carry     */
/* or carry flag (dependent upon the nibble) to see if a carry has   */
/* occurred. If either has occurred, add 0x6 to the corresponding    */
/* nibble. This will adjust it to be the correct value.              */
/*                                                                   */
/* I'm not sure if the CPU also adjusts BCD subtraction, so I'll     */
/* just put it in too. This will be a subtraction of 0x6 instead.    */
/*-------------------------------------------------------------------*/

/* Op     Parameters   Code   Cycles                                 */
/* DAA                 0x27      4                                   */
/*                                                                   */
/* Adjusts A such that it contains a valid representation of         */
/* Binary Coded Decimal.                                             */
void DAA () {

    /* Check if we're adjusting subtraction. */
    if (F & SUBTRACT_FLAG) {

        /* Adjust the upper nibble if needed. */
        if ((F & CARRY_FLAG) ||
            (A >> 4) > 0x9) {
            A -= 0x60;
        }

        /* Adjust the lower nibble if needed. */
        if ((F & HALF_CARRY_FLAG) ||
            (A & LOWER_NIBBLE_MASK) > 0x9) {
            A -= 0x6;
        }

    /* Otherwise, we're adjusting addition. */
    } else {

        /* Adjust the lower nibble if needed. */
        if ((F & HALF_CARRY_FLAG) ||
            ((A & LOWER_NIBBLE_MASK) > 0x9)) {
            A += 0x6;
        }

        /* Adjust the upper nibble if needed. */
        if ((F & CARRY_FLAG) ||
            ((A >> 4) > 0x9)) {
            A += 0x60;
        }
    }

    /* Set the carry flag. */
    F = (A > BYTE_MASK)?
            (F | CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the half-carry flag. */
    F &= ~HALF_CARRY_FLAG;

    /* Set result. */
    A = A & BYTE_MASK;

    /* Set the zero flag. */
    F = (A == 0)?
            (F | ZERO_FLAG):
            (F & ~ZERO_FLAG);
}


/*-------------------------------------------------------------------*/
/*----------------------------CPL-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is not affected.                                   */
/*                                                                   */
/* The half-carry flag is set to 1.                                  */
/*                                                                   */
/* The zero flag is not affected.                                    */
/*                                                                   */
/* The subtract flag is set to 1.                                    */
/*-------------------------------------------------------------------*/

/* Op     Parameters   Code   Cycles                                 */
/* CPL                 0x2F      4                                   */
/*                                                                   */
/* Complements the A register.                                       */
void CPL () {

    /* Set the half-carry flag. */
    F |= HALF_CARRY_FLAG;

    /* Set the subtract flag. */
    F |= SUBTRACT_FLAG;

    /* Set the result. */
    A = (~A) & BYTE_MASK;
}


/*-------------------------------------------------------------------*/
/*----------------------------CCF-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is not affected.                                   */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* The zero flag is not affected.                                    */
/*                                                                   */
/* The subtract flag is set to 0.                                    */
/*-------------------------------------------------------------------*/

/* Op     Parameters   Code   Cycles                                 */
/* CCF                 0x3F      4                                   */
/*                                                                   */
/* Complements the carry flag.                                       */
void CCF () {

    /* Set the half-carry flag. */
    F &= ~HALF_CARRY_FLAG;

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the result. */
    F = (F & CARRY_FLAG)?
            (F & ~CARRY_FLAG):
            (F | CARRY_FLAG);
}


/*-------------------------------------------------------------------*/
/*----------------------------SCF-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is set to 1.                                       */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* The zero flag is not affected.                                    */
/*                                                                   */
/* The subtract flag is set to 0.                                    */
/*-------------------------------------------------------------------*/

/* Op     Parameters   Code   Cycles                                 */
/* SCF                 0x37      4                                   */
/*                                                                   */
/* Sets the carry flag.                                              */
void SCF () {

    /* Set the half-carry flag. */
    F &= ~HALF_CARRY_FLAG;

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the result. */
    F = F | CARRY_FLAG;
}

/*-------------------------------------------------------------------*/
/*----------------------------NOP-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is not affected.                                   */
/*                                                                   */
/* The half-carry flag is not affected.                              */
/*                                                                   */
/* The zero flag is not affected.                                    */
/*                                                                   */
/* The subtract flag is not affected.                                */
/*-------------------------------------------------------------------*/

/* Op     Parameters   Code   Cycles                                 */
/* NOP                 0x00      4                                   */
/*                                                                   */
/* No Operation.                                                     */
void NOP () {
    return;
}


/*-------------------------------------------------------------------*/
/*---------------------------HALT-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is not affected.                                   */
/*                                                                   */
/* The half-carry flag is not affected.                              */
/*                                                                   */
/* The zero flag is not affected.                                    */
/*                                                                   */
/* The subtract flag is not affected.                                */
/*-------------------------------------------------------------------*/

/* Op     Parameters   Code   Cycles                                 */
/* HALT                0x76      4                                   */
/*                                                                   */
/* Halts the CPU until an interrupt occurs.                          */
void HALT () {
    /* TODO; NOT IMPLEMENTED. */
    return;
}


/*-------------------------------------------------------------------*/
/*---------------------------STOP-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is not affected.                                   */
/*                                                                   */
/* The half-carry flag is not affected.                              */
/*                                                                   */
/* The zero flag is not affected.                                    */
/*                                                                   */
/* The subtract flag is not affected.                                */
/*-------------------------------------------------------------------*/

/* Op     Parameters   Code   Cycles                                 */
/* STOP              0x10 0x00   4                                   */
/*                                                                   */
/* Halts the CPU and the display until a button is pressed.          */
void STOP () {
    /* TODO; NOT IMPLEMENTED. */
    /* Check if the next instruction is a nop. */
    /* Otherwise, just return. */
    return;
}


/*-------------------------------------------------------------------*/
/*-----------------------------DI-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is not affected.                                   */
/*                                                                   */
/* The half-carry flag is not affected.                              */
/*                                                                   */
/* The zero flag is not affected.                                    */
/*                                                                   */
/* The subtract flag is not affected.                                */
/*-------------------------------------------------------------------*/

/* Op     Parameters   Code   Cycles                                 */
/* DI                  0xF3      4                                   */
/*                                                                   */
/* Disables interrupts after the next instruction is executed.       */
void DI () {
    /* TODO; NOT IMPLEMENTED. */
    return;
}


/*-------------------------------------------------------------------*/
/*-----------------------------EI-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is not affected.                                   */
/*                                                                   */
/* The half-carry flag is not affected.                              */
/*                                                                   */
/* The zero flag is not affected.                                    */
/*                                                                   */
/* The subtract flag is not affected.                                */
/*-------------------------------------------------------------------*/

/* Op     Parameters   Code   Cycles                                 */
/* EI                  0xFB      4                                   */
/*                                                                   */
/* Enables interrupts after the next instruction is executed.        */
void EI () {
    /* TODO; NOT IMPLEMENTED. */
    return;
}


/* TODO: all of these rotations on A could be refactored to use the
 * generalized 0xCB rotations. */

/*-------------------------------------------------------------------*/
/*---------------------------RLCA-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag holds the value of the old 7th bit.                */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* The zero flag is set to 1 if the result is 0. Otherwise,          */
/* it is set to 0.                                                   */
/*                                                                   */
/* The subtract flag is set to 0.                                    */
/*-------------------------------------------------------------------*/

/* Op     Parameters   Code   Cycles                                 */
/* RLCA                0x07      4                                   */
/*                                                                   */
/* Rotates left into the carry flag. The carry flag and the 0th bit  */
/* become the value of the old 7th bit.                              */
void RLCA () {
    int previous_7th_bit = A >> 7;

    /* Shift everything down 1. */
    A = (A << 1) & BYTE_MASK;

    /* Shift the previous 7th bit in. */
    A = (previous_7th_bit)?
            (A | 0x1):
            (A & ~0x1);

    /* Set the carry flag. */
    F = (previous_7th_bit)?
            (F | CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (A)?
            (F & ~ZERO_FLAG):
            (F | ZERO_FLAG);

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the half-carryflag. */
    F &= ~HALF_CARRY_FLAG;
}


/*-------------------------------------------------------------------*/
/*----------------------------RLA-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag holds the value of the old 7th bit.                */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* The zero flag is set to 1 if the result is 0. Otherwise,          */
/* it is set to 0.                                                   */
/*                                                                   */
/* The subtract flag is set to 0.                                    */
/*-------------------------------------------------------------------*/

/* Op     Parameters   Code   Cycles                                 */
/* RLA                 0x17      4                                   */
/*                                                                   */
/* Rotates left into the carry flag. The old carry flag becomes bit  */
/* 0, and the new carry flag is the old bit 7.                       */
void RLA () {
    int previous_7th_bit = A >> 7;

    /* Shift everything down 1. */
    A = (A << 1) & BYTE_MASK;

    /* Shift the carry flag in. */
    A = (F & CARRY_FLAG)?
            (A | 0x1):
            (A & ~0x1);

    /* Set the carry flag. */
    F = (previous_7th_bit)?
            (F | CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (A)?
            (F & ~ZERO_FLAG):
            (F | ZERO_FLAG);

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the half-carryflag. */
    F &= ~HALF_CARRY_FLAG;
}


/*-------------------------------------------------------------------*/
/*---------------------------RRCA-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag holds the value of the old 0th bit.                */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* The zero flag is set to 1 if the result is 0. Otherwise,          */
/* it is set to 0.                                                   */
/*                                                                   */
/* The subtract flag is set to 0.                                    */
/*-------------------------------------------------------------------*/

/* Op     Parameters   Code   Cycles                                 */
/* RRCA                0x0F      4                                   */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag and the 7th bit */
/* become the value of the old 0th bit.                              */
void RRCA () {
    int previous_0th_bit = A & 0x1;

    /* Shift everything up 1. */
    A = (A & BYTE_MASK) >> 1;

    /* Shift the previous 0th bit in. */
    A = (previous_0th_bit)?
            (A | 0x80):
            (A & ~0x80);

    /* Set the carry flag. */
    F = (previous_0th_bit)?
            (F | CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (A)?
            (F & ~ZERO_FLAG):
            (F | ZERO_FLAG);

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the half-carryflag. */
    F &= ~HALF_CARRY_FLAG;
}


/*-------------------------------------------------------------------*/
/*----------------------------RRA-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag holds the value of the old 0th bit.                */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* The zero flag is set to 1 if the result is 0. Otherwise,          */
/* it is set to 0.                                                   */
/*                                                                   */
/* The subtract flag is set to 0.                                    */
/*-------------------------------------------------------------------*/

/* Op     Parameters   Code   Cycles                                 */
/* RRA                 0x1F      4                                   */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag becomes the     */
/* previous 0 bit and the 7th bit becomes the old carry flag.        */
void RRA () {
    int previous_0th_bit = A & 0x1;

    /* Shift everything up 1. */
    A = (A & BYTE_MASK) >> 1;

    /* Shift the carry flag in. */
    A = (F & CARRY_FLAG)?
            (A | 0x80):
            (A & ~0x80);

    /* Set the carry flag. */
    F = (previous_0th_bit)?
            (F | CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (A)?
            (F & ~ZERO_FLAG):
            (F | ZERO_FLAG);

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the half-carryflag. */
    F &= ~HALF_CARRY_FLAG;
}


/*-------------------------------------------------------------------*/
/*----------------------------RLC-Operations-------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag holds the value of the old 7th bit.                */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* The zero flag is set to 1 if the result is 0. Otherwise,          */
/* it is set to 0.                                                   */
/*                                                                   */
/* The subtract flag is set to 0.                                    */
/*-------------------------------------------------------------------*/
#define RLC_MACRO(arg_register) void RLC_##arg_register () {          \
    int previous_7th_bit = arg_register >> 7;                         \
                                                                      \
    /* Shift everything down 1. */                                    \
    arg_register = (arg_register << 1) & BYTE_MASK;                   \
                                                                      \
    /* Shift the 7th bit in. */                                       \
    arg_register = (previous_7th_bit)?                                \
            (arg_register | 0x1):                                     \
            (arg_register & ~0x1);                                    \
                                                                      \
    /* Set the carry flag. */                                         \
    F = (previous_7th_bit)?                                           \
            (F | CARRY_FLAG):                                         \
            (F & ~CARRY_FLAG);                                        \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (arg_register)?                                               \
            (F & ~ZERO_FLAG):                                         \
            (F | ZERO_FLAG);                                          \
                                                                      \
    /* Set the subtract flag. */                                      \
    F &= ~SUBTRACT_FLAG;                                              \
                                                                      \
    /* Set the half-carryflag. */                                     \
    F &= ~HALF_CARRY_FLAG;                                            \
}

/* Op     Parameters   Code   Cycles                                 */
/* RLC        B      0xCB 0x00   8                                   */
/*                                                                   */
/* Rotates left into the carry flag. The carry flag and the 0th bit  */
/* become the value of the old 7th bit.                              */
RLC_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* RLC        C      0xCB 0x01   8                                   */
/*                                                                   */
/* Rotates left into the carry flag. The carry flag and the 0th bit  */
/* become the value of the old 7th bit.                              */
RLC_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* RLC        D      0xCB 0x02   8                                   */
/*                                                                   */
/* Rotates left into the carry flag. The carry flag and the 0th bit  */
/* become the value of the old 7th bit.                              */
RLC_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* RLC        E      0xCB 0x03   8                                   */
/*                                                                   */
/* Rotates left into the carry flag. The carry flag and the 0th bit  */
/* become the value of the old 7th bit.                              */
RLC_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* RLC        H      0xCB 0x04   8                                   */
/*                                                                   */
/* Rotates left into the carry flag. The carry flag and the 0th bit  */
/* become the value of the old 7th bit.                              */
RLC_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* RLC        L      0xCB 0x05   8                                   */
/*                                                                   */
/* Rotates left into the carry flag. The carry flag and the 0th bit  */
/* become the value of the old 7th bit.                              */
RLC_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* RLC       HL      0xCB 0x06   16                                  */
/*                                                                   */
/* Rotates left into the carry flag. The carry flag and the 0th bit  */
/* become the value of the old 7th bit.                              */
void RLC_HL () {
    /* Get the current value. */
    int HL = read_memory (pair (H, L));
    int previous_7th_bit = HL >> 7;

    /* Shift everything down 1. */
    HL = (HL << 1) & BYTE_MASK;

    /* Shift the 7th bit in. */
    HL = (previous_7th_bit)?
            (HL | 0x1):
            (HL & ~0x1);

    /* Set the carry flag. */
    F = (previous_7th_bit)?
            (F | CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (HL)?
            (F & ~ZERO_FLAG):
            (F | ZERO_FLAG);

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the half-carryflag. */
    F &= ~HALF_CARRY_FLAG;

    /* Write new value out. */
    write_memory (pair (H, L), HL);
}

/* Op     Parameters   Code   Cycles                                 */
/* RLC        A      0xCB 0x07   8                                   */
/*                                                                   */
/* Rotates left into the carry flag. The carry flag and the 0th bit  */
/* become the value of the old 7th bit.                              */
RLC_MACRO (A);

#undef RLC_MACRO

/*-------------------------------------------------------------------*/
/*-----------------------------RL-Operations-------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag holds the value of the old 7th bit.                */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* The zero flag is set to 1 if the result is 0. Otherwise,          */
/* it is set to 0.                                                   */
/*                                                                   */
/* The subtract flag is set to 0.                                    */
/*-------------------------------------------------------------------*/
#define RL_MACRO(arg_register) void RL_##arg_register () {            \
    int previous_7th_bit = arg_register >> 7;                         \
                                                                      \
    /* Shift everything down 1. */                                    \
    arg_register = (arg_register << 1) & BYTE_MASK;                   \
                                                                      \
    /* Shift the carry flag in. */                                    \
    arg_register = (F & CARRY_FLAG)?                                  \
            (arg_register | 0x1):                                     \
            (arg_register & ~0x1);                                    \
                                                                      \
    /* Set the carry flag. */                                         \
    F = (previous_7th_bit)?                                           \
            (F | CARRY_FLAG):                                         \
            (F & ~CARRY_FLAG);                                        \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (arg_register)?                                               \
            (F & ~ZERO_FLAG):                                         \
            (F | ZERO_FLAG);                                          \
                                                                      \
    /* Set the subtract flag. */                                      \
    F &= ~SUBTRACT_FLAG;                                              \
                                                                      \
    /* Set the half-carryflag. */                                     \
    F &= ~HALF_CARRY_FLAG;                                            \
}

/* Op     Parameters   Code   Cycles                                 */
/* RL        B      0xCB 0x10   8                                    */
/*                                                                   */
/* Rotates left into the carry flag. The old carry flag becomes bit  */
/* 0, and the new carry flag is the old bit 7.                       */
RL_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* RL        C      0xCB 0x11   8                                    */
/*                                                                   */
/* Rotates left into the carry flag. The old carry flag becomes bit  */
/* 0, and the new carry flag is the old bit 7.                       */
RL_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* RL        D      0xCB 0x12   8                                    */
/*                                                                   */
/* Rotates left into the carry flag. The old carry flag becomes bit  */
/* 0, and the new carry flag is the old bit 7.                       */
RL_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* RL        E      0xCB 0x13   8                                    */
/*                                                                   */
/* Rotates left into the carry flag. The old carry flag becomes bit  */
/* 0, and the new carry flag is the old bit 7.                       */
RL_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* RL        H      0xCB 0x14   8                                    */
/*                                                                   */
/* Rotates left into the carry flag. The old carry flag becomes bit  */
/* 0, and the new carry flag is the old bit 7.                       */
RL_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* RL        L      0xCB 0x15   8                                    */
/*                                                                   */
/* Rotates left into the carry flag. The old carry flag becomes bit  */
/* 0, and the new carry flag is the old bit 7.                       */
RL_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* RL       HL      0xCB 0x16   16                                   */
/*                                                                   */
/* Rotates left into the carry flag. The old carry flag becomes bit  */
/* 0, and the new carry flag is the old bit 7.                       */
void RL_HL () {
    /* Read current value. */
    int HL = read_memory (pair (H, L));
    int previous_7th_bit = HL >> 7;

    /* Shift everything down 1. */
    HL = (HL << 1) & BYTE_MASK;

    /* Shift the carry flag in. */
    HL = (F & CARRY_FLAG)?
            (HL | 0x1):
            (HL & ~0x1);

    /* Set the carry flag. */
    F = (previous_7th_bit)?
            (F | CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (HL)?
            (F & ~ZERO_FLAG):
            (F | ZERO_FLAG);

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the half-carryflag. */
    F &= ~HALF_CARRY_FLAG;

    /* Write out new value.  */
    write_memory (pair (H, L), HL);
}

/* Op     Parameters   Code   Cycles                                 */
/* RL        A      0xCB 0x17   8                                    */
/*                                                                   */
/* Rotates left into the carry flag. The old carry flag becomes bit  */
/* 0, and the new carry flag is the old bit 7.                       */
RL_MACRO (A);

#undef RL_MACRO

/*-------------------------------------------------------------------*/
/*----------------------------RRC-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag holds the value of the old 0th bit.                */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* The zero flag is set to 1 if the result is 0. Otherwise,          */
/* it is set to 0.                                                   */
/*                                                                   */
/* The subtract flag is set to 0.                                    */
/*-------------------------------------------------------------------*/
#define RRC_MACRO(arg_register) void RRC_##arg_register () {          \
    int previous_0th_bit = arg_register & 0x1;                        \
                                                                      \
    /* Shift everything up 1. */                                      \
    arg_register = (arg_register & BYTE_MASK) >> 1;                   \
                                                                      \
    /* Shift the carry flag in. */                                    \
    arg_register = (previous_0th_bit)?                                \
            (arg_register | 0x80):                                    \
            (arg_register & ~0x80);                                   \
                                                                      \
    /* Set the carry flag. */                                         \
    F = (previous_0th_bit)?                                           \
            (F | CARRY_FLAG):                                         \
            (F & ~CARRY_FLAG);                                        \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (arg_register)?                                               \
            (F & ~ZERO_FLAG):                                         \
            (F | ZERO_FLAG);                                          \
                                                                      \
    /* Set the subtract flag. */                                      \
    F &= ~SUBTRACT_FLAG;                                              \
                                                                      \
    /* Set the half-carryflag. */                                     \
    F &= ~HALF_CARRY_FLAG;                                            \
}

/* Op     Parameters   Code   Cycles                                 */
/* RRC       B      0xCB 0x08   8                                    */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag and the 7th bit */
/* become the value of the old 0th bit.                              */
RRC_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* RRC       C      0xCB 0x09   8                                    */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag and the 7th bit */
/* become the value of the old 0th bit.                              */
RRC_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* RRC       D      0xCB 0x0A   8                                    */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag and the 7th bit */
/* become the value of the old 0th bit.                              */
RRC_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* RRC       E      0xCB 0x0B   8                                    */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag and the 7th bit */
/* become the value of the old 0th bit.                              */
RRC_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* RRC       H      0xCB 0x0C   8                                    */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag and the 7th bit */
/* become the value of the old 0th bit.                              */
RRC_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* RRC       L      0xCB 0x0D   8                                    */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag and the 7th bit */
/* become the value of the old 0th bit.                              */
RRC_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* RRC     (HL)     0xCB 0x0D   16                                   */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag and the 7th bit */
/* become the value of the old 0th bit.                              */
void RRC_HL () {
    /* Read current value.*/
    int HL = read_memory (pair (H, L));
    int previous_0th_bit = HL & 0x1;

    /* Shift everything up 1. */
    HL = (HL & BYTE_MASK) >> 1;

    /* Shift the carry flag in. */
    HL = (previous_0th_bit)?
            (HL | 0x80):
            (HL & ~0x80);

    /* Set the carry flag. */
    F = (previous_0th_bit)?
            (F | CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (HL)?
            (F & ~ZERO_FLAG):
            (F | ZERO_FLAG);

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the half-carryflag. */
    F &= ~HALF_CARRY_FLAG;

    /* Write out new value. */
    write_memory (pair (H, L), HL);
}

/* Op     Parameters   Code   Cycles                                 */
/* RRC       A      0xCB 0x0F   8                                    */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag and the 7th bit */
/* become the value of the old 0th bit.                              */
RRC_MACRO (A);

#undef RRC_MACRO

/*-------------------------------------------------------------------*/
/*-----------------------------RR-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag holds the value of the old 0th bit.                */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* The zero flag is set to 1 if the result is 0. Otherwise,          */
/* it is set to 0.                                                   */
/*                                                                   */
/* The subtract flag is set to 0.                                    */
/*-------------------------------------------------------------------*/
#define RR_MACRO(arg_register) void RR_##arg_register () {            \
    int previous_0th_bit = arg_register & 0x1;                        \
                                                                      \
    /* Shift everything up 1. */                                      \
    arg_register = (arg_register & BYTE_MASK) >> 1;                   \
                                                                      \
    /* Shift the carry flag in. */                                    \
    arg_register = (F & CARRY_FLAG)?                                  \
            (arg_register | 0x80):                                    \
            (arg_register & ~0x80);                                   \
                                                                      \
    /* Set the carry flag. */                                         \
    F = (previous_0th_bit)?                                           \
            (F | CARRY_FLAG):                                         \
            (F & ~CARRY_FLAG);                                        \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (arg_register)?                                               \
            (F & ~ZERO_FLAG):                                         \
            (F | ZERO_FLAG);                                          \
                                                                      \
    /* Set the subtract flag. */                                      \
    F &= ~SUBTRACT_FLAG;                                              \
                                                                      \
    /* Set the half-carryflag. */                                     \
    F &= ~HALF_CARRY_FLAG;                                            \
}


/* Op     Parameters   Code   Cycles                                 */
/* RR        B      0xCB 0x18   8                                    */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag becomes the     */
/* previous 0 bit and the 7th bit becomes the old carry flag.        */
RR_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* RR        C      0xCB 0x19   8                                    */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag becomes the     */
/* previous 0 bit and the 7th bit becomes the old carry flag.        */
RR_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* RR        D      0xCB 0x1A   8                                    */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag becomes the     */
/* previous 0 bit and the 7th bit becomes the old carry flag.        */
RR_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* RR        E      0xCB 0x1B   8                                    */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag becomes the     */
/* previous 0 bit and the 7th bit becomes the old carry flag.        */
RR_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* RR        H      0xCB 0x1C   8                                    */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag becomes the     */
/* previous 0 bit and the 7th bit becomes the old carry flag.        */
RR_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* RR        L      0xCB 0x1D   8                                    */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag becomes the     */
/* previous 0 bit and the 7th bit becomes the old carry flag.        */
RR_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* RR      (HL)     0xCB 0x1E   16                                   */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag becomes the     */
/* previous 0 bit and the 7th bit becomes the old carry flag.        */
void RR_HL () {
    /* Read current value. */
    int HL = read_memory (pair (H, L));
    int previous_0th_bit = HL & 0x1;

    /* Shift everything up 1. */
    HL = (HL & BYTE_MASK) >> 1;

    /* Shift the carry flag in. */
    HL = (F & CARRY_FLAG)?
            (HL | 0x80):
            (HL & ~0x80);

    /* Set the carry flag. */
    F = (previous_0th_bit)?
            (F | CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (HL)?
            (F & ~ZERO_FLAG):
            (F | ZERO_FLAG);

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the half-carryflag. */
    F &= ~HALF_CARRY_FLAG;

    /* Write new value. */
    write_memory (pair (H, L), HL);
}

/* Op     Parameters   Code   Cycles                                 */
/* RR        A      0xCB 0x1F   8                                    */
/*                                                                   */
/* Rotates right into the carry flag. The carry flag becomes the     */
/* previous 0 bit and the 7th bit becomes the old carry flag.        */
RR_MACRO (A);

#undef RR_MACRO

/*-------------------------------------------------------------------*/
/*----------------------------SLA-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag holds the value of the old 7th bit.                */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* The zero flag is set to 1 if the result is 0. Otherwise,          */
/* it is set to 0.                                                   */
/*                                                                   */
/* The subtract flag is set to 0.                                    */
/*-------------------------------------------------------------------*/
#define SLA_MACRO(arg_register) void SLA_##arg_register () {          \
    int previous_7th_bit = arg_register >> 7;                         \
                                                                      \
    /* Shift everything up 1. */                                      \
    arg_register = (arg_register << 1) & BYTE_MASK;                   \
                                                                      \
    /* Set the carry flag. */                                         \
    F = (previous_7th_bit)?                                           \
            (F | CARRY_FLAG):                                         \
            (F & ~CARRY_FLAG);                                        \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (arg_register)?                                               \
            (F & ~ZERO_FLAG):                                         \
            (F | ZERO_FLAG);                                          \
                                                                      \
    /* Set the subtract flag. */                                      \
    F &= ~SUBTRACT_FLAG;                                              \
                                                                      \
    /* Set the half-carryflag. */                                     \
    F &= ~HALF_CARRY_FLAG;                                            \
}

/* Op     Parameters   Code   Cycles                                 */
/* SLA       B      0xCB 0x20   8                                    */
/*                                                                   */
/* Shifts everything up 1 bit. The 0th bit is 0 and the carry flag   */
/* holds the old value of the 7th bit.                               */
SLA_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* SLA       C      0xCB 0x21   8                                    */
/*                                                                   */
/* Shifts everything up 1 bit. The 0th bit is 0 and the carry flag   */
/* holds the old value of the 7th bit.                               */
SLA_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* SLA       D      0xCB 0x22   8                                    */
/*                                                                   */
/* Shifts everything up 1 bit. The 0th bit is 0 and the carry flag   */
/* holds the old value of the 7th bit.                               */
SLA_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* SLA       E      0xCB 0x23   8                                    */
/*                                                                   */
/* Shifts everything up 1 bit. The 0th bit is 0 and the carry flag   */
/* holds the old value of the 7th bit.                               */
SLA_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* SLA       H      0xCB 0x24   8                                    */
/*                                                                   */
/* Shifts everything up 1 bit. The 0th bit is 0 and the carry flag   */
/* holds the old value of the 7th bit.                               */
SLA_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* SLA       L      0xCB 0x25   8                                    */
/*                                                                   */
/* Shifts everything up 1 bit. The 0th bit is 0 and the carry flag   */
/* holds the old value of the 7th bit.                               */
SLA_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* SLA      (HL)    0xCB 0x26   16                                   */
/*                                                                   */
/* Shifts everything up 1 bit. The 0th bit is 0 and the carry flag   */
/* holds the old value of the 7th bit.                               */
void SLA_HL () {
    /* Read current value. */
    int HL = read_memory (pair (H, L));
    int previous_7th_bit = HL >> 7;

    /* Shift everything up 1. */
    HL = (HL << 1) & BYTE_MASK;

    /* Set the carry flag. */
    F = (previous_7th_bit)?
            (F | CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (HL)?
            (F & ~ZERO_FLAG):
            (F | ZERO_FLAG);

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the half-carryflag. */
    F &= ~HALF_CARRY_FLAG;

    /* Write new value. */
    write_memory (pair (H, L), HL);
}

/* Op     Parameters   Code   Cycles                                 */
/* SLA       A      0xCB 0x27   8                                    */
/*                                                                   */
/* Shifts everything up 1 bit. The 0th bit is 0 and the carry flag   */
/* holds the old value of the 7th bit.                               */
SLA_MACRO (A);

#undef SLA_MACRO

/*-------------------------------------------------------------------*/
/*----------------------------SRA-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag holds the value of the old 7th bit.                */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* The zero flag is set to 1 if the result is 0. Otherwise,          */
/* it is set to 0.                                                   */
/*                                                                   */
/* The subtract flag is set to 0.                                    */
/*-------------------------------------------------------------------*/
#define SRA_MACRO(arg_register) void SRA_##arg_register () {          \
    int previous_7th_bit = arg_register >> 7;                         \
    int previous_0th_bit = arg_register & 0x1;                        \
                                                                      \
    /* Shift everything down 1. */                                    \
    arg_register = (arg_register >> 1);                               \
                                                                      \
    /* Set the 7th bit. */                                            \
    arg_register = (previous_7th_bit)?                                \
                       (arg_register | 0x80):                         \
                       (arg_register & ~0x80);                        \
                                                                      \
    /* Set the carry flag. */                                         \
    F = (previous_0th_bit)?                                           \
            (F | CARRY_FLAG):                                         \
            (F & ~CARRY_FLAG);                                        \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (arg_register)?                                               \
            (F & ~ZERO_FLAG):                                         \
            (F | ZERO_FLAG);                                          \
                                                                      \
    /* Set the subtract flag. */                                      \
    F &= ~SUBTRACT_FLAG;                                              \
                                                                      \
    /* Set the half-carryflag. */                                     \
    F &= ~HALF_CARRY_FLAG;                                            \
}

/* Op     Parameters   Code   Cycles                                 */
/* SRA       B      0xCB 0x28   8                                    */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit remains the same and the*/
/* old 0th bit becomes the value of the carry flag.                  */
SRA_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* SRA       C      0xCB 0x29   8                                    */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit remains the same and the*/
/* old 0th bit becomes the value of the carry flag.                  */
SRA_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* SRA       D      0xCB 0x2A   8                                    */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit remains the same and the*/
/* old 0th bit becomes the value of the carry flag.                  */
SRA_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* SRA       E      0xCB 0x2B   8                                    */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit remains the same and the*/
/* old 0th bit becomes the value of the carry flag.                  */
SRA_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* SRA       H      0xCB 0x2C   8                                    */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit remains the same and the*/
/* old 0th bit becomes the value of the carry flag.                  */
SRA_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* SRA       L      0xCB 0x2D   8                                    */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit remains the same and the*/
/* old 0th bit becomes the value of the carry flag.                  */
SRA_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* SRA      (HL)    0xCB 0x2E   16                                   */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit remains the same and the*/
/* old 0th bit becomes the value of the carry flag.                  */
void SRA_HL () {
    /* Read current value. */
    int HL = read_memory (pair (H, L));
    int previous_7th_bit = HL >> 7;
    int previous_0th_bit = HL & 0x1;

    /* Shift everything down 1. */
    HL = (HL >> 1);

    /* Set the 7th bit. */
    HL = (previous_7th_bit)?
                       (HL | 0x80):
                       (HL & ~0x80);

    /* Set the carry flag. */
    F = (previous_0th_bit)?
            (F | CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (HL)?
            (F & ~ZERO_FLAG):
            (F | ZERO_FLAG);

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the half-carryflag. */
    F &= ~HALF_CARRY_FLAG;

    /* Write new value. */
    write_memory (pair (H, L), HL);
}

/* Op     Parameters   Code   Cycles                                 */
/* SRA       A      0xCB 0x2F   8                                    */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit remains the same and the*/
/* old 0th bit becomes the value of the carry flag.                  */
SRA_MACRO (A);

#undef SRA_MACRO

/*-------------------------------------------------------------------*/
/*----------------------------SRL-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag holds the value of the old 7th bit.                */
/*                                                                   */
/* The half-carry flag is set to 0.                                  */
/*                                                                   */
/* The zero flag is set to 1 if the result is 0. Otherwise,          */
/* it is set to 0.                                                   */
/*                                                                   */
/* The subtract flag is set to 0.                                    */
/*-------------------------------------------------------------------*/
#define SRL_MACRO(arg_register) void SRL_##arg_register () {          \
    int previous_0th_bit = arg_register & 0x1;                        \
                                                                      \
    /* Shift everything down 1. */                                    \
    arg_register = (arg_register >> 1);                               \
                                                                      \
    /* Set the carry flag. */                                         \
    F = (previous_0th_bit)?                                           \
            (F | CARRY_FLAG):                                         \
            (F & ~CARRY_FLAG);                                        \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (arg_register)?                                               \
            (F & ~ZERO_FLAG):                                         \
            (F | ZERO_FLAG);                                          \
                                                                      \
    /* Set the subtract flag. */                                      \
    F &= ~SUBTRACT_FLAG;                                              \
                                                                      \
    /* Set the half-carryflag. */                                     \
    F &= ~HALF_CARRY_FLAG;                                            \
}

/* Op     Parameters   Code   Cycles                                 */
/* SRL       B      0xCB 0x38   8                                    */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit is set to 0 and the     */
/* old 0th bit becomes the value of the carry flag.                  */
SRL_MACRO (B);

/* Op     Parameters   Code   Cycles                                 */
/* SRL       C      0xCB 0x39   8                                    */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit is set to 0 and the     */
/* old 0th bit becomes the value of the carry flag.                  */
SRL_MACRO (C);

/* Op     Parameters   Code   Cycles                                 */
/* SRL       D      0xCB 0x3A   8                                    */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit is set to 0 and the     */
/* old 0th bit becomes the value of the carry flag.                  */
SRL_MACRO (D);

/* Op     Parameters   Code   Cycles                                 */
/* SRL       E      0xCB 0x3B   8                                    */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit is set to 0 and the     */
/* old 0th bit becomes the value of the carry flag.                  */
SRL_MACRO (E);

/* Op     Parameters   Code   Cycles                                 */
/* SRL       H      0xCB 0x3C   8                                    */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit is set to 0 and the     */
/* old 0th bit becomes the value of the carry flag.                  */
SRL_MACRO (H);

/* Op     Parameters   Code   Cycles                                 */
/* SRL       L      0xCB 0x3D   8                                    */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit is set to 0 and the     */
/* old 0th bit becomes the value of the carry flag.                  */
SRL_MACRO (L);

/* Op     Parameters   Code   Cycles                                 */
/* SRL      (HL)    0xCB 0x3E   16                                   */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit is set to 0 and the     */
/* old 0th bit becomes the value of the carry flag.                  */
void SRL_HL () {
    /* Read current value. */
    int HL = read_memory (pair (H, L));
    int previous_0th_bit = HL & 0x1;

    /* Shift everything down 1. */
    HL = (HL >> 1);

    /* Set the carry flag. */
    F = (previous_0th_bit)?
            (F | CARRY_FLAG):
            (F & ~CARRY_FLAG);

    /* Set the zero flag. */
    F = (HL)?
            (F & ~ZERO_FLAG):
            (F | ZERO_FLAG);

    /* Set the subtract flag. */
    F &= ~SUBTRACT_FLAG;

    /* Set the half-carryflag. */
    F &= ~HALF_CARRY_FLAG;

    /* Write new value. */
    write_memory (pair (H, L), HL);
}

/* Op     Parameters   Code   Cycles                                 */
/* SRL       A      0xCB 0x3F   8                                    */
/*                                                                   */
/* Shifts everything down 1 bit. The 7th bit is set to 0 and the     */
/* old 0th bit becomes the value of the carry flag.                  */
SRL_MACRO (A);

#undef SRL_MACRO

/*-------------------------------------------------------------------*/
/*----------------------------RST-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is not affected.                                   */
/*                                                                   */
/* The half-carry flag is not affected.                              */
/*                                                                   */
/* The zero flag is not affected.                                    */
/*                                                                   */
/* The subtract flag is not affected.                                */
/*-------------------------------------------------------------------*/
#define RST_MACRO(RST_NUMBER) void RST_##RST_NUMBER () {              \
    /* Get the relevant address for the reset number. */              \
    int jump_address = RST_NUMBER * 0x08;                             \
    int least_significant_byte, most_significant_byte;                \
                                                                      \
    /* Get relevant nibbles for pushing PC to the stack. */           \
    most_significant_byte  = (PC >> 8) & BYTE_MASK;                   \
    least_significant_byte = PC & BYTE_MASK;                          \
                                                                      \
    /* Make room on the stack for an address. */                      \
    SP -= 2;                                                          \
                                                                      \
    /* Write the PC to the stack. */                                  \
    write_memory (SP, least_significant_byte);                        \
    write_memory (SP + 1, most_significant_byte);                     \
                                                                      \
    /* Adjust PC to the expected value. */                            \
    PC = jump_address;                                                \
}

/* Op     Parameters   Code   Cycles                                 */
/* RST                 0xC7     16                                   */
/*                                                                   */
/* Push the present address onto the stack and jump to address       */
/*   0x0000 + 0x08 * 0.                                              */
RST_MACRO (0);

/* Op     Parameters   Code   Cycles                                 */
/* RST                 0xCF     16                                   */
/*                                                                   */
/* Push the present address onto the stack and jump to address       */
/*   0x0000 + 0x08 * 1.                                              */
RST_MACRO (1);

/* Op     Parameters   Code   Cycles                                 */
/* RST                 0xD7     16                                   */
/*                                                                   */
/* Push the present address onto the stack and jump to address       */
/*   0x0000 + 0x08 * 2.                                              */
RST_MACRO (2);

/* Op     Parameters   Code   Cycles                                 */
/* RST                 0xDF     16                                   */
/*                                                                   */
/* Push the present address onto the stack and jump to address       */
/*   0x0000 + 0x08 * 3.                                              */
RST_MACRO (3);

/* Op     Parameters   Code   Cycles                                 */
/* RST                 0xE7     16                                   */
/*                                                                   */
/* Push the present address onto the stack and jump to address       */
/*   0x0000 + 0x08 * 4.                                              */
RST_MACRO (4);

/* Op     Parameters   Code   Cycles                                 */
/* RST                 0xEF     16                                   */
/*                                                                   */
/* Push the present address onto the stack and jump to address       */
/*   0x0000 + 0x08 * 5.                                              */
RST_MACRO (5);

/* Op     Parameters   Code   Cycles                                 */
/* RST                 0xF7     16                                   */
/*                                                                   */
/* Push the present address onto the stack and jump to address       */
/*   0x0000 + 0x08 * 6.                                              */
RST_MACRO (6);

/* Op     Parameters   Code   Cycles                                 */
/* RST                 0xFF     16                                   */
/*                                                                   */
/* Push the present address onto the stack and jump to address       */
/*   0x0000 + 0x08 * 7.                                              */
RST_MACRO (7);

#undef RST_MACRO

/*-------------------------------------------------------------------*/
/*----------------------------SET-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is not affected.                                   */
/*                                                                   */
/* The half-carry flag is not affected.                              */
/*                                                                   */
/* The zero flag is not affected.                                    */
/*                                                                   */
/* The subtract flag is not affected.                                */
/*-------------------------------------------------------------------*/
#define SET_MACRO(arg_register, bit)                                  \
void SET_##arg_register##_##bit () {                                  \
    arg_register |= (0x1 << bit);                                     \
}

#define SET_HL_MACRO(bit) void SET_HL_##bit () {                      \
    int HL = read_memory (pair (H, L));                               \
                                                                      \
    HL |= 0x1 << bit;                                                 \
                                                                      \
    write_memory (pair (H, L), HL);                                   \
}

/*--------- SET 0 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* SET       B, 0   0xCB 0xC0    8                                   */
/*                                                                   */
/* Sets the 0 bit in B to 1.                                         */
SET_MACRO (B, 0);

/* Op     Parameters   Code   Cycles                                 */
/* SET       C, 0   0xCB 0xC1    8                                   */
/*                                                                   */
/* Sets the 0 bit in C to 1.                                         */
SET_MACRO (C, 0);

/* Op     Parameters   Code   Cycles                                 */
/* SET       D, 0   0xCB 0xC2    8                                   */
/*                                                                   */
/* Sets the 0 bit in D to 1.                                         */
SET_MACRO (D, 0);

/* Op     Parameters   Code   Cycles                                 */
/* SET       E, 0   0xCB 0xC3    8                                   */
/*                                                                   */
/* Sets the 0 bit in E to 1.                                         */
SET_MACRO (E, 0);

/* Op     Parameters   Code   Cycles                                 */
/* SET       H, 0   0xCB 0xC4    8                                   */
/*                                                                   */
/* Sets the 0 bit in H to 1.                                         */
SET_MACRO (H, 0);

/* Op     Parameters   Code   Cycles                                 */
/* SET       L, 0   0xCB 0xC5    8                                   */
/*                                                                   */
/* Sets the 0 bit in L to 1.                                         */
SET_MACRO (L, 0);

/* Op     Parameters   Code   Cycles                                 */
/* SET    (HL), 0   0xCB 0xC6    8                                   */
/*                                                                   */
/* Sets the 0 bit in (HL) to 1.                                      */
SET_HL_MACRO (0);

/* Op     Parameters   Code   Cycles                                 */
/* SET       A, 0   0xCB 0xC7    8                                   */
/*                                                                   */
/* Sets the 0 bit in A to 1.                                         */
SET_MACRO (A, 0);

/*--------- SET 1 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* SET       B, 1   0xCB 0xC8    8                                   */
/*                                                                   */
/* Sets the 1 bit in B to 1.                                         */
SET_MACRO (B, 1);

/* Op     Parameters   Code   Cycles                                 */
/* SET       C, 1   0xCB 0xC9    8                                   */
/*                                                                   */
/* Sets the 1 bit in C to 1.                                         */
SET_MACRO (C, 1);

/* Op     Parameters   Code   Cycles                                 */
/* SET       D, 1   0xCB 0xCA    8                                   */
/*                                                                   */
/* Sets the 1 bit in D to 1.                                         */
SET_MACRO (D, 1);

/* Op     Parameters   Code   Cycles                                 */
/* SET       E, 1   0xCB 0xCB    8                                   */
/*                                                                   */
/* Sets the 1 bit in E to 1.                                         */
SET_MACRO (E, 1);

/* Op     Parameters   Code   Cycles                                 */
/* SET       H, 1   0xCB 0xCC    8                                   */
/*                                                                   */
/* Sets the 1 bit in H to 1.                                         */
SET_MACRO (H, 1);

/* Op     Parameters   Code   Cycles                                 */
/* SET       L, 1   0xCB 0xCD    8                                   */
/*                                                                   */
/* Sets the 1 bit in L to 1.                                         */
SET_MACRO (L, 1);

/* Op     Parameters   Code   Cycles                                 */
/* SET    (HL), 1   0xCB 0xCE    8                                   */
/*                                                                   */
/* Sets the 1 bit in (HL) to 1.                                      */
SET_HL_MACRO (1);

/* Op     Parameters   Code   Cycles                                 */
/* SET       A, 1   0xCB 0xCF    8                                   */
/*                                                                   */
/* Sets the 1 bit in A to 1.                                         */
SET_MACRO (A, 1);

/*--------- SET 2 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* SET       B, 2   0xCB 0xD0    8                                   */
/*                                                                   */
/* Sets the 2 bit in B to 1.                                         */
SET_MACRO (B, 2);

/* Op     Parameters   Code   Cycles                                 */
/* SET       C, 2   0xCB 0xD1    8                                   */
/*                                                                   */
/* Sets the 2 bit in C to 1.                                         */
SET_MACRO (C, 2);

/* Op     Parameters   Code   Cycles                                 */
/* SET       D, 2   0xCB 0xD2    8                                   */
/*                                                                   */
/* Sets the 2 bit in D to 1.                                         */
SET_MACRO (D, 2);

/* Op     Parameters   Code   Cycles                                 */
/* SET       E, 2   0xCB 0xD3    8                                   */
/*                                                                   */
/* Sets the 2 bit in E to 1.                                         */
SET_MACRO (E, 2);

/* Op     Parameters   Code   Cycles                                 */
/* SET       H, 2   0xCB 0xD4    8                                   */
/*                                                                   */
/* Sets the 2 bit in H to 1.                                         */
SET_MACRO (H, 2);

/* Op     Parameters   Code   Cycles                                 */
/* SET       L, 2   0xCB 0xD5    8                                   */
/*                                                                   */
/* Sets the 2 bit in L to 1.                                         */
SET_MACRO (L, 2);

/* Op     Parameters   Code   Cycles                                 */
/* SET    (HL), 2   0xCB 0xD6    8                                   */
/*                                                                   */
/* Sets the 2 bit in (HL) to 1.                                      */
SET_HL_MACRO (2);

/* Op     Parameters   Code   Cycles                                 */
/* SET       A, 2   0xCB 0xD7    8                                   */
/*                                                                   */
/* Sets the 2 bit in A to 1.                                         */
SET_MACRO (A, 2);

/*--------- SET 3 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* SET       B, 3   0xCB 0xD8    8                                   */
/*                                                                   */
/* Sets the 3 bit in B to 1.                                         */
SET_MACRO (B, 3);

/* Op     Parameters   Code   Cycles                                 */
/* SET       C, 3   0xCB 0xD9    8                                   */
/*                                                                   */
/* Sets the 3 bit in C to 1.                                         */
SET_MACRO (C, 3);

/* Op     Parameters   Code   Cycles                                 */
/* SET       D, 3   0xCB 0xDA    8                                   */
/*                                                                   */
/* Sets the 3 bit in D to 1.                                         */
SET_MACRO (D, 3);

/* Op     Parameters   Code   Cycles                                 */
/* SET       E, 3   0xCB 0xDB    8                                   */
/*                                                                   */
/* Sets the 3 bit in E to 1.                                         */
SET_MACRO (E, 3);

/* Op     Parameters   Code   Cycles                                 */
/* SET       H, 3   0xCB 0xDC    8                                   */
/*                                                                   */
/* Sets the 3 bit in H to 1.                                         */
SET_MACRO (H, 3);

/* Op     Parameters   Code   Cycles                                 */
/* SET       L, 3   0xCB 0xDD    8                                   */
/*                                                                   */
/* Sets the 3 bit in L to 1.                                         */
SET_MACRO (L, 3);

/* Op     Parameters   Code   Cycles                                 */
/* SET    (HL), 3   0xCB 0xDE    8                                   */
/*                                                                   */
/* Sets the 3 bit in (HL) to 1.                                      */
SET_HL_MACRO (3);

/* Op     Parameters   Code   Cycles                                 */
/* SET       A, 3   0xCB 0xDF    8                                   */
/*                                                                   */
/* Sets the 3 bit in A to 1.                                         */
SET_MACRO (A, 3);

/*--------- SET 4 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* SET       B, 4   0xCB 0xE0    8                                   */
/*                                                                   */
/* Sets the 4 bit in B to 1.                                         */
SET_MACRO (B, 4);

/* Op     Parameters   Code   Cycles                                 */
/* SET       C, 4   0xCB 0xE1    8                                   */
/*                                                                   */
/* Sets the 4 bit in C to 1.                                         */
SET_MACRO (C, 4);

/* Op     Parameters   Code   Cycles                                 */
/* SET       D, 4   0xCB 0xE2    8                                   */
/*                                                                   */
/* Sets the 4 bit in D to 1.                                         */
SET_MACRO (D, 4);

/* Op     Parameters   Code   Cycles                                 */
/* SET       E, 4   0xCB 0xE3    8                                   */
/*                                                                   */
/* Sets the 4 bit in E to 1.                                         */
SET_MACRO (E, 4);

/* Op     Parameters   Code   Cycles                                 */
/* SET       H, 4   0xCB 0xE4    8                                   */
/*                                                                   */
/* Sets the 4 bit in H to 1.                                         */
SET_MACRO (H, 4);

/* Op     Parameters   Code   Cycles                                 */
/* SET       L, 4   0xCB 0xE5    8                                   */
/*                                                                   */
/* Sets the 4 bit in L to 1.                                         */
SET_MACRO (L, 4);

/* Op     Parameters   Code   Cycles                                 */
/* SET    (HL), 4   0xCB 0xE6    8                                   */
/*                                                                   */
/* Sets the 4 bit in (HL) to 1.                                      */
SET_HL_MACRO (4);

/* Op     Parameters   Code   Cycles                                 */
/* SET       A, 4   0xCB 0xE7    8                                   */
/*                                                                   */
/* Sets the 4 bit in A to 1.                                         */
SET_MACRO (A, 4);

/*--------- SET 5 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* SET       B, 5   0xCB 0xE8    8                                   */
/*                                                                   */
/* Sets the 5 bit in B to 1.                                         */
SET_MACRO (B, 5);

/* Op     Parameters   Code   Cycles                                 */
/* SET       C, 5   0xCB 0xE9    8                                   */
/*                                                                   */
/* Sets the 5 bit in C to 1.                                         */
SET_MACRO (C, 5);

/* Op     Parameters   Code   Cycles                                 */
/* SET       D, 5   0xCB 0xEA    8                                   */
/*                                                                   */
/* Sets the 5 bit in D to 1.                                         */
SET_MACRO (D, 5);

/* Op     Parameters   Code   Cycles                                 */
/* SET       E, 5   0xCB 0xEB    8                                   */
/*                                                                   */
/* Sets the 5 bit in E to 1.                                         */
SET_MACRO (E, 5);

/* Op     Parameters   Code   Cycles                                 */
/* SET       H, 5   0xCB 0xEC    8                                   */
/*                                                                   */
/* Sets the 5 bit in H to 1.                                         */
SET_MACRO (H, 5);

/* Op     Parameters   Code   Cycles                                 */
/* SET       L, 5   0xCB 0xED    8                                   */
/*                                                                   */
/* Sets the 5 bit in L to 1.                                         */
SET_MACRO (L, 5);

/* Op     Parameters   Code   Cycles                                 */
/* SET    (HL), 5   0xCB 0xEE    8                                   */
/*                                                                   */
/* Sets the 5 bit in (HL) to 1.                                      */
SET_HL_MACRO (5);

/* Op     Parameters   Code   Cycles                                 */
/* SET       A, 5   0xCB 0xEF    8                                   */
/*                                                                   */
/* Sets the 5 bit in A to 1.                                         */
SET_MACRO (A, 5);

/*--------- SET 6 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* SET       B, 6   0xCB 0xF0    8                                   */
/*                                                                   */
/* Sets the 6 bit in B to 1.                                         */
SET_MACRO (B, 6);

/* Op     Parameters   Code   Cycles                                 */
/* SET       C, 6   0xCB 0xF1    8                                   */
/*                                                                   */
/* Sets the 6 bit in C to 1.                                         */
SET_MACRO (C, 6);

/* Op     Parameters   Code   Cycles                                 */
/* SET       D, 6   0xCB 0xF2    8                                   */
/*                                                                   */
/* Sets the 6 bit in D to 1.                                         */
SET_MACRO (D, 6);

/* Op     Parameters   Code   Cycles                                 */
/* SET       E, 6   0xCB 0xF3    8                                   */
/*                                                                   */
/* Sets the 6 bit in E to 1.                                         */
SET_MACRO (E, 6);

/* Op     Parameters   Code   Cycles                                 */
/* SET       H, 6   0xCB 0xF4    8                                   */
/*                                                                   */
/* Sets the 6 bit in H to 1.                                         */
SET_MACRO (H, 6);

/* Op     Parameters   Code   Cycles                                 */
/* SET       L, 6   0xCB 0xF5    8                                   */
/*                                                                   */
/* Sets the 6 bit in L to 1.                                         */
SET_MACRO (L, 6);

/* Op     Parameters   Code   Cycles                                 */
/* SET    (HL), 6   0xCB 0xF6    8                                   */
/*                                                                   */
/* Sets the 6 bit in (HL) to 1.                                      */
SET_HL_MACRO (6);

/* Op     Parameters   Code   Cycles                                 */
/* SET       A, 6   0xCB 0xF7    8                                   */
/*                                                                   */
/* Sets the 6 bit in A to 1.                                         */
SET_MACRO (A, 6);

/*--------- SET 7 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* SET       B, 7   0xCB 0xF8    8                                   */
/*                                                                   */
/* Sets the 7 bit in B to 1.                                         */
SET_MACRO (B, 7);

/* Op     Parameters   Code   Cycles                                 */
/* SET       C, 7   0xCB 0xF9    8                                   */
/*                                                                   */
/* Sets the 7 bit in C to 1.                                         */
SET_MACRO (C, 7);

/* Op     Parameters   Code   Cycles                                 */
/* SET       D, 7   0xCB 0xFA    8                                   */
/*                                                                   */
/* Sets the 7 bit in D to 1.                                         */
SET_MACRO (D, 7);

/* Op     Parameters   Code   Cycles                                 */
/* SET       E, 7   0xCB 0xFB    8                                   */
/*                                                                   */
/* Sets the 7 bit in E to 1.                                         */
SET_MACRO (E, 7);

/* Op     Parameters   Code   Cycles                                 */
/* SET       H, 7   0xCB 0xFC    8                                   */
/*                                                                   */
/* Sets the 7 bit in H to 1.                                         */
SET_MACRO (H, 7);

/* Op     Parameters   Code   Cycles                                 */
/* SET       L, 7   0xCB 0xFD    8                                   */
/*                                                                   */
/* Sets the 7 bit in L to 1.                                         */
SET_MACRO (L, 7);

/* Op     Parameters   Code   Cycles                                 */
/* SET    (HL), 7   0xCB 0xFE    8                                   */
/*                                                                   */
/* Sets the 7 bit in (HL) to 1.                                      */
SET_HL_MACRO (7);

/* Op     Parameters   Code   Cycles                                 */
/* SET       A, 7   0xCB 0xFF    8                                   */
/*                                                                   */
/* Sets the 7 bit in A to 1.                                         */
SET_MACRO (A, 7);

#undef SET_MACRO
#undef SET_HL_MACRO

/*-------------------------------------------------------------------*/
/*----------------------------RES-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is not affected.                                   */
/*                                                                   */
/* The half-carry flag is not affected.                              */
/*                                                                   */
/* The zero flag is not affected.                                    */
/*                                                                   */
/* The subtract flag is not affected.                                */
/*-------------------------------------------------------------------*/
#define RES_MACRO(arg_register, bit)                                  \
void RES_##arg_register##_##bit () {                                  \
    arg_register &= ~(0x1 << bit);                                    \
}

#define RES_HL_MACRO(bit) void RES_HL_##bit () {                      \
    int HL = read_memory (pair (H, L));                               \
                                                                      \
    HL &= ~(0x1 << bit);                                              \
                                                                      \
    write_memory (pair (H, L), HL);                                   \
}

/*--------- RES 0 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* RES       B, 0   0xCB 0x80    8                                   */
/*                                                                   */
/* Sets the 0 bit in B to 0.                                         */
RES_MACRO (B, 0);

/* Op     Parameters   Code   Cycles                                 */
/* RES       C, 0   0xCB 0x81    8                                   */
/*                                                                   */
/* Sets the 0 bit in C to 0.                                         */
RES_MACRO (C, 0);

/* Op     Parameters   Code   Cycles                                 */
/* RES       D, 0   0xCB 0x82    8                                   */
/*                                                                   */
/* Sets the 0 bit in D to 0.                                         */
RES_MACRO (D, 0);

/* Op     Parameters   Code   Cycles                                 */
/* RES       E, 0   0xCB 0x83    8                                   */
/*                                                                   */
/* Sets the 0 bit in E to 0.                                         */
RES_MACRO (E, 0);

/* Op     Parameters   Code   Cycles                                 */
/* RES       H, 0   0xCB 0x84    8                                   */
/*                                                                   */
/* Sets the 0 bit in H to 0.                                         */
RES_MACRO (H, 0);

/* Op     Parameters   Code   Cycles                                 */
/* RES       L, 0   0xCB 0x85    8                                   */
/*                                                                   */
/* Sets the 0 bit in L to 0.                                         */
RES_MACRO (L, 0);

/* Op     Parameters   Code   Cycles                                 */
/* RES    (HL), 0   0xCB 0x86    8                                   */
/*                                                                   */
/* Sets the 0 bit in (HL) to 0.                                      */
RES_HL_MACRO (0);

/* Op     Parameters   Code   Cycles                                 */
/* RES       A, 0   0xCB 0x87    8                                   */
/*                                                                   */
/* Sets the 0 bit in A to 0.                                         */
RES_MACRO (A, 0);

/*--------- RES 1 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* RES       B, 1   0xCB 0x88    8                                   */
/*                                                                   */
/* Sets the 1 bit in B to 0.                                         */
RES_MACRO (B, 1);

/* Op     Parameters   Code   Cycles                                 */
/* RES       C, 1   0xCB 0x89    8                                   */
/*                                                                   */
/* Sets the 1 bit in C to 0.                                         */
RES_MACRO (C, 1);

/* Op     Parameters   Code   Cycles                                 */
/* RES       D, 1   0xCB 0x8A    8                                   */
/*                                                                   */
/* Sets the 1 bit in D to 0.                                         */
RES_MACRO (D, 1);

/* Op     Parameters   Code   Cycles                                 */
/* RES       E, 1   0xCB 0x8B    8                                   */
/*                                                                   */
/* Sets the 1 bit in E to 0.                                         */
RES_MACRO (E, 1);

/* Op     Parameters   Code   Cycles                                 */
/* RES       H, 1   0xCB 0x8C    8                                   */
/*                                                                   */
/* Sets the 1 bit in H to 0.                                         */
RES_MACRO (H, 1);

/* Op     Parameters   Code   Cycles                                 */
/* RES       L, 1   0xCB 0x8D    8                                   */
/*                                                                   */
/* Sets the 1 bit in L to 0.                                         */
RES_MACRO (L, 1);

/* Op     Parameters   Code   Cycles                                 */
/* RES    (HL), 1   0xCB 0x8E    8                                   */
/*                                                                   */
/* Sets the 1 bit in (HL) to 0.                                      */
RES_HL_MACRO (1);

/* Op     Parameters   Code   Cycles                                 */
/* RES       A, 1   0xCB 0x8F    8                                   */
/*                                                                   */
/* Sets the 1 bit in A to 0.                                         */
RES_MACRO (A, 1);

/*--------- RES 2 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* RES       B, 2   0xCB 0x90    8                                   */
/*                                                                   */
/* Sets the 2 bit in B to 0.                                         */
RES_MACRO (B, 2);

/* Op     Parameters   Code   Cycles                                 */
/* RES       C, 2   0xCB 0x91    8                                   */
/*                                                                   */
/* Sets the 2 bit in C to 0.                                         */
RES_MACRO (C, 2);

/* Op     Parameters   Code   Cycles                                 */
/* RES       D, 2   0xCB 0x92    8                                   */
/*                                                                   */
/* Sets the 2 bit in D to 0.                                         */
RES_MACRO (D, 2);

/* Op     Parameters   Code   Cycles                                 */
/* RES       E, 2   0xCB 0x93    8                                   */
/*                                                                   */
/* Sets the 2 bit in E to 0.                                         */
RES_MACRO (E, 2);

/* Op     Parameters   Code   Cycles                                 */
/* RES       H, 2   0xCB 0x94    8                                   */
/*                                                                   */
/* Sets the 2 bit in H to 0.                                         */
RES_MACRO (H, 2);

/* Op     Parameters   Code   Cycles                                 */
/* RES       L, 2   0xCB 0x95    8                                   */
/*                                                                   */
/* Sets the 2 bit in L to 0.                                         */
RES_MACRO (L, 2);

/* Op     Parameters   Code   Cycles                                 */
/* RES    (HL), 2   0xCB 0x96    8                                   */
/*                                                                   */
/* Sets the 2 bit in (HL) to 0.                                      */
RES_HL_MACRO (2);

/* Op     Parameters   Code   Cycles                                 */
/* RES       A, 2   0xCB 0x97    8                                   */
/*                                                                   */
/* Sets the 2 bit in A to 0.                                         */
RES_MACRO (A, 2);

/*--------- RES 3 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* RES       B, 3   0xCB 0x98    8                                   */
/*                                                                   */
/* Sets the 3 bit in B to 0.                                         */
RES_MACRO (B, 3);

/* Op     Parameters   Code   Cycles                                 */
/* RES       C, 3   0xCB 0x99    8                                   */
/*                                                                   */
/* Sets the 3 bit in C to 0.                                         */
RES_MACRO (C, 3);

/* Op     Parameters   Code   Cycles                                 */
/* RES       D, 3   0xCB 0x9A    8                                   */
/*                                                                   */
/* Sets the 3 bit in D to 0.                                         */
RES_MACRO (D, 3);

/* Op     Parameters   Code   Cycles                                 */
/* RES       E, 3   0xCB 0x9B    8                                   */
/*                                                                   */
/* Sets the 3 bit in E to 0.                                         */
RES_MACRO (E, 3);

/* Op     Parameters   Code   Cycles                                 */
/* RES       H, 3   0xCB 0x9C    8                                   */
/*                                                                   */
/* Sets the 3 bit in H to 0.                                         */
RES_MACRO (H, 3);

/* Op     Parameters   Code   Cycles                                 */
/* RES       L, 3   0xCB 0x9D    8                                   */
/*                                                                   */
/* Sets the 3 bit in L to 0.                                         */
RES_MACRO (L, 3);

/* Op     Parameters   Code   Cycles                                 */
/* RES    (HL), 3   0xCB 0x9E    8                                   */
/*                                                                   */
/* Sets the 3 bit in (HL) to 0.                                      */
RES_HL_MACRO (3);

/* Op     Parameters   Code   Cycles                                 */
/* RES       A, 3   0xCB 0x9F    8                                   */
/*                                                                   */
/* Sets the 3 bit in A to 0.                                         */
RES_MACRO (A, 3);

/*--------- RES 4 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* RES       B, 4   0xCB 0xA0    8                                   */
/*                                                                   */
/* Sets the 4 bit in B to 0.                                         */
RES_MACRO (B, 4);

/* Op     Parameters   Code   Cycles                                 */
/* RES       C, 4   0xCB 0xA1    8                                   */
/*                                                                   */
/* Sets the 4 bit in C to 0.                                         */
RES_MACRO (C, 4);

/* Op     Parameters   Code   Cycles                                 */
/* RES       D, 4   0xCB 0xA2    8                                   */
/*                                                                   */
/* Sets the 4 bit in D to 0.                                         */
RES_MACRO (D, 4);

/* Op     Parameters   Code   Cycles                                 */
/* RES       E, 4   0xCB 0xA3    8                                   */
/*                                                                   */
/* Sets the 4 bit in E to 0.                                         */
RES_MACRO (E, 4);

/* Op     Parameters   Code   Cycles                                 */
/* RES       H, 4   0xCB 0xA4    8                                   */
/*                                                                   */
/* Sets the 4 bit in H to 0.                                         */
RES_MACRO (H, 4);

/* Op     Parameters   Code   Cycles                                 */
/* RES       L, 4   0xCB 0xA5    8                                   */
/*                                                                   */
/* Sets the 4 bit in L to 0.                                         */
RES_MACRO (L, 4);

/* Op     Parameters   Code   Cycles                                 */
/* RES    (HL), 4   0xCB 0xA6    8                                   */
/*                                                                   */
/* Sets the 4 bit in (HL) to 0.                                      */
RES_HL_MACRO (4);

/* Op     Parameters   Code   Cycles                                 */
/* RES       A, 4   0xCB 0xA7    8                                   */
/*                                                                   */
/* Sets the 4 bit in A to 0.                                         */
RES_MACRO (A, 4);

/*--------- RES 5 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* RES       B, 5   0xCB 0xA8    8                                   */
/*                                                                   */
/* Sets the 5 bit in B to 0.                                         */
RES_MACRO (B, 5);

/* Op     Parameters   Code   Cycles                                 */
/* RES       C, 5   0xCB 0xA9    8                                   */
/*                                                                   */
/* Sets the 5 bit in C to 0.                                         */
RES_MACRO (C, 5);

/* Op     Parameters   Code   Cycles                                 */
/* RES       D, 5   0xCB 0xAA    8                                   */
/*                                                                   */
/* Sets the 5 bit in D to 0.                                         */
RES_MACRO (D, 5);

/* Op     Parameters   Code   Cycles                                 */
/* RES       E, 5   0xCB 0xAB    8                                   */
/*                                                                   */
/* Sets the 5 bit in E to 0.                                         */
RES_MACRO (E, 5);

/* Op     Parameters   Code   Cycles                                 */
/* RES       H, 5   0xCB 0xAC    8                                   */
/*                                                                   */
/* Sets the 5 bit in H to 0.                                         */
RES_MACRO (H, 5);

/* Op     Parameters   Code   Cycles                                 */
/* RES       L, 5   0xCB 0xAD    8                                   */
/*                                                                   */
/* Sets the 5 bit in L to 0.                                         */
RES_MACRO (L, 5);

/* Op     Parameters   Code   Cycles                                 */
/* RES    (HL), 5   0xCB 0xAE    8                                   */
/*                                                                   */
/* Sets the 5 bit in (HL) to 0.                                      */
RES_HL_MACRO (5);

/* Op     Parameters   Code   Cycles                                 */
/* RES       A, 5   0xCB 0xAF    8                                   */
/*                                                                   */
/* Sets the 5 bit in A to 0.                                         */
RES_MACRO (A, 5);

/*--------- RES 6 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* RES       B, 6   0xCB 0xB0    8                                   */
/*                                                                   */
/* Sets the 6 bit in B to 0.                                         */
RES_MACRO (B, 6);

/* Op     Parameters   Code   Cycles                                 */
/* RES       C, 6   0xCB 0xB1    8                                   */
/*                                                                   */
/* Sets the 6 bit in C to 0.                                         */
RES_MACRO (C, 6);

/* Op     Parameters   Code   Cycles                                 */
/* RES       D, 6   0xCB 0xB2    8                                   */
/*                                                                   */
/* Sets the 6 bit in D to 0.                                         */
RES_MACRO (D, 6);

/* Op     Parameters   Code   Cycles                                 */
/* RES       E, 6   0xCB 0xB3    8                                   */
/*                                                                   */
/* Sets the 6 bit in E to 0.                                         */
RES_MACRO (E, 6);

/* Op     Parameters   Code   Cycles                                 */
/* RES       H, 6   0xCB 0xB4    8                                   */
/*                                                                   */
/* Sets the 6 bit in H to 0.                                         */
RES_MACRO (H, 6);

/* Op     Parameters   Code   Cycles                                 */
/* RES       L, 6   0xCB 0xB5    8                                   */
/*                                                                   */
/* Sets the 6 bit in L to 0.                                         */
RES_MACRO (L, 6);

/* Op     Parameters   Code   Cycles                                 */
/* RES    (HL), 6   0xCB 0xB6    8                                   */
/*                                                                   */
/* Sets the 6 bit in (HL) to 0.                                      */
RES_HL_MACRO (6);

/* Op     Parameters   Code   Cycles                                 */
/* RES       A, 6   0xCB 0xB7    8                                   */
/*                                                                   */
/* Sets the 6 bit in A to 0.                                         */
RES_MACRO (A, 6);

/*--------- RES 7 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* RES       B, 7   0xCB 0xB8    8                                   */
/*                                                                   */
/* Sets the 7 bit in B to 0.                                         */
RES_MACRO (B, 7);

/* Op     Parameters   Code   Cycles                                 */
/* RES       C, 7   0xCB 0xB9    8                                   */
/*                                                                   */
/* Sets the 7 bit in C to 0.                                         */
RES_MACRO (C, 7);

/* Op     Parameters   Code   Cycles                                 */
/* RES       D, 7   0xCB 0xBA    8                                   */
/*                                                                   */
/* Sets the 7 bit in D to 0.                                         */
RES_MACRO (D, 7);

/* Op     Parameters   Code   Cycles                                 */
/* RES       E, 7   0xCB 0xBB    8                                   */
/*                                                                   */
/* Sets the 7 bit in E to 0.                                         */
RES_MACRO (E, 7);

/* Op     Parameters   Code   Cycles                                 */
/* RES       H, 7   0xCB 0xBC    8                                   */
/*                                                                   */
/* Sets the 7 bit in H to 0.                                         */
RES_MACRO (H, 7);

/* Op     Parameters   Code   Cycles                                 */
/* RES       L, 7   0xCB 0xBD    8                                   */
/*                                                                   */
/* Sets the 7 bit in L to 0.                                         */
RES_MACRO (L, 7);

/* Op     Parameters   Code   Cycles                                 */
/* RES    (HL), 7   0xCB 0xBE    8                                   */
/*                                                                   */
/* Sets the 7 bit in (HL) to 0.                                      */
RES_HL_MACRO (7);

/* Op     Parameters   Code   Cycles                                 */
/* RES       A, 7   0xCB 0xBF    8                                   */
/*                                                                   */
/* Sets the 7 bit in A to 0.                                         */
RES_MACRO (A, 7);

#undef RES_MACRO
#undef RES_HL_MACRO

/*-------------------------------------------------------------------*/
/*----------------------------BIT-Operation--------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is not affected.                                   */
/*                                                                   */
/* The half-carry flag is set to 1.                                  */
/*                                                                   */
/* The zero flag is set to 0 if the bit is 0, otherwise it is set to */
/* 1 if the bit is 1.                                                */
/*                                                                   */
/* The subtract flag is set to 0.                                    */
/*-------------------------------------------------------------------*/
#define BIT_MACRO(arg_register, bit)                                  \
void BIT_##arg_register##_##bit () {                                  \
    int target_bit = arg_register & (0x1 << bit);                     \
                                                                      \
    /* Set half-carry flag to 1. */                                   \
    F |= HALF_CARRY_FLAG;                                             \
                                                                      \
    /* Set subtract flag to 0. */                                     \
    F &= ~SUBTRACT_FLAG;                                              \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (target_bit == 0)?                                            \
            (F | ZERO_FLAG):                                          \
            (F & ~ZERO_FLAG);                                         \
}

#define BIT_HL_MACRO(bit) void BIT_HL_##bit () {                      \
    int HL = read_memory (pair (H, L));                               \
    int target_bit = HL & (0x1 << bit);                               \
                                                                      \
    /* Set half-carry flag to 1. */                                   \
    F |= HALF_CARRY_FLAG;                                             \
                                                                      \
    /* Set subtract flag to 0. */                                     \
    F &= ~SUBTRACT_FLAG;                                              \
                                                                      \
    /* Set the zero flag. */                                          \
    F = (target_bit == 0)?                                            \
            (F | ZERO_FLAG):                                          \
            (F & ~ZERO_FLAG);                                         \
}

/*--------- BIT 0 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* BIT       B, 0   0xCB 0x40    8                                   */
/*                                                                   */
/* Checks the 0 bit in B and sets relevant flags.                    */
BIT_MACRO (B, 0);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       C, 0   0xCB 0x41    8                                   */
/*                                                                   */
/* Checks the 0 bit in C and sets relevant flags.                    */
BIT_MACRO (C, 0);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       D, 0   0xCB 0x42    8                                   */
/*                                                                   */
/* Checks the 0 bit in D and sets relevant flags.                    */
BIT_MACRO (D, 0);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       E, 0   0xCB 0x43    8                                   */
/*                                                                   */
/* Checks the 0 bit in E and sets relevant flags.                    */
BIT_MACRO (E, 0);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       H, 0   0xCB 0x44    8                                   */
/*                                                                   */
/* Checks the 0 bit in H and sets relevant flags.                    */
BIT_MACRO (H, 0);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       L, 0   0xCB 0x45    8                                   */
/*                                                                   */
/* Checks the 0 bit in L and sets relevant flags.                    */
BIT_MACRO (L, 0);

/* Op     Parameters   Code   Cycles                                 */
/* BIT    (HL), 0   0xCB 0x46    8                                   */
/*                                                                   */
/* Checks the 0 bit in (HL) and sets relevant flags.                 */
BIT_HL_MACRO (0);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       A, 0   0xCB 0x47    8                                   */
/*                                                                   */
/* Checks the 0 bit in A and sets relevant flags.                    */
BIT_MACRO (A, 0);

/*--------- BIT 1 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* BIT       B, 1   0xCB 0x48    8                                   */
/*                                                                   */
/* Checks the 1 bit in B and sets relevant flags.                    */
BIT_MACRO (B, 1);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       C, 1   0xCB 0x49    8                                   */
/*                                                                   */
/* Checks the 1 bit in C and sets relevant flags.                    */
BIT_MACRO (C, 1);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       D, 1   0xCB 0x4A    8                                   */
/*                                                                   */
/* Checks the 1 bit in D and sets relevant flags.                    */
BIT_MACRO (D, 1);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       E, 1   0xCB 0x4B    8                                   */
/*                                                                   */
/* Checks the 1 bit in E and sets relevant flags.                    */
BIT_MACRO (E, 1);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       H, 1   0xCB 0x4C    8                                   */
/*                                                                   */
/* Checks the 1 bit in H and sets relevant flags.                    */
BIT_MACRO (H, 1);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       L, 1   0xCB 0x4D    8                                   */
/*                                                                   */
/* Checks the 1 bit in L and sets relevant flags.                    */
BIT_MACRO (L, 1);

/* Op     Parameters   Code   Cycles                                 */
/* BIT    (HL), 1   0xCB 0x4E    8                                   */
/*                                                                   */
/* Checks the 1 bit in (HL) and sets relevant flags.                 */
BIT_HL_MACRO (1);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       A, 1   0xCB 0x4F    8                                   */
/*                                                                   */
/* Checks the 1 bit in A and sets relevant flags.                    */
BIT_MACRO (A, 1);

/*--------- BIT 2 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* BIT       B, 2   0xCB 0x50    8                                   */
/*                                                                   */
/* Checks the 2 bit in B and sets relevant flags.                    */
BIT_MACRO (B, 2);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       C, 2   0xCB 0x51    8                                   */
/*                                                                   */
/* Checks the 2 bit in C and sets relevant flags.                    */
BIT_MACRO (C, 2);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       D, 2   0xCB 0x52    8                                   */
/*                                                                   */
/* Checks the 2 bit in D and sets relevant flags.                    */
BIT_MACRO (D, 2);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       E, 2   0xCB 0x53    8                                   */
/*                                                                   */
/* Checks the 2 bit in E and sets relevant flags.                    */
BIT_MACRO (E, 2);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       H, 2   0xCB 0x54    8                                   */
/*                                                                   */
/* Checks the 2 bit in H and sets relevant flags.                    */
BIT_MACRO (H, 2);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       L, 2   0xCB 0x55    8                                   */
/*                                                                   */
/* Checks the 2 bit in L and sets relevant flags.                    */
BIT_MACRO (L, 2);

/* Op     Parameters   Code   Cycles                                 */
/* BIT    (HL), 2   0xCB 0x56    8                                   */
/*                                                                   */
/* Checks the 2 bit in (HL) and sets relevant flags.                 */
BIT_HL_MACRO (2);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       A, 2   0xCB 0x57    8                                   */
/*                                                                   */
/* Checks the 2 bit in A and sets relevant flags.                    */
BIT_MACRO (A, 2);

/*--------- BIT 3 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* BIT       B, 3   0xCB 0x58    8                                   */
/*                                                                   */
/* Checks the 3 bit in B and sets relevant flags.                    */
BIT_MACRO (B, 3);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       C, 3   0xCB 0x59    8                                   */
/*                                                                   */
/* Checks the 3 bit in C and sets relevant flags.                    */
BIT_MACRO (C, 3);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       D, 3   0xCB 0x5A    8                                   */
/*                                                                   */
/* Checks the 3 bit in D and sets relevant flags.                    */
BIT_MACRO (D, 3);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       E, 3   0xCB 0x5B    8                                   */
/*                                                                   */
/* Checks the 3 bit in E and sets relevant flags.                    */
BIT_MACRO (E, 3);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       H, 3   0xCB 0x5C    8                                   */
/*                                                                   */
/* Checks the 3 bit in H and sets relevant flags.                    */
BIT_MACRO (H, 3);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       L, 3   0xCB 0x5D    8                                   */
/*                                                                   */
/* Checks the 3 bit in L and sets relevant flags.                    */
BIT_MACRO (L, 3);

/* Op     Parameters   Code   Cycles                                 */
/* BIT    (HL), 3   0xCB 0x5E    8                                   */
/*                                                                   */
/* Checks the 3 bit in (HL) and sets relevant flags.                 */
BIT_HL_MACRO (3);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       A, 3   0xCB 0x5F    8                                   */
/*                                                                   */
/* Checks the 3 bit in A and sets relevant flags.                    */
BIT_MACRO (A, 3);

/*--------- BIT 4 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* BIT       B, 4   0xCB 0x60    8                                   */
/*                                                                   */
/* Checks the 4 bit in B and sets relevant flags.                    */
BIT_MACRO (B, 4);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       C, 4   0xCB 0x61    8                                   */
/*                                                                   */
/* Checks the 4 bit in C and sets relevant flags.                    */
BIT_MACRO (C, 4);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       D, 4   0xCB 0x62    8                                   */
/*                                                                   */
/* Checks the 4 bit in D and sets relevant flags.                    */
BIT_MACRO (D, 4);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       E, 4   0xCB 0x63    8                                   */
/*                                                                   */
/* Checks the 4 bit in E and sets relevant flags.                    */
BIT_MACRO (E, 4);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       H, 4   0xCB 0x64    8                                   */
/*                                                                   */
/* Checks the 4 bit in H and sets relevant flags.                    */
BIT_MACRO (H, 4);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       L, 4   0xCB 0x65    8                                   */
/*                                                                   */
/* Checks the 4 bit in L and sets relevant flags.                    */
BIT_MACRO (L, 4);

/* Op     Parameters   Code   Cycles                                 */
/* BIT    (HL), 4   0xCB 0x66    8                                   */
/*                                                                   */
/* Checks the 4 bit in (HL) and sets relevant flags.                 */
BIT_HL_MACRO (4);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       A, 4   0xCB 0x67    8                                   */
/*                                                                   */
/* Checks the 4 bit in A and sets relevant flags.                    */
BIT_MACRO (A, 4);

/*--------- BIT 5 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* BIT       B, 5   0xCB 0x68    8                                   */
/*                                                                   */
/* Checks the 5 bit in B and sets relevant flags.                    */
BIT_MACRO (B, 5);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       C, 5   0xCB 0x69    8                                   */
/*                                                                   */
/* Checks the 5 bit in C and sets relevant flags.                    */
BIT_MACRO (C, 5);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       D, 5   0xCB 0x6A    8                                   */
/*                                                                   */
/* Checks the 5 bit in D and sets relevant flags.                    */
BIT_MACRO (D, 5);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       E, 5   0xCB 0x6B    8                                   */
/*                                                                   */
/* Checks the 5 bit in E and sets relevant flags.                    */
BIT_MACRO (E, 5);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       H, 5   0xCB 0x6C    8                                   */
/*                                                                   */
/* Checks the 5 bit in H and sets relevant flags.                    */
BIT_MACRO (H, 5);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       L, 5   0xCB 0x6D    8                                   */
/*                                                                   */
/* Checks the 5 bit in L and sets relevant flags.                    */
BIT_MACRO (L, 5);

/* Op     Parameters   Code   Cycles                                 */
/* BIT    (HL), 5   0xCB 0x6E    8                                   */
/*                                                                   */
/* Checks the 5 bit in (HL) and sets relevant flags.                 */
BIT_HL_MACRO (5);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       A, 5   0xCB 0x6F    8                                   */
/*                                                                   */
/* Checks the 5 bit in A and sets relevant flags.                    */
BIT_MACRO (A, 5);

/*--------- BIT 6 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* BIT       B, 6   0xCB 0x70    8                                   */
/*                                                                   */
/* Checks the 6 bit in B and sets relevant flags.                    */
BIT_MACRO (B, 6);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       C, 6   0xCB 0x71    8                                   */
/*                                                                   */
/* Checks the 6 bit in C and sets relevant flags.                    */
BIT_MACRO (C, 6);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       D, 6   0xCB 0x72    8                                   */
/*                                                                   */
/* Checks the 6 bit in D and sets relevant flags.                    */
BIT_MACRO (D, 6);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       E, 6   0xCB 0x73    8                                   */
/*                                                                   */
/* Checks the 6 bit in E and sets relevant flags.                    */
BIT_MACRO (E, 6);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       H, 6   0xCB 0x74    8                                   */
/*                                                                   */
/* Checks the 6 bit in H and sets relevant flags.                    */
BIT_MACRO (H, 6);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       L, 6   0xCB 0x75    8                                   */
/*                                                                   */
/* Checks the 6 bit in L and sets relevant flags.                    */
BIT_MACRO (L, 6);

/* Op     Parameters   Code   Cycles                                 */
/* BIT    (HL), 6   0xCB 0x76    8                                   */
/*                                                                   */
/* Checks the 6 bit in (HL) and sets relevant flags.                 */
BIT_HL_MACRO (6);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       A, 6   0xCB 0x77    8                                   */
/*                                                                   */
/* Checks the 6 bit in A and sets relevant flags.                    */
BIT_MACRO (A, 6);

/*--------- BIT 7 --------*/

/* Op     Parameters   Code   Cycles                                 */
/* BIT       B, 7   0xCB 0x78    8                                   */
/*                                                                   */
/* Checks the 7 bit in B and sets relevant flags.                    */
BIT_MACRO (B, 7);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       C, 7   0xCB 0x79    8                                   */
/*                                                                   */
/* Checks the 7 bit in C and sets relevant flags.                    */
BIT_MACRO (C, 7);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       D, 7   0xCB 0x7A    8                                   */
/*                                                                   */
/* Checks the 7 bit in D and sets relevant flags.                    */
BIT_MACRO (D, 7);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       E, 7   0xCB 0x7B    8                                   */
/*                                                                   */
/* Checks the 7 bit in E and sets relevant flags.                    */
BIT_MACRO (E, 7);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       H, 7   0xCB 0x7C    8                                   */
/*                                                                   */
/* Checks the 7 bit in H and sets relevant flags.                    */
BIT_MACRO (H, 7);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       L, 7   0xCB 0x7D    8                                   */
/*                                                                   */
/* Checks the 7 bit in L and sets relevant flags.                    */
BIT_MACRO (L, 7);

/* Op     Parameters   Code   Cycles                                 */
/* BIT    (HL), 7   0xCB 0x7E    8                                   */
/*                                                                   */
/* Checks the 7 bit in (HL) and sets relevant flags.                 */
BIT_HL_MACRO (7);

/* Op     Parameters   Code   Cycles                                 */
/* BIT       A, 7   0xCB 0x7F    8                                   */
/*                                                                   */
/* Checks the 7 bit in A and sets relevant flags.                    */
BIT_MACRO (A, 7);

#undef BIT_MACRO
#undef BIT_HL_MACRO

/*-------------------------------------------------------------------*/
/*---------------------------JUMP-Operations-------------------------*/
/*-------------------------------------------------------------------*/
/* The carry flag is not affected.                                   */
/*                                                                   */
/* The half-carry flag is not affected.                              */
/*                                                                   */
/* The zero flag is not affected.                                    */
/*                                                                   */
/* The subtract flag is not affected.                                */
/*-------------------------------------------------------------------*/

/* Op     Parameters   Code   Cycles                                 */
/* LD         nn       0xC3     16                                   */
/*                                                                   */
/* Jump to 2 byte immediate value.                                   */
void JP_nn () {
    int least_significant_byte, most_significant_byte;

    /* LSB comes first at PC. */
    least_significant_byte = read_memory (PC);

    /* Increment past LSB. */
    increment_PC ();

    /* MSB comes second at PC+1.*/
    most_significant_byte = read_memory (PC);

    /* Increment past MSB. */
    increment_PC ();

    /* Set PC to the new address. */
    PC = (most_significant_byte << 8) | least_significant_byte;
}

/*-------------------------------------------------------------------*/
/*------------------------OPERATION-DISPATCH-------------------------*/
/*-------------------------------------------------------------------*/
/* An array of functions pointers. The index should match up with    */
/* the op code.                                                      */
/*                                                                   */
/* We define them literally here. If this were not the case, we      */
/* would need an initialization function.                            */
/*-------------------------------------------------------------------*/

/* This dispatch table is used for any instruction
   with the 0xCB prefix */
void (* CB_op_dispatch [0xFF + 0x01]) () = {
/*  0x00                                                             */
    RLC_B, RLC_C, RLC_D, RLC_E, RLC_H, RLC_L, RLC_HL, RLC_A, RRC_B, RRC_C,
/*  0x0A                                                             */
    RRC_D, RRC_E, RRC_H, RRC_L, RRC_HL, RRC_A, RL_B, RL_C, RL_D, RL_E,
/*  0x14                                                             */
    RL_H, RL_L, RL_HL, RL_A, RR_B, RR_C, RR_D, RR_E, RR_H, RR_L,
/*  0x1E                                                             */
    RR_HL, RR_A, SLA_B, SLA_C, SLA_D, SLA_E, SLA_H, SLA_L, SLA_HL, SLA_A,
/*  0x28                                                             */
    SRA_B, SRA_C, SRA_D, SRA_E, SRA_H, SRA_L, SRA_HL, SRA_A, SWAP_B, SWAP_C,
/*  0x32                                                             */
    SWAP_D, SWAP_E, SWAP_H, SWAP_L, SWAP_HL, SWAP_A, SRL_B, SRL_C, SRL_D, SRL_E,
/*  0x3C                                                             */
    SRL_H, SRL_L, SRL_HL, SRL_A, BIT_B_0, BIT_C_0, BIT_D_0, BIT_E_0, BIT_H_0, BIT_L_0,
/*  0x46                                                             */
    BIT_HL_0, BIT_A_0, BIT_B_1, BIT_C_1, BIT_D_1, BIT_E_1, BIT_H_1, BIT_L_1, BIT_HL_1, BIT_A_1,
/*  0x50                                                             */
    BIT_B_2, BIT_C_2, BIT_D_2, BIT_E_2, BIT_H_2, BIT_L_2, BIT_HL_2, BIT_A_2, BIT_B_3, BIT_C_3,
/*  0x5A                                                             */
    BIT_D_3, BIT_E_3, BIT_H_3, BIT_L_3, BIT_HL_3, BIT_A_3, BIT_B_4, BIT_C_4, BIT_D_4, BIT_E_4,
/*  0x64                                                             */
    BIT_H_4, BIT_L_4, BIT_HL_4, BIT_A_4, BIT_B_5, BIT_C_5, BIT_D_5, BIT_E_5, BIT_H_5, BIT_L_5,
/*  0x6E                                                             */
    BIT_HL_5, BIT_A_5, BIT_B_6, BIT_C_6, BIT_D_6, BIT_E_6, BIT_H_6, BIT_L_6, BIT_HL_6, BIT_A_6,
/*  0x78                                                             */
    BIT_B_7, BIT_C_7, BIT_D_7, BIT_E_7, BIT_H_7, BIT_L_7, BIT_HL_7, BIT_A_7, RES_B_0, RES_C_0,
/*  0x82                                                             */
    RES_D_0, RES_E_0, RES_H_0, RES_L_0, RES_HL_0, RES_A_0, RES_B_1, RES_C_1, RES_D_1, RES_E_1,
/*  0x8C                                                             */
    RES_H_1, RES_L_1, RES_HL_1, RES_A_1, RES_B_2, RES_C_2, RES_D_2, RES_E_2, RES_H_2, RES_L_2,
/*  0x96                                                             */
    RES_HL_2, RES_A_2, RES_B_3, RES_C_3, RES_D_3, RES_E_3, RES_H_3, RES_L_3, RES_HL_3, RES_A_3,
/*  0xA0                                                             */
    RES_B_4, RES_C_4, RES_D_4, RES_E_4, RES_H_4, RES_L_4, RES_HL_4, RES_A_4, RES_B_5, RES_C_5,
/*  0xAA                                                             */
    RES_D_5, RES_E_5, RES_H_5, RES_L_5, RES_HL_5, RES_A_5, RES_B_6, RES_C_6, RES_D_6, RES_E_6,
/*  0xB4                                                             */
    RES_H_6, RES_L_6, RES_HL_6, RES_A_6, RES_B_7, RES_C_7, RES_D_7, RES_E_7, RES_H_7, RES_L_7,
/*  0xBE                                                             */
    RES_HL_7, RES_A_7, SET_B_0, SET_C_0, SET_D_0, SET_E_0, SET_H_0, SET_L_0, SET_HL_0, SET_A_0,
/*  0XC8                                                             */
    SET_B_1, SET_C_1, SET_D_1, SET_E_1, SET_H_1, SET_L_1, SET_HL_1, SET_A_1, SET_B_2, SET_C_2,
/*  0XD2                                                             */
    SET_D_2, SET_E_2, SET_H_2, SET_L_2, SET_HL_2, SET_A_2, SET_B_3, SET_C_3, SET_D_3, SET_E_3,
/*  0XDC                                                             */
    SET_H_3, SET_L_3, SET_HL_3, SET_A_3, SET_B_4, SET_C_4, SET_D_4, SET_E_4, SET_H_4, SET_L_4,
/*  0XE6                                                             */
    SET_HL_4, SET_A_4, SET_B_5, SET_C_5, SET_D_5, SET_E_5, SET_H_5, SET_L_5, SET_HL_5, SET_A_5,
/*  0XF0                                                             */
    SET_B_6, SET_C_6, SET_D_6, SET_E_6, SET_H_6, SET_L_6, SET_HL_6, SET_A_6, SET_B_7, SET_C_7,
/*  0XFA                                                             */
    SET_D_7, SET_E_7, SET_H_7, SET_L_7, SET_HL_7, SET_A_7
};


/* This is the primary dispatch table. */
void (* op_dispatch [0xFF + 0x01]) () = {
/*  0x00                                                             */
    NOP, NULL, LD_BC_A, INC16_BC, INC_B, DEC_B, NULL, RLCA, NULL, ADD_HL_BC,
/*  0x0A                                                             */
    LD_A_BC, DEC16_BC, INC_C, DEC_C, NULL, RRCA, STOP, NULL, NULL, INC16_DE,
/*  0x14                                                             */
    INC_D, DEC_D, NULL, RLA, NULL, ADD_HL_DE, LD_A_DE, DEC16_DE, INC_E, DEC_E,
/*  0x1E                                                             */
    NULL, RRA, NULL, NULL, NULL, INC16_HL, INC_H, DEC_H, NULL, DAA,
/*  0x28                                                             */
    NULL, ADD_HL_HL, NULL, DEC16_HL, INC_L, DEC_L, NULL, CPL, NULL, NULL,
/*  0x32                                                             */
    NULL, INC16_SP, INC_HL, DEC_HL, LD_HL_n, SCF, NULL, ADD_HL_SP, NULL, DEC16_SP,
/*  0x3C                                                             */
    INC_A, DEC_A, LD_A_n, CCF, LD_B_B, LD_B_C, LD_B_D, LD_B_E, LD_B_H, LD_B_L,
/*  0x46                                                             */
    LD_B_HL, LD_B_A, LD_C_B, LD_C_C, LD_C_D, LD_C_E, LD_C_H, LD_C_L, LD_C_HL, LD_C_A,
/*  0x50                                                             */
    LD_D_B, LD_D_C, LD_D_D, LD_D_E, LD_D_H, LD_D_L, LD_D_HL, LD_D_A, LD_E_B, LD_E_C,
/*  0x5A                                                             */
    LD_E_D, LD_E_E, LD_E_H, LD_E_L, LD_E_HL, LD_E_A, LD_H_B, LD_H_C, LD_H_D, LD_H_E,
/*  0x64                                                             */
    LD_H_H, LD_H_L, LD_H_HL, LD_H_A, LD_L_B, LD_L_C, LD_L_D, LD_L_E, LD_L_H, LD_L_L,
/*  0x6E                                                             */
    LD_L_HL, LD_L_A, LD_HL_B, LD_HL_C, LD_HL_D, LD_HL_E, LD_HL_H, LD_HL_L, HALT, LD_HL_A,
/*  0x78                                                             */
    LD_A_B, LD_A_C, LD_A_D, LD_A_E, LD_A_H, LD_A_L, LD_A_HL, LD_A_A, ADD_A_B, ADD_A_C,
/*  0x82                                                             */
    ADD_A_D, ADD_A_E, ADD_A_H, ADD_A_L, ADD_A_HL, ADD_A_A, ADC_A_B, ADC_A_C, ADC_A_D, ADC_A_E,
/*  0x8C                                                             */
    ADC_A_H, ADC_A_L, ADC_A_HL, ADC_A_A, SUB_A_B, SUB_A_C, SUB_A_D, SUB_A_E, SUB_A_H, SUB_A_L,
/*  0x96                                                             */
    SUB_A_HL, SUB_A_A, SBC_A_B, SBC_A_C, SBC_A_D, SBC_A_E, SBC_A_H, SBC_A_L, SBC_A_HL, SBC_A_A,
/*  0xA0                                                             */
    AND_A_B, AND_A_C, AND_A_D, AND_A_E, AND_A_H, AND_A_L, AND_A_HL, AND_A_A, XOR_A_B, XOR_A_C,
/*  0xAA                                                             */
    XOR_A_D, XOR_A_E, XOR_A_H, XOR_A_L, XOR_A_HL, XOR_A_A, OR_A_B, OR_A_C, OR_A_D, OR_A_E,
/*  0xB4                                                             */
    OR_A_H, OR_A_L, OR_A_HL, OR_A_A, CP_A_B, CP_A_C, CP_A_D, CP_A_E, CP_A_H, CP_A_L,
/*  0xBE                                                             */
    CP_A_HL, CP_A_A, NULL, NULL, NULL, NULL, NULL, NULL, NULL, RST_0,
/*  0xC8                                                             */
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, RST_1, NULL, NULL,
/*  0xD2                                                             */
    NULL, NULL, NULL, NULL, NULL, RST_2, NULL, NULL, NULL, NULL,
/*  0xDC                                                             */
    NULL, NULL, NULL, RST_3, NULL, NULL, NULL, NULL, NULL, NULL,
/*  0xE6                                                             */
    NULL, RST_4, NULL, NULL, LD_nn_A, NULL, NULL, NULL, NULL, RST_5,
/*  0xF0                                                             */
    NULL, NULL, NULL, DI, NULL, NULL, NULL, RST_6, NULL, NULL,
/*  0xFA                                                             */
    LD_A_nn, EI, NULL, NULL, NULL, RST_7
};
