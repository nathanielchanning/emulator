.PHONY: build_emulator clean read_err

TEST_FLAGS=-DTEST_BUILD -g -fpic

build_test: build_emulator_test
	gcc -shared -o libemulator.so emulator-test.o memory-test.o

build_emulator_test: emulator-test.o memory-test.o

build_emulator: emulator.o

memory-test.o: src/memory.c
	gcc -c src/memory.c -o memory-test.o $(TEST_FLAGS)

memory.o: src/memory.c
	gcc -c src/memory.c

emulator-test.o: src/emulator.c
	gcc -c src/emulator.c -o emulator-test.o $(TEST_FLAGS)

emulator.o: src/emulator.c
	gcc -c src/emulator.c

read_err:
	make 2>&1 | less

clean:
	rm op_tests *.o
